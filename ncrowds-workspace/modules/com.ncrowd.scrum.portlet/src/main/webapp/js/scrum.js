/* (c) 2017 RTG Portale GmbH */
var scrum = scrum || {};
(function($, window) {
    "use strict";
    scrum.scrumBoard = {
        colorPickerActive: false,
        taskReceived: false,
        init: function(pushEnabled) {
            var board = this;
            var connectWithList = '.taskColumn';
            board.modifyColumnHeight();
            if ($('.sprintSelection label').text() === 'Backlog') {
                connectWithList = '';
                $('.backlogWarningMessage').show();
            } else {
                $('.backlogWarningMessage').hide();
            }
            $('.taskColumn').sortable({
                revert: true,
                receive: board.handleReceiveTask,
                stop: board.handleSortTasks,
                connectWith: connectWithList,
                cancel: '.ui-dialog, .taskActions, .colorpicker'
            });
            $('.taskColumn').each(function() {
                board.addPlaceholderIfEmpty(this);
            });
            $('.task').hover(board.onMouseEnterTask, board.onMouseLeaveTask);
            var colorMode = $('.selectColorMode').find('input:checked').val();
            if (colorMode === 'INDIVIDUAL' || colorMode === 'GROUP') {
                $('.colorPicker').show();
                $('.colorPicker').each(function() {
                    board.initColorPicker(this);
                });
            } else {
                $('.colorPicker').hide();
            }
            if (pushEnabled) {
                var url = '';
                if (themeDisplay != null) {
                    url = themeDisplay.getPortalURL().replace(/https?:\/\//i, 'ws://') + '/o/updateTask';
                }
                var websocket = new WebSocket(url);
                websocket.onopen = function(evt) {
                    console.log('Successfully connected to websocket via ' + url);
                };
                websocket.onclose = function(evt) {
                    console.log('Websocket disconnected');
                };
                websocket.onmessage = function(evt) {
                    scrum.util.handleRefreshMessage(evt.data);
                };
                websocket.onerror = function(evt) {
                    console.log('Websocket error: ' + evt.data);
                };
            }
        },
        initColorPicker: function(picker) {
            var board = this;
            var colorMode = $('.selectColorMode').find('input:checked').val();
            var taskGroupId = $(picker).parent().parent().parent().parent().find('[id$="taskGroupId"]').val();
            if (colorMode === 'GROUP' && taskGroupId == 0) {
                $(picker).hide();
                return;
            }
            var taskBackground = $(picker).parent().parent().parent().css('background-color');
            $(picker).children('div').css('background-color', taskBackground);
            $(picker).ColorPicker({
                color: scrum.util.rgbToHex(taskBackground),
                appendTo: $(picker).parents('.task'),
                onShow: function(colpkr) {
                    $(colpkr).fadeIn(500);
                    scrum.scrumBoard.colorPickerActive = true;
                    $(this).parent().css('display', 'block');
                    return false;
                },
                onHide: function(colpkr) {
                    var rgb = $(colpkr).find('.colorpicker_new_color').css('background-color');
                    var colorMode = $('.selectColorMode').find('input:checked').val();
                    var taskElementId = $('.taskActions:visible').parent().parent().parent().attr('id').split('_');
                    var modifiedTaskId = taskElementId[1];
                    var oJson = {
                        command: 'updateColor',
                        taskId: modifiedTaskId,
                        hexColor: scrum.util.rgbToHex(rgb),
                        mode: colorMode
                    };
                    board.sendCommandToBean(oJson);
                    $(colpkr).fadeOut(500);
                    scrum.scrumBoard.colorPickerActive = false;
                    $('.taskActions').css('display', 'none');
                    return false;
                },
                onSubmit: function(hsb, hex, rgb) {
                    updateColor(hex);
                },
                onChange: function(hsb, hex, rgb) {
                    updateColor(hex);
                }
            });

            function updateColor(hex) {
                var colorMode = $('.selectColorMode').find('input:checked').val();
                if (colorMode === 'INDIVIDUAL') {
                    $('.colorPicker div:visible').css('backgroundColor', '#' + hex);
                    $('.taskActions:visible').parent().parent().css('backgroundColor', '#' + hex);
                } else if (colorMode === 'GROUP') {
                    var taskGroupId = $('.taskActions:visible').parent().parent().parent().find('[id$="taskGroupId"]').val();
                    $('[id$="taskGroupId"]').each(function() {
                        if ($(this).val() === taskGroupId) {
                            $(this).parent().children(':first').css('backgroundColor', '#' + hex);
                            $(this).parent().find('.colorPicker div').css('backgroundColor', '#' + hex);
                        }
                    });
                }
            }
        },
        handleReceiveTask: function(event, ui) {
            var newColumnUL = this;
            var taskElementId = $(ui.item).attr('id').split('_');
            var modifiedTaskId = taskElementId[1];
            var newStatusIndex = $(newColumnUL).index('.taskColumn');
            var oldStatusIndex = $(ui.sender).index('.taskColumn');
            var newIndex = $(ui.item).index();
            var oJson = {
                command: 'updateStatus',
                taskId: modifiedTaskId,
                newStatusIndex: newStatusIndex,
                oldStatusIndex: oldStatusIndex,
                newSortingIndex: newIndex
            };
            scrum.scrumBoard.sendCommandToBean(oJson);
            scrum.scrumBoard.taskReceived = true;
            if ($(newColumnUL).find('.taskPlaceholder').length !== 0) {
                $(newColumnUL).find('.taskPlaceholder').remove();
            }
            scrum.scrumBoard.modifyColumnHeight();
        },
        handleSortTasks: function(event, ui) {
            if (scrum.scrumBoard.taskReceived) {
                scrum.scrumBoard.taskReceived = false;
                return;
            }
            var taskElementId = $(ui.item).attr('id').split('_');
            var taskId = taskElementId[1];
            var newIndex = $(ui.item).index();
            var oJson = {
                command: 'updateSorting',
                taskId: taskId,
                newSortingIndex: newIndex
            };
            scrum.scrumBoard.sendCommandToBean(oJson);
        },
        addPlaceholderIfEmpty: function(taskColumn) {
            if ($(taskColumn).children('li').length === 0) {
                $(taskColumn).append('<li class="taskPlaceholder">&#160;</li>');
            }
        },
        onMouseEnterTask: function() {
            var task = this;
            if (!scrum.scrumBoard.colorPickerActive) {
                $(task).find('.taskActions').css('display', 'block');
            }
        },
        onMouseLeaveTask: function() {
            var task = this;
            if (!scrum.scrumBoard.colorPickerActive) {
                $(task).find('.taskActions').css('display', 'none');
            }
        },
        sendCommandToBean: function(json) {
        	debugger;
            sendAjaxCommand([{
                name: 'ajaxCommand',
                value: JSON.stringify(json)
            }]);
        },
        modifyColumnHeight: function() {
            var $columns = $('.scrumBoardColumn');
            var maxHeight = 0;
            for (var i = 0; i < $columns.length; i++) {
                if ($columns[i].clientHeight > maxHeight) {
                    maxHeight = $columns[i].clientHeight;
                }
            }
            if (maxHeight > 0) {
                $('.taskColumn').css('min-height', maxHeight);
            }
        }
    };
    scrum.util = {
        initTabs: function() {
            $('#scrumTabs').tabs({}).bind('tabsshow', function(event, ui) {
                scrum.util.activateTab(event, ui);
            });
        },
        activateTab: function(event, ui) {
            if (ui.panel.id === 'tabSprints') {
                PF('sprintSchedule').update();
                $('.sprintSelection').hide();
            } else if (ui.panel.id === 'tabVision') {
                $('.sprintSelection').hide();
            } else if (ui.panel.id === 'tabBurndown') {
                var burndown = PF('burndown');
                if (typeof burndown !== 'undefined' && burndown) {
                    burndown.plot.destroy();
                    burndown.render();
                }
                $('.sprintSelection').show();
            } else {
                $('.sprintSelection').show();
            }
        },
        resetForm: function() {
            $('.scrumForm').get(0).reset();
        },
        toggleEditNewDataTableItem: function() {
            $('.ui-datatable-data tr').last().find('span.ui-icon-pencil').click();
        },
        rgbToHex: function(rgb) {
            var util = this;
            if (rgb.search('rgb') === -1) {
                return rgb;
            } else {
                rgb = rgb.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/);
                return '#' + util.hex(rgb[1]) + util.hex(rgb[2]) + util.hex(rgb[3]);
            }
        },
        hex: function(x) {
            return ('0' + parseInt(x).toString(16)).slice(-2);
        },
        extendPlot: function() {
            var plot = this;
            plot.cfg.canvasOverlay = {
                show: true,
                objects: [{
                    line: {
                        name: 'ideal',
                        start: [new $.jsDate(plot.cfg.axes.xaxis.min).getTime(), this.cfg.data[0][0][1]],
                        stop: [new $.jsDate(plot.cfg.axes.xaxis.max).getTime(), 0],
                        lineWidth: 1,
                        color: 'rgb(163, 163, 163)',
                        shadow: false,
                        lineCap: 'butt',
                        xOffset: 0
                    }
                }]
            };
            if (plot.cfg.legend) {
                plot.cfg.legend.renderer = $.jqplot.TableLegendRenderer;
                $.jqplot.addLegendRowHooks.push(scrum.util.extendChartLegend);
            }
        },
        extendChartLegend: function(series) {
            var legend = this;
            if ($(legend._elem).find('tr').size() == 1) {
                var legendItem = {
                    label: Liferay.Language.get('scrum-burndown-ideal-line'),
                    color: 'rgb(163, 163, 163)'
                };
                return legendItem;
            }
        },
        getBurndownTooltip: function(str, seriesIndex, pointIndex, plot) {
            var val = plot.data[seriesIndex][pointIndex];
            return '<span class="tooltip-value">' + val[1] + '</span>';
        },
        toggleTreeRows: function() {
            $('.ui-treetable-toggler').click();
        },
        rebuildStatusPlots: function() {
            var burndown = PF('burndown');
            if (typeof burndown !== 'undefined' && burndown) {
                burndown.plot.destroy();
                burndown.render();
            }
            if (typeof meter !== 'undefined' && meter) {
                meter.plot.destroy();
                meter.render();
            }
            if (typeof taskStatus !== 'undefined' && taskStatus) {
                taskStatus.plot.destroy();
                taskStatus.render();
            }
        },
        handleRefreshMessage: function(msg) {
            console.log('Push message received: update task ' + msg);
            refreshTasks();
        }
    };
})(jQuery, window);