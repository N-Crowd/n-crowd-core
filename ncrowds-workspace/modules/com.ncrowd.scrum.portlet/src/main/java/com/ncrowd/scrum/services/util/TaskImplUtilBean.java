package com.ncrowd.scrum.services.util;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.ncrowd.model.Task;
import com.ncrowd.scrum.bean.ServiceTrackerBean;
import com.ncrowd.scrum.model.ColorMode;
import com.ncrowd.scrum.servicetracker.TaskServiceTracker;
import com.ncrowd.scrum.servicetracker.WorkingHoursServiceTracker;
import com.ncrowd.service.TaskLocalService;
import com.ncrowd.service.WorkingHoursLocalService;

import java.util.HashMap;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

@ManagedBean
@ViewScoped
public class TaskImplUtilBean {
	
	private Task groupTask;
	private double timeForCopy;
	private static String DEFAULT_COLOR = "#FCEE74";
	private static float DEFAULT_WORKING_HOURS = 4.0F;
	private static final Log LOG = LogFactoryUtil.getLog(TaskImplUtilBean.class);
	private Map<ColorMode, String> colorMap;
	@ManagedProperty("#{trackerBean}")
	private ServiceTrackerBean serviceTrackerBean;

	public void setServiceTrackerBean(ServiceTrackerBean serviceTrackerBean) {
		this.serviceTrackerBean = serviceTrackerBean;
	}

	private ServiceTracker<TaskLocalService, TaskLocalService> getTaskServiceTracker() {
		return new TaskServiceTracker(FrameworkUtil.getBundle(this.getClass()).getBundleContext());
	}

	private ServiceTracker<WorkingHoursLocalService, WorkingHoursLocalService> getWorkingHoursServiceTracker() {
		return new WorkingHoursServiceTracker(FrameworkUtil.getBundle(this.getClass()).getBundleContext());
	}

	public TaskLocalService getTaskService() {
		ServiceTracker<TaskLocalService, TaskLocalService> tracker = getTaskServiceTracker();
		tracker.open();
		return !tracker.isEmpty() ? tracker.getService() : null;
	}

	public WorkingHoursLocalService getWorkingHoursService() {
		ServiceTracker<WorkingHoursLocalService, WorkingHoursLocalService> tracker = getWorkingHoursServiceTracker();
		tracker.open();
		return !tracker.isEmpty() ? tracker.getService() : null;
	}

	public Task getGroupTask() {
	      if (groupTask.getTaskGroupId() != 0L) {
	         try {
	            groupTask = serviceTrackerBean.getTaskService().getTask(groupTask.getTaskGroupId());
	         } catch (Exception e) {
	            LOG.error("Error getting task group of task " + groupTask.getTaskId(), e);
	         }
	      }

	      return groupTask;
	   }
	
	 public void setGroupTask(Task groupTask) {
	      this.groupTask = groupTask;
	  }

	public String getColor(ColorMode mode) {
		this.initColorMap();
		return (String) this.colorMap.get(mode);
	}

	private void initColorMap() {
		this.colorMap = new HashMap<>();
		String individualColor = DEFAULT_COLOR;
		String groupColor = "";
		if (groupTask.getTaskGroupId() != 0L) {
			groupColor = groupTask.getGroupTask().getIndividualColor();
		}

		if ("".equals(groupColor)) {
			groupColor = DEFAULT_COLOR;
		}

		this.colorMap.put(ColorMode.DEFAULT, DEFAULT_COLOR);
		this.colorMap.put(ColorMode.INDIVIDUAL, individualColor);
		this.colorMap.put(ColorMode.GROUP, groupColor);
	}
}
