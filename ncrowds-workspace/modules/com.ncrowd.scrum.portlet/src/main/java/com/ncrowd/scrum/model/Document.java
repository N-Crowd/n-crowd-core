package com.ncrowd.scrum.model;

import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFileVersion;
import com.liferay.document.library.kernel.util.PDFProcessorUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.FileVersion;
import com.ncrowd.scrum.bean.ServiceTrackerBean;
import com.ncrowd.scrum.util.ScrumUtil;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedProperty;

import org.primefaces.model.StreamedContent;

public class Document {
	private long documentId;
	private String title;
	private String description;
	private String version;
	private String mimeType;
	private long size;
	private String userName;
	private Date date;
	private String thumbnailSrc;
	private String previewSrc;
	private List<Document> oldVersions;
	private long mappingId;
	private Map<String, String> mimeTypeMapping;
	private static final Log LOG = LogFactoryUtil.getLog(Document.class);

	@ManagedProperty("#{trackerBean}")
	private ServiceTrackerBean serviceTrackerBean;

	public void setServiceTrackerBean(ServiceTrackerBean serviceTrackerBean) {
		this.serviceTrackerBean = serviceTrackerBean;
	}

	public Document() {
	}

	public Document(DLFileEntry dlFileEntry, String thumbnailSrc, String previewSrc, long mappingId) {
		this.documentId = dlFileEntry.getFileEntryId();
		this.title = dlFileEntry.getTitle();
		this.description = dlFileEntry.getDescription();
		this.version = dlFileEntry.getVersion();
		this.mimeType = dlFileEntry.getMimeType();
		this.size = dlFileEntry.getSize();
		this.userName = dlFileEntry.getUserName();
		this.date = dlFileEntry.getModifiedDate();
		this.thumbnailSrc = thumbnailSrc;
		this.previewSrc = previewSrc;
		this.mappingId = mappingId;
		this.mimeTypeMapping = this.fillMimeTypeMapping();
	}

	private Map<String, String> fillMimeTypeMapping() {
		return new HashMap<>();
	}

	public Document(DLFileVersion dlFileVersion, String thumbnailSrc, String previewSrc) {
		this.documentId = dlFileVersion.getFileEntryId();
		this.title = dlFileVersion.getTitle();
		this.description = dlFileVersion.getDescription();
		this.version = dlFileVersion.getVersion();
		this.mimeType = dlFileVersion.getMimeType();
		this.size = dlFileVersion.getSize();
		this.userName = dlFileVersion.getUserName();
		this.date = dlFileVersion.getModifiedDate();
		this.thumbnailSrc = thumbnailSrc;
		this.previewSrc = previewSrc;
		this.mappingId = 0L;
	}

	public long getDocumentId() {
		return this.documentId;
	}

	public String getDocumentIdString() {
		return String.valueOf(this.documentId);
	}

	public void setDocumentId(long documentId) {
		this.documentId = documentId;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getVersion() {
		return this.version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getMimeType() {
		return this.mimeType;
	}

	public String getMimeTypePretty() {
		String mapping = this.mimeTypeMapping.get(this.mimeType);
		return mapping != null ? mapping : this.mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public long getSize() {
		return this.size;
	}

	public String getSizeFormat() {
		if (this.size >= 1048576L) {
			return Math.round((float) this.size / 1048576.0F) + " MB";
		} else {
			return this.size >= 1024L ? Math.round((float) this.size / 1024.0F) + " KB" : this.size + " B";
		}
	}

	public void setSize(long size) {
		this.size = size;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getThumbnailSrc() {
		return this.thumbnailSrc;
	}

	public void setThumbnailSrc(String thumbnailSrc) {
		this.thumbnailSrc = thumbnailSrc;
	}

	public String getPreviewSrc() {
		return this.previewSrc;
	}

	public void setPreviewSrc(String previewSrc) {
		this.previewSrc = previewSrc;
	}

	public List<Document> getOldVersions() {
		return this.oldVersions;
	}

	public void setOldVersions(List<Document> oldVersions) {
		this.oldVersions = oldVersions;
	}

	public StreamedContent getFile() throws PortalException, IOException {
		DLFileEntry dlFileEntry = serviceTrackerBean.getDlFileService().fetchDLFileEntry(this.documentId);
		if (dlFileEntry != null) {
			InputStream stream = dlFileEntry.getContentStream();
			ScrumUtil.writeStreamToOutputStream(stream, this.title, this.mimeType);
		}

		return null;
	}

	public long getMappingId() {
		return this.mappingId;
	}

	public void setMappingId(long mappingId) {
		this.mappingId = mappingId;
	}

	public boolean isImage() {
		return this.mimeType.startsWith("image/") || !PDFProcessorUtil.isDocumentSupported(this.mimeType);
	}

	public boolean isMedia() {
		if (this.isImage()) {
			return false;
		} else if ("application/pdf".equals(this.mimeType)) {
			return true;
		} else {
			try {
				FileEntry fileEntry = serviceTrackerBean.getDlAppService().getFileEntry(this.documentId);
				FileVersion fileVersion = fileEntry.getFileVersion(this.version);
				if (PDFProcessorUtil.hasImages(fileVersion)) {
					return true;
				}
			} catch (Exception var3) {
				LOG.error("Error getting PDF preview of document " + this.documentId, var3);
			}

			return false;
		}
	}

	public boolean isPreviewable() {
		return this.isImage() || this.isMedia();
	}

	public String getPdfPreviewUrl() {
		return "application/pdf".equals(this.mimeType) ? this.previewSrc : this.previewSrc + "&targetExtension=pdf";
	}
}
