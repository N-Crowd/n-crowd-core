package com.ncrowd.scrum.bean;

import com.liferay.message.boards.model.MBDiscussion;
import com.liferay.message.boards.model.MBMessage;
import com.liferay.message.boards.model.MBThread;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.ratings.kernel.model.RatingsEntry;
import com.ncrowd.model.Task;
import com.ncrowd.scrum.util.ScrumUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import info.whitebyte.component.commentbox.Comment;

@ManagedBean(name = "discussionBean")
@ViewScoped
public class DiscussionBean implements Serializable {

	private boolean onCreateComment;

	@ManagedProperty("#{editTaskBean}")
	private EditTaskBean editTaskBean;
	private static Log LOG = LogFactoryUtil.getLog(DiscussionBean.class);
	private final double COMMENT_LIKE_SCORE = 5.0D;
	private long taskId;
	private long commentsPerPage = Long.MAX_VALUE;
	private long commentsPage = 1L;
	private long commentCount;
	private List<Comment> comments;
	@ManagedProperty("#{trackerBean}")
	private ServiceTrackerBean serviceTrackerBean;

	public void setServiceTrackerBean(ServiceTrackerBean serviceTrackerBean) {
		this.serviceTrackerBean = serviceTrackerBean;
	}

	public void setEditTaskBean(EditTaskBean editTaskBean) {
		this.editTaskBean = editTaskBean;
	}

	@PostConstruct
	public void init() {
		Task task = this.editTaskBean.getCurrentTask();
		if (task != null && !task.isNew()) {
			this.taskId = task.getTaskId();
			this.loadTaskComments();
		} else {
			this.comments = new ArrayList<>();
		}

	}

	private void loadTaskComments() {
		this.comments = new ArrayList<>();

		try {
			MBDiscussion mbDiscussion = serviceTrackerBean.getMbDiscussionService().getDiscussion(Task.class.getName(), this.taskId);
			MBThread mbThread = serviceTrackerBean.getMbThreadService().getThread(mbDiscussion.getThreadId());
			List<MBMessage> messages = serviceTrackerBean.getMbMessageService().getThreadMessages(mbThread.getThreadId(), -1);
			List<MBMessage> answers = new ArrayList<>();
			Iterator var6 = messages.iterator();

			while (var6.hasNext()) {
				MBMessage message = (MBMessage) var6.next();
				if (message.getParentMessageId() == mbThread.getRootMessageId()) {
					this.comments.add(this.convertMessageToComment(message, (Comment) null));
				} else if (!message.isRoot()) {
					 answers.add(message);
				}
			}

			Comment comment;
			for (var6 = this.comments.iterator(); var6
					.hasNext(); answers = this.addChildComments(comment, (List) answers)) {
				comment = (Comment) var6.next();
			}
		} catch (SystemException var7) {
			LOG.error("Error loading task comments", var7);
		} catch (PortalException var8) {
			LOG.error("Error loading task comments", var8);
		}

		this.commentCount = (long) this.comments.size();
	}

	private List<MBMessage> addChildComments(Comment comment, List<MBMessage> messages) {
		List<Comment> answers = new ArrayList<>();
		List<MBMessage> remainingMessages = new ArrayList<>();
		Iterator var6 = messages.iterator();

		while (var6.hasNext()) {
			MBMessage message = (MBMessage) var6.next();
			if (message.getParentMessageId() == Long.parseLong(comment.getId())) {
				Comment answer = this.convertMessageToComment(message, comment);
				this.addChildComments(answer, messages);
				answers.add(answer);
			} else {
				remainingMessages.add(message);
			}
		}

		comment.setAnswers(answers);
		return remainingMessages;
	}

	private Comment convertMessageToComment(MBMessage message, Comment parent) {
		ThemeDisplay themeDisplay = ScrumUtil.getThemeDisplay();
		Comment comment = new Comment();
		comment.setComment_text(message.getBody());
		comment.setId(String.valueOf(message.getPrimaryKey()));
		comment.setModification_time(new Date(message.getModifiedDate().getTime()));
		comment.setUser_id(String.valueOf(message.getUserId()));
		comment.setUser_username(message.getUserName());
		comment.setParent(parent);
		int ratingsCount = this.getCommentRatingsCount(Long.valueOf(comment.getId()));
		comment.setLikecount(ratingsCount);
		String avatarURL = "";

		try {
			User user = serviceTrackerBean.getUserService().getUser(message.getUserId());
			avatarURL = themeDisplay.getPortalURL() + user.getPortraitURL(themeDisplay);
		} catch (Exception var8) {
			LOG.error("Error getting user object for comment", var8);
		}

		comment.setUser_profile_avatar_url(avatarURL);
		return comment;
	}

	private int getCommentRatingsCount(long id) {
		int ratingsCount = 0;

		try {
			ratingsCount = serviceTrackerBean.getRatingsEntryService().getEntriesCount(MBMessage.class.getName(), id, 5.0D);
		} catch (SystemException var5) {
			LOG.error("Error getting ratings stats of comment " + id, var5);
		}

		return ratingsCount;
	}

	private boolean saveComment(Comment comment) {
		try {
			MBDiscussion mbDiscussion = serviceTrackerBean.getMbDiscussionService().getDiscussion(Task.class.getName(), this.taskId);
			MBThread mbThread = serviceTrackerBean.getMbThreadService().getThread(mbDiscussion.getThreadId());
			ServiceContext serviceContext = ScrumUtil.getServiceContext();
			long groupId = serviceContext.getScopeGroupId();
			long parentId = 0L;
			boolean answer = false;
			if (comment.getParent() != null) {
				parentId = Long.parseLong(comment.getParent().getId());
				answer = true;
			} else {
				parentId = mbThread.getRootMessageId();
			}

			MBMessage message = serviceTrackerBean.getMbMessageService().addDiscussionMessage(Long.parseLong(comment.getUser_id()),
					comment.getUser_username(), groupId, Task.class.getName(), this.taskId, mbThread.getThreadId(),
					parentId, String.valueOf(this.taskId), comment.getComment_text(), serviceContext);
			if (answer) {
				serviceTrackerBean.getMbMessageService().updateAnswer(message.getMessageId(), true, false);
			}

			comment.setId(String.valueOf(message.getMessageId()));
			return true;
		} catch (SystemException var11) {
			LOG.error("Error creating a new comment", var11);
		} catch (PortalException var12) {
			LOG.error("Error creating a new comment", var12);
		}

		return false;
	}

	public boolean onCreateComment(Comment comment) {
		return this.saveComment(comment);
	}

	public boolean onCreateAnswer(Comment comment) {
		return this.saveComment(comment);
	}

	public void onCommentLike(Comment comment) {
		try {
			RatingsEntry ratingsEntry = serviceTrackerBean.getRatingsEntryService().fetchEntry(Long.valueOf(comment.getUser_id()),
					MBMessage.class.getName(), Long.valueOf(comment.getId()));
			if (ratingsEntry != null) {
				return;
			}

			long entryId = serviceTrackerBean.getCounterService().increment(RatingsEntry.class.getName());
			ratingsEntry = serviceTrackerBean.getRatingsEntryService().createRatingsEntry(entryId);
			ServiceContext serviceContext = ScrumUtil.getServiceContext();
			long companyId = serviceContext.getCompanyId();
			ratingsEntry.setClassName(MBMessage.class.getName());
			ratingsEntry.setClassPK(Long.valueOf(comment.getId()));
			ratingsEntry.setCompanyId(companyId);
			ratingsEntry.setCreateDate(new Date());
			ratingsEntry.setScore(5.0D);
			ratingsEntry.setUserId(Long.valueOf(comment.getUser_id()));
			ratingsEntry.setUserName(comment.getUser_username());
			serviceTrackerBean.getRatingsEntryService().addRatingsEntry(ratingsEntry);
		} catch (Exception var8) {
			LOG.error("Error creating ratings entry for comment " + comment.getId(), var8);
		}

	}

	public boolean onCommentEdit(Comment comment) {
		try {
			ServiceContext serviceContext = ScrumUtil.getServiceContext();
			serviceTrackerBean.getMbMessageService().updateDiscussionMessage(Long.parseLong(comment.getUser_id()),
					Long.parseLong(comment.getId()), Task.class.getName(), this.taskId, String.valueOf(this.taskId),
					comment.getComment_text(), serviceContext);
			return true;
		} catch (Exception var3) {
			LOG.error("Error updating comment with ID " + comment.getId(), var3);
			return false;
		}
	}

	public void onCommentDelete(Comment comment) {
		try {
			serviceTrackerBean.getMbMessageService().deleteDiscussionMessage(Long.parseLong(comment.getId()));
			Comment commentToDelete = null;
			Iterator var4 = this.comments.iterator();

			while (var4.hasNext()) {
				Comment c = (Comment) var4.next();
				if (c.getId().equals(comment.getId())) {
					commentToDelete = c;
				}
			}

			this.comments.remove(commentToDelete);
		} catch (Exception var5) {
			LOG.error("Error deleting comment with ID " + comment.getId(), var5);
		}

	}

	public void onPageChange(Long newPage) {
		this.commentsPage = newPage;
	}

	public Long getCommentCount() {
		return this.commentCount;
	}

	public void setCommentCount(Long commentCount) {
		this.commentCount = commentCount;
	}

	public Long getCommentsPerPage() {
		return this.commentsPerPage;
	}

	public List<Comment> getComments() {
		return this.comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public Long getCommentsPage() {
		return this.commentsPage;
	}

	public void setCommentsPage(Long commentsPage) {
		this.commentsPage = commentsPage;
	}

}
