package com.ncrowd.scrum.services.util;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.ncrowd.model.Task;
import com.ncrowd.model.WorkingHours;
import com.ncrowd.model.impl.WorkingHoursImpl;
import com.ncrowd.scrum.bean.PreferencesBean;
import com.ncrowd.scrum.bean.ServiceTrackerBean;
import com.ncrowd.scrum.model.ColorMode;
import com.ncrowd.scrum.servicetracker.TaskServiceTracker;
import com.ncrowd.scrum.servicetracker.WorkingHoursServiceTracker;
import com.ncrowd.scrum.util.FacesMessageUtil;
import com.ncrowd.scrum.util.ScrumUtil;
import com.ncrowd.service.TaskLocalService;
import com.ncrowd.service.WorkingHoursLocalService;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

@ManagedBean
@ViewScoped
public class TaskImplUtil {

	private WorkingHours timeToSave;
	private List<WorkingHours> workingHours;
	private Task groupTask;
	private double timeForCopy;
	private static String DEFAULT_COLOR = "#FCEE74";
	private static float DEFAULT_WORKING_HOURS = 4.0F;
	private static final Log LOG = LogFactoryUtil.getLog(TaskImplUtil.class);
	private Map<ColorMode, String> colorMap;
	@ManagedProperty("#{trackerBean}")
	private ServiceTrackerBean serviceTrackerBean;

	public void setServiceTrackerBean(ServiceTrackerBean serviceTrackerBean) {
		this.serviceTrackerBean = serviceTrackerBean;
	}

	private ServiceTracker<TaskLocalService, TaskLocalService> getTaskServiceTracker() {
		return new TaskServiceTracker(FrameworkUtil.getBundle(this.getClass()).getBundleContext());
	}

	private ServiceTracker<WorkingHoursLocalService, WorkingHoursLocalService> getWorkingHoursServiceTracker() {
		return new WorkingHoursServiceTracker(FrameworkUtil.getBundle(this.getClass()).getBundleContext());
	}

	public TaskLocalService getTaskService() {
		ServiceTracker<TaskLocalService, TaskLocalService> tracker = getTaskServiceTracker();
		tracker.open();
		return !tracker.isEmpty() ? tracker.getService() : null;
	}

	public WorkingHoursLocalService getWorkingHoursService() {
		ServiceTracker<WorkingHoursLocalService, WorkingHoursLocalService> tracker = getWorkingHoursServiceTracker();
		tracker.open();
		return !tracker.isEmpty() ? tracker.getService() : null;
	}

	public void saveWorkingHours(WorkingHours time, PreferencesBean prefBean) {
		if (time.isNew()) {
			long workingHoursId = serviceTrackerBean.getCounterService().increment(WorkingHours.class.getName());
			time.setWorkingHoursId(workingHoursId);
			time.setCompanyId(prefBean.getCompanyId());
			time.setGroupId(prefBean.getGroupId());
			time.setCreateDate(new Date());
			time.setUserGroupId(prefBean.getScrumGroupId());
			time.setUserId(prefBean.getUserId());
		}

		if (time.getStartDate() == null) {
			Date lastEndDate = this.getLastEndDateOfUserToday(prefBean.getUserId());
			if (lastEndDate != null) {
				time.setStartDate(lastEndDate);
			} else {
				time.setStartDate(ScrumUtil.getDefaultStartDate());
			}
		}

		time.persist();
//			prefBean.sendPushMessage("/updateTask", String.valueOf(this.getTaskId()));
	}

	public void saveWorkingHours(PreferencesBean prefBean) throws SystemException {
		this.saveWorkingHours(this.timeToSave, prefBean);
	}

	public void addNewTime() {
		FacesContext context = FacesContext.getCurrentInstance();
		Map<String, Object> sessionMap = context.getExternalContext().getSessionMap();
		PreferencesBean prefBean = (PreferencesBean) sessionMap.get("prefBean");
		WorkingHours newTime = serviceTrackerBean.getWorkingHoursService().createWorkingHours(serviceTrackerBean.getCounterService().increment(WorkingHours.class.getName()));
		newTime.setNew(true);
		newTime.setTaskId(groupTask.getTaskId());
		newTime.setUserId(prefBean.getUserId());
		newTime.setStartDate(ScrumUtil.getDefaultStartDate());
		newTime.setHours((double) DEFAULT_WORKING_HOURS);
		getWorkingHours().add(newTime);
	}

	public List<WorkingHours> getWorkingHours() {
		if (workingHours == null) {
			workingHours = new ArrayList<>();
			try {
				List<WorkingHours> loadedWorkingHours = serviceTrackerBean.getWorkingHoursService()
						.findByTaskId(groupTask.getTaskId());
				this.workingHours.addAll(loadedWorkingHours);
			} catch (SystemException e) {
				LOG.error("Failed to get the working hours for task " + groupTask.getTaskId(), e);
			}
		}

		return this.workingHours;
	}

	public float getTotalWorkingHours() {
		float totalHours = 0.0F;

		for (WorkingHours hours : getWorkingHours()) {
			totalHours = (float) ((double) totalHours + hours.getHours());
		}

		return totalHours;
	}

	private Date getLastEndDateOfUserToday(long userId) {
		List<WorkingHours> userHours = null;
		try {
			userHours = serviceTrackerBean.getWorkingHoursService().findByUserId(userId);
		} catch (SystemException var8) {
			LOG.error("Error getting working hours of user " + userId, var8);
			return null;
		}

		Date lastEndDate = null;
		Iterator<WorkingHours> uHoursItr = userHours.iterator();

		while (true) {
			Date endDate;
			do {
				WorkingHours hours;
				do {
					if (!uHoursItr.hasNext()) {
						return lastEndDate;
					}

					hours = uHoursItr.next();
				} while (!ScrumUtil.isToday(hours.getStartDate()));

				endDate = ScrumUtil.addHours(hours.getStartDate(), hours.getHours());
			} while (lastEndDate != null && !lastEndDate.before(endDate));

			lastEndDate = endDate;
		}
	}

	public void submitTimeForm() {
		if (this.timeToSave != null && this.timeToSave.getHours() > 0.0D) {
			FacesContext context = FacesContext.getCurrentInstance();
			Map<String, Object> sessionMap = context.getExternalContext().getSessionMap();
			PreferencesBean prefBean = (PreferencesBean) sessionMap.get("prefBean");

			try {
				this.saveWorkingHours(prefBean);
				FacesMessageUtil.addGlobalInfoMessage(context, "scrum-save-time-successful");
			} catch (SystemException var5) {
				LOG.error("Failed to save working hours for task " + groupTask.getTaskId(), var5);
				FacesMessageUtil.addGlobalErrorMessage(context, "scrum-save-time-failed");
			}

			this.timeToSave = null;
		}

	}

	public String getColor(ColorMode mode) {
		this.initColorMap();
		return (String) this.colorMap.get(mode);
	}

	private void initColorMap() {
		this.colorMap = new HashMap<>();
		String individualColor = DEFAULT_COLOR;
		String groupColor = "";
		if (groupTask.getTaskGroupId() != 0L) {
			groupColor = groupTask.getGroupTask().getIndividualColor();
		}

		if ("".equals(groupColor)) {
			groupColor = DEFAULT_COLOR;
		}

		this.colorMap.put(ColorMode.DEFAULT, DEFAULT_COLOR);
		this.colorMap.put(ColorMode.INDIVIDUAL, individualColor);
		this.colorMap.put(ColorMode.GROUP, groupColor);
	}
}
