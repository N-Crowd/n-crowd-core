package com.ncrowd.scrum.services.util;

public class TEst {
	public static void main(String[] args) {
		System.out.println("object is : "+getService(Long.class));
	}

	public static <T> T getService(Class<T> clz) {
		String className = clz.getName();
		if (className.contains("String")) {
			return (T) new String("sudhanshu");
		} else if (className.contains("Long")) {
			return (T) new Long(101);
		} else if (className.contains("Integer")) {
			return (T) new Integer(1);
		} else {
			return null;
		}
	}
}
