package com.ncrowd.scrum.servicetracker;

import com.liferay.message.boards.service.MBDiscussionLocalService;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

@SuppressWarnings("deprecation")
public class MBDiscussionServiceTracker extends ServiceTracker<MBDiscussionLocalService, MBDiscussionLocalService>{
	public MBDiscussionServiceTracker(BundleContext bundleContext) {
		super(bundleContext,MBDiscussionLocalService.class,null);
	}
}
