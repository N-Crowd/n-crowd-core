package com.ncrowd.scrum.constants;

public interface ScrumPortletConstants {
	public static String DEFAULT_COLOR = "#FCEE74";
	public static float DEFAULT_WORKING_HOURS = 4.0F;
}
