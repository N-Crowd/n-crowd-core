package com.ncrowd.scrum.servicetracker;

import com.ncrowd.service.StatusLocalService;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

public class StatusServiceTracker extends ServiceTracker<StatusLocalService, StatusLocalService>{

	public StatusServiceTracker(BundleContext bundleContext){
		super(bundleContext, StatusLocalService.class, null);
	}
	
	
}
