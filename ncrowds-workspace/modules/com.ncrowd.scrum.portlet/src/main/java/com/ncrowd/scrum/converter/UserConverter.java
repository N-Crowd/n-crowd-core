package com.ncrowd.scrum.converter;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.ncrowd.scrum.bean.ServiceTrackerBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

@ManagedBean(name="userConverter")
public class UserConverter implements Converter {
	private static final Log LOG = LogFactoryUtil.getLog(UserConverter.class);

	@ManagedProperty("#{trackerBean}")
	private ServiceTrackerBean serviceTrackerBean;

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		long userId = Long.parseLong(value);
		User user = null;
		try {
			user = serviceTrackerBean.getUserService().getUser(userId);
		} catch (Exception var8) {
			LOG.error("Error getting the user object", var8);
		}

		return user;
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		return String.valueOf(((User) value).getUserId());
	}

	public void setServiceTrackerBean(ServiceTrackerBean serviceTrackerBean) {
		this.serviceTrackerBean = serviceTrackerBean;
	}

}
