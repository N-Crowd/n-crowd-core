package com.ncrowd.scrum.bean;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;
import com.ncrowd.model.Sprint;
import com.ncrowd.model.Task;
import com.ncrowd.scrum.util.FacesMessageUtil;
import com.ncrowd.scrum.util.ScrumUtil;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.MeterGaugeChartModel;
import org.primefaces.model.chart.PieChartModel;

@ManagedBean(name = "statsBean")
@ViewScoped
public class StatsBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManagedProperty("#{prefBean}")
	private PreferencesBean prefBean;
	
	@ManagedProperty("#{trackerBean}")
	private ServiceTrackerBean serviceTrackerBean;
	
	@ManagedProperty("#{sprintBean}")
	private SprintBean sprintBean;
	private Sprint currentSprint;
	private long previousStatusSprintId;
	private long nextStatusSprintId;
	private double overtimeHours;
	private LineChartModel burndownChart;
	private MeterGaugeChartModel meterGaugeModel;
	private PieChartModel taskStatusChart;
	private List<Task> taskList;
	private static final Log LOG = LogFactoryUtil.getLog(StatsBean.class);

	public void setPrefBean(PreferencesBean prefBean) {
		this.prefBean = prefBean;
	}


	public void setServiceTrackerBean(ServiceTrackerBean serviceTrackerBean) {
		this.serviceTrackerBean = serviceTrackerBean;
	}
	
	public void setSprintBean(SprintBean sprintBean) {
		this.sprintBean = sprintBean;
	}

	public long getPreviousStatusSprintId() {
		if (currentSprint == null) {
			getStatusSprint();
		}

		return previousStatusSprintId;
	}

	public long getNextStatusSprintId() {
		return nextStatusSprintId;
	}

	public CartesianChartModel getBurndownChart() {
		prefBean = (PreferencesBean) ScrumUtil.getCurrentBean("prefBean");
		LOG.info("get burnDoencahrt is  : "+burndownChart);
		burndownChart = null;
		long currentSprintId = prefBean.getCurrentSprintId();
		LOG.info("get current sprint id is : "+currentSprintId);
		return getBurndownChart(currentSprintId);
	}

	public CartesianChartModel getBurndownChartStatus(long sprintId) {
		return getBurndownChart(sprintId);
	}

	private CartesianChartModel getBurndownChart(long sprintId) {
		if (burndownChart != null) {
			LOG.info("burndown chart is found : "+burndownChart);
			return burndownChart;
		} else {
			ThemeDisplay themeDisplay = ScrumUtil.getThemeDisplay();
			currentSprint =  sprintBean.getSprintMap().get(sprintId);
			burndownChart = new LineChartModel();
			ChartSeries taskSeries = new ChartSeries();
			taskSeries.setLabel(LanguageUtil.get(themeDisplay.getLocale(), "scrum-burndown-points"));
			if (currentSprint != null) {
				Calendar startDate = ScrumUtil.convertToCalendarWithoutTime(currentSprint.getStartDate());
				Calendar endDate = ScrumUtil.convertToCalendarWithoutTime(currentSprint.getEndDate());
				List<Task> taskLst = getTasks();
				Map<Date, Integer> fixedValues = getBurndownValues(taskLst);
				Integer currentValue = 0;
				int totalEvaluation = getTotalSprintEvaluation(taskLst);
				int pointsDone = 0;
				String dateFormat = ScrumUtil.getLocaleString("scrum-date-format-short", "dd.MM.");
				SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
				Calendar date = (Calendar) startDate.clone();
				Calendar now = Calendar.getInstance();

				while (date.before(endDate)) {
					String dateString = sdf.format(date.getTime());
					if (date.after(now)) {
						currentValue = null;
					} else if (date.equals(startDate)) {
						currentValue = totalEvaluation;
					} else {
						int dateValue;
						if (date.equals(endDate)) {
							dateValue = getDateValueOrZero(fixedValues, date.getTime());
							dateValue += getDateValueOrZero(fixedValues, ScrumUtil.addDays(date.getTime(), 1));
							currentValue = currentValue - dateValue;
							pointsDone += dateValue;
						} else {
							dateValue = getDateValueOrZero(fixedValues, date.getTime());
							currentValue = currentValue - dateValue;
							pointsDone += dateValue;
						}
					}

					taskSeries.set(dateString, currentValue);
					date.add(5, 1);
				}

				createMeterGauge(totalEvaluation, pointsDone);
			}

			if (taskSeries.getData().size() == 0) {
				taskSeries.set(0, 0);
			}

			burndownChart.addSeries(taskSeries);
			return burndownChart;
		}
	}

	private int getDateValueOrZero(Map<Date, Integer> fixedValues, Date date) {
		return fixedValues.get(date) != null ? (Integer) fixedValues.get(date) : 0;
	}

	private List<Task> getTasks() {
		List<Task> taskLst = new ArrayList<>();

		try {
			List<Task> loadedTasks = serviceTrackerBean.getTaskService().findByG_S(prefBean.getScrumGroupId(),
					getStatusSprint().getSprintId());
			Iterator<Task> loadedTaskItr = loadedTasks.iterator();

			while (loadedTaskItr.hasNext()) {
				Task task = loadedTaskItr.next();
				taskLst.add(task);
			}
		} catch (SystemException var5) {
			LOG.error("Error getting the tasks of group " + prefBean.getScrumGroupId(), var5);
		}

		return taskLst;
	}

	private int getTotalSprintEvaluation(List<Task> taskList) {
		int total = 0;

		Task task;
		for (Iterator<Task> takItr = taskList.iterator(); takItr
				.hasNext(); total = (int) ((double) total + task.getEvaluation())) {
			task = takItr.next();
		}

		return total;
	}

	private Map<Date, Integer> getBurndownValues(List<Task> taskList) {
		Map<Date, Integer> values = new HashMap<>();
		Iterator<Task> taskItr = taskList.iterator();
		while (taskItr.hasNext()) {
			Task task = taskItr.next();
			Date fixedSince = task.getFixedSince();
			if (task.getStatus().isFixedStatus() && fixedSince != null) {
				Date saveDate = ScrumUtil.addDays(fixedSince, 1);
				saveDate = ScrumUtil.removeTime(saveDate);
				if (values.get(saveDate) != null) {
					int currentValue = (Integer) values.get(saveDate);
					values.put(saveDate, (int) ((double) currentValue + task.getEvaluation()));
				} else {
					values.put(saveDate, (int) task.getEvaluation());
				}
			}
		}

		return values;
	}

	public MeterGaugeChartModel getMeterGauge() {
		if (meterGaugeModel == null) {
			getBurndownChart(currentSprint.getSprintId());
		}

		return meterGaugeModel;
	}

	private void createMeterGauge(int totalEvaluation, int currentValue) {
		List<Number> intervals = new ArrayList<>();
		intervals.add((float) totalEvaluation / 4.0F);
		intervals.add((float) totalEvaluation / 2.0F);
		intervals.add((float) (totalEvaluation * 3) / 4.0F);
		intervals.add(totalEvaluation);
		meterGaugeModel = new MeterGaugeChartModel(currentValue, intervals);
	}

	public PieChartModel getTaskStatusChart() {
		if (taskStatusChart == null) {
			createTaskStatusChart();
		}

		return taskStatusChart;
	}

	private void createTaskStatusChart() {
		Map<String, Number> chartModel = new HashMap<>();
		List<Task> tasks = getTasks();
		for (Task task : tasks) {
			String statusName = task.getStatus().getName();
			if (chartModel.containsKey(statusName)) {
				int oldValue = (Integer) chartModel.get(statusName);
				++oldValue;
				chartModel.put(statusName, oldValue);
			} else {
				chartModel.put(statusName, 1);
			}
		}

		taskStatusChart = new PieChartModel(chartModel);
	}

	public List<Task> getTaskList() {
		if (taskList == null) {
			taskList = getTasks();
			setOvertimeHours();
		}

		return taskList;
	}

	public double getOvertimeHours() {
		getTaskList();
		return overtimeHours;
	}

	public int getTaskCount() {
		return getTaskList().size();
	}

	public void setOvertimeHours() {
		overtimeHours = 0.0D;
		List<Task> tskList = getTaskList();
		Iterator<Task> taskItr = tskList.iterator();

		while (true) {
			Task task;
			double taskOvertime;
			do {
				if (!taskItr.hasNext()) {
					return;
				}

				task = (Task) taskItr.next();
				taskOvertime = (double) task.getTotalWorkingHours() - task.getEvaluation();
			} while (taskOvertime <= 0.0D && (taskOvertime >= 0.0D || !task.getStatus().isFixedStatus()));

			overtimeHours += taskOvertime;
		}
	}

	public Sprint getStatusSprint() {
		if (Validator.isNotNull(currentSprint)) {
			return currentSprint;
		} else {
			PortletRequest portletRequest = ScrumUtil.getPortletRequest();
			HttpServletRequest request = PortalUtil.getHttpServletRequest(portletRequest);
			String sprintIdString = PortalUtil.getOriginalServletRequest(request)
					.getParameter("_comncrowdscrumportlet_WAR_comncrowdscrumportlet_sprintId");
			if (!Validator.isNull(sprintIdString) && Validator.isNumber(sprintIdString)) {
				currentSprint =  sprintBean.getSprintMap().get(Long.valueOf(sprintIdString));
				setNextPrevSprintId();
				return currentSprint;
			} else {
				FacesMessageUtil.addGlobalErrorMessage(FacesContext.getCurrentInstance(), "scrum-load-sprint-failed");
				return null;
			}
		}
	}

	private void setNextPrevSprintId() {
		nextStatusSprintId = 0L;
		previousStatusSprintId = 0L;
		Iterator<Sprint> var2 = sprintBean.getSprintMap().values().iterator();

		while (var2.hasNext()) {
			Sprint sprint = var2.next();
			if (sprint.getSprintNumber() == currentSprint.getSprintNumber() + 1) {
				nextStatusSprintId = sprint.getSprintId();
			} else if (sprint.getSprintNumber() == currentSprint.getSprintNumber() - 1) {
				previousStatusSprintId = sprint.getSprintId();
			}
		}

	}

	public String getStatusSprintTitle() {
		getStatusSprint();
		return currentSprint != null ? "Sprint " + currentSprint.getSprintNumber() : "";
	}

	public void loadNextStatusSprint() {
		currentSprint =  sprintBean.getSprintMap().get(nextStatusSprintId);
		setNextPrevSprintId();
		reset();
	}

	public void loadPreviousStatusSprint() {
		currentSprint =  sprintBean.getSprintMap().get(previousStatusSprintId);
		setNextPrevSprintId();
		reset();
	}

	public void reset() {
		burndownChart = null;
		meterGaugeModel = null;
		taskStatusChart = null;
		overtimeHours = 0.0D;
		taskList = null;
	}

}
