package com.ncrowd.scrum.servicetracker;

import com.ncrowd.service.DocumentMappingLocalService;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

public class DocumentMappingServiceTracker extends ServiceTracker<DocumentMappingLocalService, DocumentMappingLocalService>{

	public DocumentMappingServiceTracker(BundleContext bundleContext){
		super(bundleContext, DocumentMappingLocalService.class, null);
	}
	
}
