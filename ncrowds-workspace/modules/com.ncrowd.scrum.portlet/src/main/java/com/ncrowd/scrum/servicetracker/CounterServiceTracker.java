package com.ncrowd.scrum.servicetracker;

import com.liferay.counter.kernel.service.CounterLocalService;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

public class CounterServiceTracker extends ServiceTracker<CounterLocalService, CounterLocalService>{
	public CounterServiceTracker(BundleContext bundleContext) {
		super(bundleContext,CounterLocalService.class,null);
	}
}
