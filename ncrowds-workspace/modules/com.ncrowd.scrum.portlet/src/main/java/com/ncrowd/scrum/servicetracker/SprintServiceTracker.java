package com.ncrowd.scrum.servicetracker;

import com.ncrowd.service.SprintLocalService;
import com.ncrowd.service.impl.SprintLocalServiceImpl;

import org.osgi.framework.BundleContext;
import org.osgi.framework.Filter;
import org.osgi.util.tracker.ServiceTracker;

public class SprintServiceTracker extends ServiceTracker<SprintLocalService, SprintLocalServiceImpl>{

	public SprintServiceTracker(BundleContext context){
		super(context, SprintLocalService.class, null);
	}
	
}
