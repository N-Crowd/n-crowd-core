package com.ncrowd.scrum.bean;

import com.liferay.document.library.kernel.exception.DuplicateFileException;
import com.liferay.document.library.kernel.exception.NoSuchFolderException;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFileShortcut;
import com.liferay.document.library.kernel.model.DLFileVersion;
import com.liferay.document.library.kernel.util.DLUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.UserGroup;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.FileVersion;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.FileUtil;
import com.ncrowd.model.DocumentMapping;
import com.ncrowd.model.Task;
import com.ncrowd.scrum.model.Document;
import com.ncrowd.scrum.model.TempFile;
import com.ncrowd.scrum.util.FacesMessageUtil;
import com.ncrowd.scrum.util.ScrumUtil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.UploadedFile;

@ManagedBean(name = "documentBean")
@ViewScoped
public class DocumentBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManagedProperty("#{editTaskBean}")
	private EditTaskBean editTaskBean;

	@ManagedProperty("#{prefBean}")
	private PreferencesBean prefBean;
	private transient TempFile fileToReplace;
	private static final String SCRUM_FOLDER_NAME = "ScrumPortlet";
	private static final Log LOG = LogFactoryUtil.getLog(DocumentBean.class);
	private long taskId;
	private long teamFolderId;
	private transient List<Document> documents;
	@ManagedProperty("#{trackerBean}")
	private ServiceTrackerBean serviceTrackerBean;

	public void setEditTaskBean(EditTaskBean editTaskBean) {
		this.editTaskBean = editTaskBean;
	}

	public void setPrefBean(PreferencesBean prefBean) {
		this.prefBean = prefBean;
	}

	public void setServiceTrackerBean(ServiceTrackerBean serviceTrackerBean) {
		this.serviceTrackerBean = serviceTrackerBean;
	}

	
	@PostConstruct
	public void init() {
		fileToReplace = null;
		Task task = editTaskBean.getCurrentTask();
		if (task != null) {
			taskId = task.getTaskId();
			loadTaskDocuments();
		} else {
			documents = new ArrayList<>();
		}

		try {
			checkDLFolders();
		} catch (Exception var3) {
			LOG.error("Error checking for scrum DLFolders", var3);
		}

	}

	@PreDestroy
	public void destroy() {
		if (fileToReplace != null) {
			try {
				fileToReplace.getFile().delete();
			} catch (Exception e) {
				LOG.error("Exception occurs : "+e);
			}
		}

	}

	public TempFile getFileToReplace() {
		return fileToReplace;
	}

	public void setFileToReplace(TempFile fileToReplace) {
		if (fileToReplace != null) {
			try {
				this.fileToReplace.getFile().delete();
			} catch (Exception var3) {
				LOG.error("Error deleting temp file", var3);
			}
		}

		this.fileToReplace = fileToReplace;
	}

	private void loadTaskDocuments() {
		documents = new ArrayList<>();

		try {
			List<DocumentMapping> mappings = serviceTrackerBean.getDocumentService().findByTaskId(taskId);
			for (DocumentMapping mapping : mappings) {
				addDocument(mapping.getMappingId(), mapping.getDlFileEntryId());
			}
		} catch (SystemException var4) {
			LOG.error("Error getting document mappings of task " + taskId, var4);
		}

	}

	private void addDocument(long mappingId, long fileEntryId) {
		try {
			DLFileEntry dlFileEntry = serviceTrackerBean.getDlFileService().fetchDLFileEntry(fileEntryId);
			if (dlFileEntry == null) {
				LOG.debug("DLFileEntry " + fileEntryId + " not found. Deleting mapping " + mappingId);
				deleteMapping(mappingId);
				return;
			}

			ThemeDisplay themeDisplay = ScrumUtil.getThemeDisplay();
			Document document = convertDLFileEntryToDocument(dlFileEntry, themeDisplay, mappingId);
			if (document != null) {
				documents.add(document);
			}
		} catch (SystemException var8) {
			LOG.error("Error getting DLFileEntry " + fileEntryId, var8);
		}

	}

	private Document convertDLFileEntryToDocument(DLFileEntry dlFileEntry, ThemeDisplay themeDisplay, long mappingId) {
		String thumbnailSrc = getThumbnailSrc(dlFileEntry, themeDisplay);
		String previewSrc = getPreviewURL(dlFileEntry, themeDisplay);
		Document document = new Document(dlFileEntry, thumbnailSrc, previewSrc, mappingId);
		document.setOldVersions(getOldDocumentVersions(dlFileEntry, themeDisplay));
		return document;
	}
	
	public StreamedContent getFile(long documentId,String title,String mimeType) throws PortalException, IOException {
		DLFileEntry dlFileEntry = serviceTrackerBean.getDlFileService().fetchDLFileEntry(documentId);
		if (dlFileEntry != null) {
			InputStream stream = dlFileEntry.getContentStream();
			ScrumUtil.writeStreamToOutputStream(stream,title, mimeType);
		}

		return null;
	}

	private List<Document> getOldDocumentVersions(DLFileEntry dlFileEntry, ThemeDisplay themeDisplay) {
		List<Document> oldVersions = new ArrayList<>();

		try {
			List<DLFileVersion> dlFileVersions = dlFileEntry.getFileVersions(-1);
			for (DLFileVersion version : dlFileVersions) {
				if (!version.getVersion().equals(dlFileEntry.getVersion())) {
					String thumbnailSrc = getThumbnailSrc(dlFileEntry, version, themeDisplay);
					String previewSrc = getPreviewURL(dlFileEntry, version, themeDisplay);
					oldVersions.add(new Document(version, thumbnailSrc, previewSrc));
				}
			}
		} catch (SystemException var9) {
			LOG.error("Error getting versions of DLFileEntry " + dlFileEntry.getFileEntryId(), var9);
		}

		return oldVersions;
	}

	public void deleteMapping(long mappingId) {
		try {
			serviceTrackerBean.getDocumentService().deleteDocumentMapping(mappingId);
		} catch (Exception var4) {
			LOG.error("Error deleting document mapping " + mappingId, var4);
		}

	}

	public List<Document> getDocuments() {
		return documents;
	}

	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

	public TreeNode getDocumentTree() {
		TreeNode rootNode = new DefaultTreeNode("root", (TreeNode) null);
		Iterator var3 = this.documents.iterator();

		while (var3.hasNext()) {
			Document document = (Document) var3.next();
			TreeNode documentNode = new DefaultTreeNode(document, rootNode);
			documentNode.setExpanded(true);
			Iterator var6 = document.getOldVersions().iterator();

			while (var6.hasNext()) {
				Document oldVersion = (Document) var6.next();
				new DefaultTreeNode(oldVersion, documentNode);
			}
		}

		return rootNode;
	}

	private String getThumbnailSrc(DLFileEntry dlFileEntry, ThemeDisplay themeDisplay) {
		try {
			DLFileVersion dlFileVersion = dlFileEntry.getFileVersion();
			return this.getThumbnailSrc(dlFileEntry, dlFileVersion, themeDisplay);
		} catch (Exception var4) {
			LOG.error("Error getting current DLFileVersion of DLFileEntry " + dlFileEntry.getFileEntryId(), var4);
			return "";
		}
	}

	private String getThumbnailSrc(DLFileEntry dlFileEntry, DLFileVersion dlFileVersion, ThemeDisplay themeDisplay) {
		FileVersion fileVersion = null;
		FileEntry fileEntry = null;

		try {
			fileEntry = serviceTrackerBean.getDlAppService().getFileEntry(dlFileEntry.getFileEntryId());
			fileVersion = serviceTrackerBean.getDlAppService().getFileVersion(dlFileVersion.getFileVersionId());
			return DLUtil.getThumbnailSrc(fileEntry, fileVersion, (DLFileShortcut) null, themeDisplay);
		} catch (Exception var7) {
			LOG.error("Error getting thumbnail src for DLFileEntry " + dlFileEntry.getFileEntryId(), var7);
			return "";
		}
	}

	private String getPreviewURL(DLFileEntry dlFileEntry, ThemeDisplay themeDisplay) {
		try {
			DLFileVersion dlFileVersion = dlFileEntry.getFileVersion();
			return this.getPreviewURL(dlFileEntry, dlFileVersion, themeDisplay);
		} catch (Exception var4) {
			LOG.error("Error getting current DLFileVersion of DLFileEntry " + dlFileEntry.getFileEntryId(), var4);
			return "";
		}
	}

	private String getPreviewURL(DLFileEntry dlFileEntry, DLFileVersion dlFileVersion, ThemeDisplay themeDisplay) {
		FileVersion fileVersion = null;
		FileEntry fileEntry = null;

		try {
			fileEntry = serviceTrackerBean.getDlAppService().getFileEntry(dlFileEntry.getFileEntryId());
			fileVersion = serviceTrackerBean.getDlAppService().getFileVersion(dlFileVersion.getFileVersionId());
			return DLUtil.getPreviewURL(fileEntry, fileVersion, themeDisplay, "");
		} catch (Exception var7) {
			LOG.error("Error getting thumbnail src for DLFileEntry " + dlFileEntry.getFileEntryId(), var7);
			return "";
		}
	}

	public void handleFileUpload(FileUploadEvent event) {
		UploadedFile file = event.getFile();

		try {
			ServiceContext serviceContext = ScrumUtil.getServiceContext();
			long fileEntryId = this.createFile(serviceContext, file);
			this.createMapping(serviceContext, fileEntryId);
		} catch (DuplicateFileException var6) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessageUtil.addInfoMessage("uploadForm", context, "scrum-document-already-exists", file.getFileName());
			this.saveFileToReplace(file);
		} catch (Exception var7) {
			LOG.error("Error while uploading a file", var7);
		}

	}

	private void saveFileToReplace(UploadedFile uploadedFile) {
		try {
			File file = FileUtil.createTempFile(uploadedFile.getInputstream());
			TempFile tempFile = new TempFile(file, uploadedFile.getFileName(), uploadedFile.getContentType(),
					uploadedFile.getSize());
			tempFile.setAlreadyAssigned(this.isDocumentAssignedToTask(uploadedFile.getFileName()));
			this.fileToReplace = tempFile;
		} catch (Exception var4) {
			LOG.error("Error saving temp file to replace", var4);
		}

	}

	private boolean isDocumentAssignedToTask(String fileName) {
		Iterator var3 = this.documents.iterator();

		while (var3.hasNext()) {
			Document document = (Document) var3.next();
			if (document.getTitle().equals(fileName)) {
				return true;
			}
		}

		return false;
	}

	private void createMapping(ServiceContext serviceContext, long fileEntryId) throws SystemException {
		if (this.existsMapping(fileEntryId)) {
			LOG.debug("Document mapping already exists: taskId " + this.taskId + ", fileEntryId " + fileEntryId);
		} else {
			long mappingId = serviceTrackerBean.getCounterService().increment(DocumentMapping.class.getName());
			DocumentMapping mapping = serviceTrackerBean.getDocumentService().createDocumentMapping(mappingId);
			mapping.setCompanyId(serviceContext.getCompanyId());
			mapping.setDlFileEntryId(fileEntryId);
			mapping.setGroupId(serviceContext.getScopeGroupId());
			mapping.setTaskId(this.taskId);
			mapping.setUserGroupId(this.prefBean.getScrumGroupId());
			serviceTrackerBean.getDocumentService().addDocumentMapping(mapping);
			this.addDocument(mappingId, fileEntryId);
		}
	}

	private boolean existsMapping(long fileEntryId) {
		try {
			List<DocumentMapping> documentMappings = serviceTrackerBean.getDocumentService().findByT_D(this.taskId,
					fileEntryId);
			return documentMappings.size() > 0;
		} catch (SystemException var4) {
			LOG.error("Error getting DocumentMapping for taskId " + this.taskId + ", fileEntryId " + fileEntryId);
			return false;
		}
	}

	private void checkDLFolders() throws PortalException, SystemException {
		ServiceContext serviceContext = ScrumUtil.getServiceContext();
		long groupId = serviceContext.getScopeGroupId();
		long scrumFolderId = 0L;

		try {
			scrumFolderId = serviceTrackerBean.getDlFolderService().getFolder(groupId, 0L, SCRUM_FOLDER_NAME).getFolderId();
		} catch (NoSuchFolderException var10) {
			scrumFolderId = this.createFolder(serviceContext, SCRUM_FOLDER_NAME, 0L);
		}

		long teamFolderId = 0L;
		if (scrumFolderId != 0L) {
			String teamFolderName = this.createTeamFolderName();

			try {
				teamFolderId = serviceTrackerBean.getDlFolderService().getFolder(groupId, scrumFolderId, teamFolderName).getFolderId();
			} catch (NoSuchFolderException var9) {
				teamFolderId = this.createFolder(serviceContext, teamFolderName, scrumFolderId);
			}
		}

		if (scrumFolderId != 0L && teamFolderId != 0L) {
			this.teamFolderId = teamFolderId;
		} else {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessageUtil.addErrorMessage("uploadForm", context, "scrum-load-folders-failed");
		}

	}

	private String createTeamFolderName() throws SystemException {
		long userGroupId = this.prefBean.getScrumGroupId();
		UserGroup userGroup = serviceTrackerBean.getUserGroupService().fetchUserGroup(userGroupId);
		String userGroupName = "";
		if (userGroup != null) {
			userGroupName = userGroup.getName() + "_";
		}

		return userGroupName + String.valueOf(userGroupId);
	}

	private long createFolder(ServiceContext serviceContext, String folderName, long parentFolderId)
			throws SystemException, PortalException {
		long groupId = serviceContext.getScopeGroupId();
		String[] guestPermissions = new String[] { "ACCESS" };
		serviceContext.setGuestPermissions(guestPermissions);
		serviceContext.setAddGuestPermissions(true);
		LOG.info("serviceTrackerBean.getDlAppService() : "+serviceTrackerBean);
		LOG.info(".getDlAppService() : "+serviceTrackerBean.getDlAppService());
		Folder folder = serviceTrackerBean.getDlAppService().addFolder(serviceContext.getUserId(), groupId, parentFolderId, folderName,
				"", serviceContext);
		return folder.getFolderId();
	}

	private long createFile(ServiceContext serviceContext, UploadedFile file)
			throws SystemException, PortalException, IOException {
		String fileName = file.getFileName();
		String[] guestPermissions = new String[] { "VIEW" };
		serviceContext.setGuestPermissions(guestPermissions);
		serviceContext.setAddGuestPermissions(true);
		FileEntry fileEntry = serviceTrackerBean.getDlAppService().addFileEntry(serviceContext.getUserId(),
				serviceContext.getScopeGroupId(), this.teamFolderId, fileName, file.getContentType(), fileName, "", "",
				file.getInputstream(), file.getSize(), serviceContext);
		return fileEntry.getFileEntryId();
	}

	private long createFile(ServiceContext serviceContext, TempFile file)
			throws SystemException, PortalException, IOException {
		String fileName = file.getFileName();
		String[] guestPermissions = new String[] { "VIEW" };
		serviceContext.setGuestPermissions(guestPermissions);
		serviceContext.setAddGuestPermissions(true);
		FileEntry fileEntry = serviceTrackerBean.getDlAppService().addFileEntry(serviceContext.getUserId(),
				serviceContext.getScopeGroupId(), this.teamFolderId, fileName, file.getMimeType(), fileName, "", "",
				file.getFile(), serviceContext);
		return fileEntry.getFileEntryId();
	}

	public void useExistingFile(ActionEvent event) {
		LOG.debug("use existing file " + this.fileToReplace.getFileName());

		try {
			ServiceContext serviceContext = ScrumUtil.getServiceContext();
			FileEntry existingFile = serviceTrackerBean.getDlAppService().getFileEntry(serviceContext.getScopeGroupId(),
					this.teamFolderId, this.fileToReplace.getFileName());
			this.createMapping(serviceContext, existingFile.getFileEntryId());
		} catch (Exception var4) {
			LOG.error("Error linking existing file with task", var4);
		}

		this.setFileToReplace((TempFile) null);
	}

	public void replaceFile(ActionEvent event) {
		LOG.debug("replace file " + this.fileToReplace.getFileName());
		try {
			String fileName = this.fileToReplace.getFileName();
			ServiceContext serviceContext = ScrumUtil.getServiceContext();
			FileEntry existingFile = serviceTrackerBean.getDlAppService().getFileEntry(serviceContext.getScopeGroupId(),
					this.teamFolderId, this.fileToReplace.getFileName());
			serviceTrackerBean.getDlAppService().updateFileEntry(serviceContext.getUserId(), existingFile.getFileEntryId(), fileName,
					this.fileToReplace.getMimeType(), fileName, "", "", false, this.fileToReplace.getFile(),
					serviceContext);
			this.createMapping(serviceContext, existingFile.getFileEntryId());
		} catch (Exception var5) {
			LOG.error("Error linking existing file with task", var5);
		}

		this.setFileToReplace((TempFile) null);
		this.loadTaskDocuments();
	}

	public void keepFile(ActionEvent event) {
		LOG.debug("keep file " + this.fileToReplace.getFileName());
		String fileName = this.fileToReplace.getFileName();

		try {
			ServiceContext serviceContext = ScrumUtil.getServiceContext();
			fileName = this.getUniqueFileName(fileName, serviceContext);
			this.fileToReplace.setFileName(fileName);
			long fileEntryId = this.createFile(serviceContext, this.fileToReplace);
			this.createMapping(serviceContext, fileEntryId);
		} catch (Exception var6) {
			LOG.error("Error adding file with modified name", var6);
		}

		this.setFileToReplace((TempFile) null);
	}

	private String getUniqueFileName(String fileName, ServiceContext serviceContext) throws SystemException {
		boolean uniqueName = false;
		int counter = 2;
		int pointIndex = fileName.lastIndexOf('.');
		String extension = fileName.substring(pointIndex);
		String oldFileName = fileName.substring(0, pointIndex);
		String counterString = "";

		do {
			counterString = " (" + counter + ")";

			try {
				serviceTrackerBean.getDlAppService().getFileEntry(serviceContext.getScopeGroupId(), this.teamFolderId,
						oldFileName + counterString + extension);
			} catch (PortalException var9) {
				uniqueName = true;
			}

			++counter;
		} while (!uniqueName && counter <= 100);

		return oldFileName + counterString + extension;
	}

	public void deleteMappingFromTable(long mappingId) {
		if (mappingId != 0L) {
			this.deleteMapping(mappingId);
			this.loadTaskDocuments();
		}

	}

}
