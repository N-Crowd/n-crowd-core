package com.ncrowd.scrum.bean;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.ncrowd.model.Sprint;
import com.ncrowd.model.Task;
import com.ncrowd.scrum.util.FacesMessageUtil;
import com.ncrowd.scrum.util.ScrumUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

@ManagedBean(name = "sprintBean")
@ViewScoped
public class SprintBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ManagedProperty("#{prefBean}")
	private PreferencesBean prefBean;
	private ScheduleModel sprintSchedule;
	private Map<Long, Sprint> sprintMap;
	private int sprintDays;
	private static final int DEFAULT_SPRINT_LENGTH = 14;
	private static final Log LOG = LogFactoryUtil.getLog(SprintBean.class);
	@ManagedProperty("#{trackerBean}")
	private ServiceTrackerBean serviceTrackerBean;
	public SprintBean() {
		this.sprintDays = DEFAULT_SPRINT_LENGTH;
		
	}


	public void setServiceTrackerBean(ServiceTrackerBean serviceTrackerBean) {
		this.serviceTrackerBean = serviceTrackerBean;
	}

	
	@PostConstruct
	public void init() {
		sprintSchedule = new DefaultScheduleModel();
		System.out.println("sprint schedule is loaded."+sprintSchedule);
		try {
			sprintMap = new TreeMap<>();
				List<Sprint> loadedSprints = serviceTrackerBean.getSprintService().findByUserGroupId(this.prefBean.getScrumGroupId());
				int lastSprintNumber = 0;
				long lastSprintId = 0L;
	
				Sprint sprint;
				for (Iterator<Sprint> loadSprintItr = loadedSprints.iterator(); loadSprintItr.hasNext(); this.sprintMap.put(sprint.getSprintId(),
						sprint)) {
					sprint = loadSprintItr.next();
					if (sprint.getSprintNumber() > lastSprintNumber) {
						lastSprintNumber = sprint.getSprintNumber();
						lastSprintId = sprint.getSprintId();
					}
				}
				if (this.prefBean.getCurrentSprintId() == -1L) {
					this.prefBean.setCurrentSprintId(lastSprintId);
				}
		} catch (SystemException e) {
			LOG.error("Error getting sprints of group " + this.prefBean.getScrumGroupId(), e);
		}

		this.initSchedule();
	}

	private void initSchedule() {
		sprintSchedule.clear();
		Iterator<Sprint> sprintItr = this.sprintMap.values().iterator();
		while (sprintItr.hasNext()) {
			Sprint sprint = sprintItr.next();
			DefaultScheduleEvent event = new DefaultScheduleEvent();
			event.setData(sprint.getSprintId());
			event.setAllDay(true);
			event.setEditable(true);
			event.setStartDate(sprint.getStartDate());
			event.setEndDate(sprint.getEndDate());
			event.setTitle("Sprint " + sprint.getSprintNumber());
			sprintSchedule.addEvent(event);
		}
	}

	public void setPrefBean(PreferencesBean prefBean) {
		this.prefBean = prefBean;
	}

	public ScheduleModel getSprintSchedule() {
		return this.sprintSchedule;
	}

	public void setSprintSchedule(ScheduleModel sprintSchedule) {
		this.sprintSchedule = sprintSchedule;
	}

	public Map<Long, Sprint> getSprintMap() {
		return this.sprintMap;
	}

	public void setSprintMap(Map<Long, Sprint> sprintMap) {
		this.sprintMap = sprintMap;
	}

	public String getSprintNumber(String sprintIdString) {
		long sprintId = Long.parseLong(sprintIdString);
		return sprintId == 0L ? "Backlog" : String.valueOf(( this.sprintMap.get(sprintId)).getSprintNumber());
	}

	public List<Sprint> getSprintList() {
		return new ArrayList<>(this.sprintMap.values());
	}

	public int getSprintDays() {
		return this.sprintDays;
	}

	public void setSprintDays(int sprintDays) {
		this.sprintDays = sprintDays;
	}

	public void onEventMove(ScheduleEntryMoveEvent event) {
		ScheduleEvent scheduleEvent = event.getScheduleEvent();
		this.updateSprint(scheduleEvent);
	}

	public void onEventResize(ScheduleEntryResizeEvent event) {
		ScheduleEvent scheduleEvent = event.getScheduleEvent();
		this.updateSprint(scheduleEvent);
	}

	public void onEventSelect(SelectEvent event) {
		DefaultScheduleEvent scheduleEvent = (DefaultScheduleEvent) event.getObject();
		long statusSprintId = (Long) scheduleEvent.getData();
		String redirect = "/WEB-INF/views/sprintStatus.xhtml";
		Map<String, String> params = new HashMap<>();
		params.put("sprintId", String.valueOf(statusSprintId));

		try {
			ScrumUtil.doRedirect(redirect, params);
		} catch (Exception var7) {
			FacesMessageUtil.addGlobalErrorMessage(FacesContext.getCurrentInstance(), "scrum-load-sprint-failed");
		}

	}

	private void updateSprint(ScheduleEvent event) {
		Sprint sprint = (Sprint) this.sprintMap.get(event.getData());
		sprint.setStartDate(event.getStartDate());
		sprint.setEndDate(event.getEndDate());

		try {
			sprint.persist();
		} catch (SystemException var4) {
			LOG.error("Error updating sprint", var4);
		}

	}

	public void addSprint() {
		try {
			long sprintId = serviceTrackerBean.getCounterService().increment(Sprint.class.getName());
			Sprint sprint = serviceTrackerBean.getSprintService().createSprint(sprintId);
			initSprintNumberAndDate(sprint);
			sprint.setCompanyId(this.prefBean.getCompanyId());
			sprint.setGroupId(this.prefBean.getGroupId());
			sprint.setUserGroupId(this.prefBean.getScrumGroupId());
			sprint.persist();
			sprintMap.put(sprintId, sprint);
			initSchedule();
		} catch (SystemException var4) {
			LOG.error("Failed to create new sprint", var4);
		}

	}

	private void initSprintNumberAndDate(Sprint newSprint) throws SystemException {
		int sprintNumber = 0;
		Date lastSprintEnd = null;
		List<Sprint> newStartDateList =null;
		try {
			this.sprintMap = new TreeMap<>();
			newStartDateList= serviceTrackerBean.getSprintService().findByUserGroupId(this.prefBean.getScrumGroupId());
			Iterator<Sprint> nDateItr = newStartDateList.iterator();

			label33: while (true) {
				Sprint sprint;
				do {
					if (!nDateItr.hasNext()) {
						break label33;
					}

					sprint =  nDateItr.next();
					this.sprintMap.put(sprint.getSprintId(), sprint);
					if (sprint.getSprintNumber() > sprintNumber) {
						sprintNumber = sprint.getSprintNumber();
					}
				} while (lastSprintEnd != null && !sprint.getEndDate().after(lastSprintEnd));

				lastSprintEnd = sprint.getEndDate();
			}
		} catch (SystemException var7) {
			LOG.error("Error getting sprints of group " + this.prefBean.getScrumGroupId(), var7);
		}

		Date newStartDate=null;
		if (lastSprintEnd != null) {
			Calendar newStartCalendar = Calendar.getInstance();
			newStartCalendar.setTime(lastSprintEnd);
			newStartCalendar.add(5, 1);
			newStartDate = newStartCalendar.getTime();
		} else {
			newStartDate = ScrumUtil.getDefaultStartDate();
		}

		Date newEndDate = ScrumUtil.addDays(newStartDate, this.sprintDays - 1);
		newSprint.setStartDate(newStartDate);
		newSprint.setEndDate(newEndDate);
		newSprint.setSprintNumber(sprintNumber + 1);
	}

	public String deleteSprint(long sprintId) {
		try {
			List<Task> taskList = serviceTrackerBean.getTaskService().findByG_S(this.prefBean.getScrumGroupId(), sprintId);
			if (taskList.isEmpty()) {
				serviceTrackerBean.getSprintService().deleteSprint(sprintId);
				FacesMessageUtil.addGlobalInfoMessage(FacesContext.getCurrentInstance(),
						"scrum-delete-sprint-successful");
			} else {
				FacesMessageUtil.addGlobalErrorMessage(FacesContext.getCurrentInstance(), "scrum-delete-sprint-failed");
			}
		} catch (Exception var4) {
			LOG.error("Failed to delete sprint with ID " + sprintId, var4);
			FacesMessageUtil.addGlobalErrorMessage(FacesContext.getCurrentInstance(), "scrum-delete-sprint-failed");
		}

		return "view";
	}
}
