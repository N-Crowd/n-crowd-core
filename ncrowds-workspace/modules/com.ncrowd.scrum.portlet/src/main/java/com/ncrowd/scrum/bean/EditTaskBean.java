package com.ncrowd.scrum.bean;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.ncrowd.model.Status;
import com.ncrowd.model.Task;
import com.ncrowd.model.WorkingHours;
import com.ncrowd.scrum.constants.ScrumPortletConstants;
import com.ncrowd.scrum.services.util.TaskImplUtilBean;
import com.ncrowd.scrum.util.FacesMessageUtil;
import com.ncrowd.scrum.util.ScrumUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.RowEditEvent;

@ManagedBean
@ViewScoped
public class EditTaskBean implements Serializable {
	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;

	@ManagedProperty("#{prefBean}")
	private PreferencesBean prefBean;
	private List<WorkingHours> workingHours;
	private Task groupTask;
	private Task taskToSave;
	private List<Task> allTasks;
	private List<Task> tasksToMove;
	@ManagedProperty("#{taskImplUtilBean}")
	private transient TaskImplUtilBean implUtil;
	private static final Log LOG = LogFactoryUtil.getLog(TaskBean.class);
	@ManagedProperty("#{trackerBean}")
	private ServiceTrackerBean serviceTrackerBean;

	public void setPrefBean(PreferencesBean prefBean) {
		this.prefBean = prefBean;
	}

	public void setServiceTrackerBean(ServiceTrackerBean serviceTrackerBean) {
		this.serviceTrackerBean = serviceTrackerBean;
	}

	public void setImplUtil(TaskImplUtilBean implUtil) {
		this.implUtil = implUtil;
	}

	public void saveWorkingHours(WorkingHours time, PreferencesBean prefBean) {
		if (time.isNew()) {
			long workingHoursId = serviceTrackerBean.getCounterService().increment(WorkingHours.class.getName());
			time.setWorkingHoursId(workingHoursId);
			time.setCompanyId(prefBean.getCompanyId());
			time.setGroupId(prefBean.getGroupId());
			time.setCreateDate(new Date());
			time.setUserGroupId(prefBean.getScrumGroupId());
			time.setUserId(prefBean.getUserId());
		}

		if (time.getStartDate() == null) {
			Date lastEndDate = getLastEndDateOfUserToday(prefBean.getUserId());
			if (lastEndDate != null) {
				time.setStartDate(lastEndDate);
			} else {
				time.setStartDate(ScrumUtil.getDefaultStartDate());
			}
		}
		time.persist();
	}
	
	private Date getLastEndDateOfUserToday(long userId) {
		List<WorkingHours> userHours = null;
		try {
			userHours = serviceTrackerBean.getWorkingHoursService().findByUserId(userId);
		} catch (SystemException var8) {
			LOG.error("Error getting working hours of user " + userId, var8);
			return null;
		}

		Date lastEndDate = null;
		Iterator<WorkingHours> uHoursItr = userHours.iterator();

		while (true) {
			Date endDate;
			do {
				WorkingHours hours;
				do {
					if (!uHoursItr.hasNext()) {
						return lastEndDate;
					}

					hours = uHoursItr.next();
				} while (!ScrumUtil.isToday(hours.getStartDate()));

				endDate = ScrumUtil.addHours(hours.getStartDate(), hours.getHours());
			} while (lastEndDate != null && !lastEndDate.before(endDate));

			lastEndDate = endDate;
		}
	}
	
	
	public Task getCurrentTask() {
		if (Validator.isNotNull(taskToSave)) {
			return taskToSave;
		} else {
			FacesContext context = FacesContext.getCurrentInstance();
			Map<String, String> requestParams = context.getExternalContext().getRequestParameterMap();
			String taskIdString =  requestParams.get("taskId");
			Boolean newTask = Boolean.parseBoolean( requestParams.get("newTask"));
			if (newTask) {
				try {
					taskToSave = createNewTask();
					groupTask=taskToSave;
				} catch (SystemException e) {
					LOG.error("Failed to create new task", e);
				}
			} else if (Validator.isNumber(taskIdString)) {
				long taskId = Long.parseLong(taskIdString);
				try {
					taskToSave = serviceTrackerBean.getTaskService().fetchTask(taskId);
					groupTask=taskToSave;
				} catch (SystemException e) {
					LOG.error("Failed to get task with ID " + taskId, e);
				}
			}

			if (Validator.isNotNull(taskIdString) && Validator.isNull(taskToSave)) {
				FacesMessageUtil.addGlobalErrorMessage(context, "scrum-load-task-failed");
			}

			return taskToSave;
		}
	}
	
	public void addNewTime() {
		FacesContext context = FacesContext.getCurrentInstance();
		if(Validator.isNotNull(groupTask)) {
			Map<String, Object> sessionMap = context.getExternalContext().getSessionMap();
			PreferencesBean currPrefBean = (PreferencesBean) sessionMap.get("prefBean");
			WorkingHours newTime = serviceTrackerBean.getWorkingHoursService().createWorkingHours(serviceTrackerBean.getCounterService().increment(WorkingHours.class.getName()));
			newTime.setNew(true);
			newTime.setTaskId(groupTask.getTaskId());
			newTime.setUserId(currPrefBean.getUserId());
			newTime.setStartDate(ScrumUtil.getDefaultStartDate());
			newTime.setHours((double) ScrumPortletConstants.DEFAULT_WORKING_HOURS);
			workingHours=getWorkingHours();
			workingHours.add(newTime);
			taskToSave.setWorkingHours(workingHours);
		}else {
			FacesMessageUtil.addGlobalErrorMessage(context, "scrum-load-group-task-failed");
		}
	}
	
	public List<WorkingHours> getWorkingHours() {
		if (workingHours == null) {
			workingHours = new ArrayList<>();
			try {
				List<WorkingHours> loadedWorkingHours = serviceTrackerBean.getWorkingHoursService()
						.findByTaskId(groupTask.getTaskId());
				workingHours.addAll(loadedWorkingHours);
			} catch (SystemException e) {
				LOG.error("Failed to get the working hours for task " + groupTask.getTaskId(), e);
			}
		}

		return workingHours;
	}

	public boolean isPortletReady() {
		return prefBean.isPortletReady(false) && getCurrentTask() != null;
	}

	private Task createNewTask() {
		long taskId = serviceTrackerBean.getCounterService().increment(Task.class.getName());
		Task newTask = serviceTrackerBean.getTaskService().createTask(taskId);
		newTask.setStatus(getDefaultStatus());
		newTask.setSprintId(prefBean.getCurrentSprintId());
		return newTask;
	}

	private Status getDefaultStatus() {
		try {
			return serviceTrackerBean.getStatusService().getDefaultStatus(prefBean.getScrumGroupId());
		} catch (Exception e) {
			LOG.error("Failed to get default status of group " + prefBean.getScrumGroupId(), e);
			return null;
		}
	}

	public void submitTaskForm(ActionEvent event) {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		try {
			saveTask(taskToSave, prefBean.getCurrentSprintId());
			prefBean.sendPushMessage("/updateTask", String.valueOf(taskToSave.getTaskId()));
			taskToSave = null;
			FacesMessageUtil.addGlobalInfoMessage(facesContext, "scrum-save-task-successful");
		} catch (Exception var4) {
			LOG.error("Error saving task", var4);
			FacesMessageUtil.addGlobalErrorMessage(facesContext, "scrum-save-task-failed");
		}

	}

	protected void saveTaskAndLogErrors(Task task, long sprintId) {
		try {
			saveTask(task, sprintId);
		} catch (Exception e) {
			LOG.error("Error saving task", e);
		}

	}

	protected void saveTask(Task task, long sprintId) throws PortalException {
		if (task == null) {
			throw new SystemException("Task to save is null");
		} else {
			Date currentDate = new Date();
			if (task.isNew()) {
				initNewTask(task, sprintId);
			} else {
				task.setModifiedDate(currentDate);
			}

			if (task.getStatusId() != 0L && task.getStatus().isFixedStatus()) {
				if (task.getFixedSince() == null) {
					task.setFixedSince(currentDate);
				}
			} else {
				task.setFixedSince((Date) null);
			}

			task.persist();
			implUtil.setGroupTask(task);
		}
	}

	private void initNewTask(Task task, long sprintId) {
		int sortIndex = getNextSortingIndex(sprintId, task.getStatusId());
		task.setCompanyId(prefBean.getCompanyId());
		task.setGroupId(prefBean.getGroupId());
		task.setUserGroupId(prefBean.getScrumGroupId());
		task.setUserId(prefBean.getUserId());
		task.setSortIndex(sortIndex);
		task.setCreateDate(new Date());
		task.setTaskGroup(false);
	}

	private int getNextSortingIndex(long sprintId, long statusId) {
		List<Task> statusTasks = new ArrayList<>();

		try {
			statusTasks = serviceTrackerBean.getTaskService().findByG_S_S(prefBean.getScrumGroupId(), sprintId, statusId);
		} catch (SystemException var9) {
			LOG.error("Failed to get tasks of group " + prefBean.getScrumGroupId() + ", sprint "
					+ prefBean.getCurrentSprintId() + ", status " + taskToSave.getStatusId(), var9);
		}

		int sortIndex = -1;
		for (Task task : statusTasks) {
			if (task.getSortIndex() > sortIndex) {
				sortIndex = task.getSortIndex();
			}
		}

		return sortIndex + 1;
	}

	public void onEditTime(RowEditEvent event) {
		WorkingHours editedHours = (WorkingHours) event.getObject();
		FacesContext facesContext = FacesContext.getCurrentInstance();
		if (editedHours.getUserId() == prefBean.getUserId()) {
			try {
				saveWorkingHours(editedHours, prefBean);
				prefBean.sendPushMessage("/updateTask", String.valueOf(taskToSave.getTaskId()));
				FacesMessageUtil.addGlobalInfoMessage(facesContext, "scrum-save-time-successful");
			} catch (SystemException var5) {
				LOG.error("Failed to save working hours for task " + taskToSave.getTaskId(), var5);
				FacesMessageUtil.addGlobalErrorMessage(facesContext, "scrum-save-time-failed");
			}
		} else {
			FacesMessageUtil.addGlobalErrorMessage(facesContext, "scrum-save-time-forbidden");
		}
	}

	public void setTimeToDelete(WorkingHours timeToDelete) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		try {
			if (timeToDelete.getUserId() == prefBean.getUserId()) {
				serviceTrackerBean.getWorkingHoursService().deleteWorkingHours(timeToDelete);
				taskToSave.getWorkingHours().remove(timeToDelete);
				FacesMessageUtil.addGlobalInfoMessage(facesContext, "scrum-delete-time-successful");
			} else {
				FacesMessageUtil.addGlobalErrorMessage(facesContext, "scrum-delete-time-forbidden");
			}
		} catch (Exception var3) {
			LOG.error("Failed to delete working hours with ID " + timeToDelete.getWorkingHoursId());
			FacesMessageUtil.addGlobalErrorMessage(facesContext, "scrum-delete-time-failed");
		}

	}

	public String getGroupSelection() {
		long taskGroupId = getCurrentTask().getTaskGroupId();
		if (taskGroupId != 0L) {
			try {
				return serviceTrackerBean.getTaskService().getTask(taskGroupId).getDescription();
			} catch (Exception var4) {
				LOG.error("Error getting task group with ID " + taskGroupId, var4);
			}
		}

		return "";
	}

	public void setGroupSelection(String groupSelection) {
		if (Validator.isNotNull(groupSelection)) {
			long taskGroupId = getTaskGroupIdByName(groupSelection);
			if (taskGroupId == 0L) {
				taskGroupId = createTaskGroup(groupSelection);
			}

			getCurrentTask().setTaskGroupId(taskGroupId);
		}

	}

	private long getTaskGroupIdByName(String groupSelection) {
		long taskGroupId = 0L;

		try {
			List<Task> taskList = serviceTrackerBean.getTaskService().findTaskGroupByName(prefBean.getScrumGroupId(),
					groupSelection);
			if (!taskList.isEmpty()) {
				taskGroupId = taskList.get(0).getTaskId();
			}
		} catch (SystemException e) {
			LOG.error("Error finding task group by name: " + groupSelection, e);
		}

		return taskGroupId;
	}

	private long createTaskGroup(String groupSelection) {
		long taskId = 0L;

		try {
			Task groupTask = createNewTask();
			groupTask.setTaskGroup(true);
			groupTask.setDescription(groupSelection);
			groupTask.setSprintId(0L);
			groupTask.setStatusId(0L);
			groupTask.setCreateDate(new Date());
			groupTask.setCompanyId(prefBean.getCompanyId());
			groupTask.setGroupId(prefBean.getGroupId());
			groupTask.setUserGroupId(prefBean.getScrumGroupId());
			groupTask.setUserId(prefBean.getUserId());
			groupTask.persist();
			implUtil.setGroupTask(groupTask);
			taskId = groupTask.getTaskId();
		} catch (SystemException var5) {
			LOG.error("Error creating task for task group " + groupSelection, var5);
		}

		return taskId;
	}

	public List<Task> getTasksToMove() {
		return tasksToMove;
	}

	public void setTasksToMove(List<Task> tasksToMove) {
		this.tasksToMove = tasksToMove;
	}

	public void moveTasksToSprint(long sprintId) {
		for (Task task : tasksToMove) {
			if (task.getTimeForCopy() > 0.0D) {
				copyTaskToSprint(task, sprintId);
			} else {
				moveTaskToSprint(sprintId, task);
			}
		}

		if (!tasksToMove.isEmpty()) {
			prefBean.sendPushMessage("/updateTask", "");
		}

		allTasks = null;
		tasksToMove = new ArrayList<>();
	}

	private void moveTaskToSprint(long sprintId, Task task) {
		task.setSprintId(sprintId);
		int sortIndex = getNextSortingIndex(sprintId, task.getStatusId());
		task.setSortIndex(sortIndex);
		saveTaskAndLogErrors(task, sprintId);
	}

	private void copyTaskToSprint(Task task, long sprintId) {
		try {
			long taskId = serviceTrackerBean.getCounterService().increment(Task.class.getName());
			Status newStatus = serviceTrackerBean.getStatusService().findByUserGroupId(prefBean.getScrumGroupId())
					.get(0);
			task.setPrimaryKey(taskId);
			task.setNew(true);
			task.setSprintId(sprintId);
			task.setEvaluation(task.getTimeForCopy());
			task.setStatus(newStatus);
			saveTaskAndLogErrors(task, sprintId);
		} catch (SystemException e) {
			LOG.error("Error copying task " + task.getTaskId() + " to sprint " + sprintId, e);
		}

		task.setTimeForCopy(0.0D);
	}

	public List<Task> getAllTasksExceptSprint(long sprintId) {
		if (allTasks != null) {
			return allTasks;
		} else {
			allTasks = new ArrayList<>();
			try {
				List<Task> loadedTasks = serviceTrackerBean.getTaskService().findByUserGroupId(prefBean.getScrumGroupId());
				for (Task task : loadedTasks) {
					if (task.getSprintId() != sprintId) {
						allTasks.add(task);
					}
				}
			} catch (SystemException var6) {
				LOG.error("Error getting the tasks of group " + prefBean.getScrumGroupId(), var6);
			}

			return allTasks;
		}
	}

	public String deleteTask() {
		if (Validator.isNotNull(taskToSave)) {
			if (taskToSave.getTotalWorkingHours() > 0.0F) {
				FacesMessageUtil.addGlobalErrorMessage(FacesContext.getCurrentInstance(),
						"scrum-delete-task-workinghours");
				return "";
			}

			try {
				serviceTrackerBean.getTaskService().deleteTask(taskToSave.getTaskId());
			} catch (Exception e) {
				LOG.error("Error deleting task " + taskToSave.getTaskId(), e);
			}
		}

		return "view";
	}

}
