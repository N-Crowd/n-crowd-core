package com.ncrowd.scrum.bean;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.document.library.kernel.service.DLAppLocalService;
import com.liferay.document.library.kernel.service.DLFileEntryLocalService;
import com.liferay.document.library.kernel.service.DLFolderLocalService;
import com.liferay.message.boards.service.MBMessageLocalService;
import com.liferay.message.boards.service.MBThreadLocalService;
import com.liferay.message.boards.service.MBDiscussionLocalService;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.UserGroupLocalService;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.ratings.kernel.service.RatingsEntryLocalService;
import com.ncrowd.scrum.servicetracker.CounterServiceTracker;
import com.ncrowd.scrum.servicetracker.DlAppServiceTracker;
import com.ncrowd.scrum.servicetracker.DlFileServiceTracker;
import com.ncrowd.scrum.servicetracker.DlFolderServiceTracker;
import com.ncrowd.scrum.servicetracker.DocumentMappingServiceTracker;
import com.ncrowd.scrum.servicetracker.MBDiscussionServiceTracker;
import com.ncrowd.scrum.servicetracker.MBMessageServiceTracker;
import com.ncrowd.scrum.servicetracker.MBThreadServiceTracker;
import com.ncrowd.scrum.servicetracker.RatingsEntryServiceTracker;
import com.ncrowd.scrum.servicetracker.SprintServiceTracker;
import com.ncrowd.scrum.servicetracker.StatusServiceTracker;
import com.ncrowd.scrum.servicetracker.TaskServiceTracker;
import com.ncrowd.scrum.servicetracker.UserGroupServiceTracker;
import com.ncrowd.scrum.servicetracker.UserServiceTracker;
import com.ncrowd.scrum.servicetracker.WorkingHoursServiceTracker;
import com.ncrowd.service.DocumentMappingLocalService;
import com.ncrowd.service.SprintLocalService;
import com.ncrowd.service.StatusLocalService;
import com.ncrowd.service.TaskLocalService;
import com.ncrowd.service.WorkingHoursLocalService;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;

@ManagedBean(name = "trackerBean")
@ApplicationScoped
public class ServiceTrackerBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Log LOG = LogFactoryUtil.getLog(ServiceTrackerBean.class);
	private CounterServiceTracker counterServiceTracker;
	private SprintServiceTracker sprintServiceTracker;
	private StatusServiceTracker statusServiceTracker;
	private UserGroupServiceTracker userGroupServiceTracker;
	private UserServiceTracker userServiceTracker;
	private TaskServiceTracker taskServiceTracker;
	private DocumentMappingServiceTracker documentServiceTracker;
	private WorkingHoursServiceTracker workingHoursServiceTracker;
	private DlFolderServiceTracker dlFolderServiceTracker;
	private DlFileServiceTracker dlFileServiceTracker;
	private MBMessageServiceTracker mbMessageServiceTracker;
	private MBThreadServiceTracker mbThreadServiceTracker;
	private MBDiscussionServiceTracker mbDiscussionServiceTracker;
	private RatingsEntryServiceTracker ratingsServiceServiceTracker;
	private DlAppServiceTracker dlAppServiceTracker;
	
	private CounterLocalService counterService;
	private StatusLocalService statusService;
	private SprintLocalService sprintService;
	private UserGroupLocalService userGroupService;
	private UserLocalService userService;
	private TaskLocalService taskService;
	private MBMessageLocalService mbMessageService;
	private MBThreadLocalService mbThreadService;
	private MBDiscussionLocalService mbDiscussionService;
	private RatingsEntryLocalService ratingsEntryService;
	private DLFolderLocalService dlFolderService;
	private DLFileEntryLocalService dlFileService;
	private WorkingHoursLocalService workingHoursService;
	private DocumentMappingLocalService documentService;
	private DLAppLocalService dlAppService;

	@PostConstruct
	public void init() {
		try {
			BundleContext context = getBundleContext();
			counterServiceTracker = new CounterServiceTracker(context);
			dlFolderServiceTracker = new DlFolderServiceTracker(context);
			dlFileServiceTracker = new DlFileServiceTracker(context);
			dlAppServiceTracker=new DlAppServiceTracker(context);
			documentServiceTracker = new DocumentMappingServiceTracker(context);
			mbMessageServiceTracker = new MBMessageServiceTracker(context);
			mbThreadServiceTracker = new MBThreadServiceTracker(context);
			mbDiscussionServiceTracker = new MBDiscussionServiceTracker(context);
			ratingsServiceServiceTracker = new RatingsEntryServiceTracker(context);
			statusServiceTracker = new StatusServiceTracker(context);
			sprintServiceTracker = new SprintServiceTracker(context);
			taskServiceTracker = new TaskServiceTracker(context);
			userGroupServiceTracker = new UserGroupServiceTracker(context);
			userServiceTracker = new UserServiceTracker(context);
			workingHoursServiceTracker=new WorkingHoursServiceTracker(context);
			
			LOG.info("opening all service trackers..................");
			openAllServices();
			if(!counterServiceTracker.isEmpty())
				counterService=counterServiceTracker.getService();
			if(!dlFolderServiceTracker.isEmpty())
				dlFolderService=dlFolderServiceTracker.getService();
			if(!dlFileServiceTracker.isEmpty())
				dlFileService=dlFileServiceTracker.getService();
			if(!documentServiceTracker.isEmpty())
				documentService=documentServiceTracker.getService();
			if(!mbMessageServiceTracker.isEmpty())
				mbMessageService=mbMessageServiceTracker.getService();
			if(!mbThreadServiceTracker.isEmpty())
				mbThreadService=mbThreadServiceTracker.getService();
			if(!mbDiscussionServiceTracker.isEmpty())
				mbDiscussionService=mbDiscussionServiceTracker.getService();
			if(!ratingsServiceServiceTracker.isEmpty())
				ratingsEntryService=ratingsServiceServiceTracker.getService();
			if(!statusServiceTracker.isEmpty())
				statusService=statusServiceTracker.getService();
			if(!sprintServiceTracker.isEmpty())
				sprintService=sprintServiceTracker.getService();
			if(!taskServiceTracker.isEmpty())
				taskService=taskServiceTracker.getService();
			if(!userGroupServiceTracker.isEmpty())
				userGroupService=userGroupServiceTracker.getService();
			if(!userServiceTracker.isEmpty())
				userService=userServiceTracker.getService();
			if(!workingHoursServiceTracker.isEmpty())
				workingHoursService=workingHoursServiceTracker.getService();
			if(!dlAppServiceTracker.isEmpty())
				dlAppService=dlAppServiceTracker.getService();
		} catch (Exception e) {
			LOG.error("Unable to create initlize service please deploy and chek it again.");
		}
	}

	private void openAllServices() {
		dlFolderServiceTracker.open();
		dlFileServiceTracker.open();
		dlAppServiceTracker.open();
		mbDiscussionServiceTracker.open();
		mbThreadServiceTracker.open();
		mbMessageServiceTracker.open();
		counterServiceTracker.open();
		userServiceTracker.open();
		userGroupServiceTracker.open();
		statusServiceTracker.open();
		sprintServiceTracker.open();
		taskServiceTracker.open();
		ratingsServiceServiceTracker.open();
		workingHoursServiceTracker.open();
		documentServiceTracker.open();
	}

	private BundleContext getBundleContext() {
		return FrameworkUtil.getBundle(this.getClass()).getBundleContext();
	}

	@PreDestroy
	public void destroy() {
		dlFolderServiceTracker.close();
		dlFileServiceTracker.close();
		mbDiscussionServiceTracker.close();
		mbThreadServiceTracker.close();
		mbMessageServiceTracker.close();
		counterServiceTracker.close();
		userServiceTracker.close();
		userGroupServiceTracker.close();
		statusServiceTracker.close();
		sprintServiceTracker.close();
		taskServiceTracker.close();
		ratingsServiceServiceTracker.close();
		workingHoursServiceTracker.close();
		documentServiceTracker.close();
	}

	public WorkingHoursServiceTracker getWorkingHoursServiceTracker() {
		return workingHoursServiceTracker;
	}

	public CounterLocalService getCounterService() {
		return counterService;
	}

	public StatusLocalService getStatusService() {
		return statusService;
	}

	public SprintLocalService getSprintService() {
		return sprintService;
	}

	public UserGroupLocalService getUserGroupService() {
		return userGroupService;
	}

	public UserLocalService getUserService() {
		return userService;
	}

	public TaskLocalService getTaskService() {
		return taskService;
	}

	public MBMessageLocalService getMbMessageService() {
		return mbMessageService;
	}

	public MBThreadLocalService getMbThreadService() {
		return mbThreadService;
	}

	public MBDiscussionLocalService getMbDiscussionService() {
		return mbDiscussionService;
	}

	public RatingsEntryLocalService getRatingsEntryService() {
		return ratingsEntryService;
	}

	public DLFolderLocalService getDlFolderService() {
		return dlFolderService;
	}

	public DLFileEntryLocalService getDlFileService() {
		return dlFileService;
	}

	public WorkingHoursLocalService getWorkingHoursService() {
		return workingHoursService;
	}

	public DocumentMappingLocalService getDocumentService() {
		return documentService;
	}

	public DLAppLocalService getDlAppService() {
		return dlAppService;
	}
}
