package com.ncrowd.scrum.servicetracker;

import com.ncrowd.service.WorkingHoursLocalService;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

public class WorkingHoursServiceTracker extends ServiceTracker<WorkingHoursLocalService, WorkingHoursLocalService>{

	public WorkingHoursServiceTracker(BundleContext bundleContext){
		super(bundleContext, WorkingHoursLocalService.class, null);
	}
	
}
