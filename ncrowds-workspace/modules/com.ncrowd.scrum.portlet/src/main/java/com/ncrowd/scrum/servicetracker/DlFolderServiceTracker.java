package com.ncrowd.scrum.servicetracker;

import com.liferay.document.library.kernel.service.DLFolderLocalService;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

public class DlFolderServiceTracker extends ServiceTracker<DLFolderLocalService, DLFolderLocalService>{
	public DlFolderServiceTracker(BundleContext bundleContext) {
		super(bundleContext,DLFolderLocalService.class,null);
	}
}
