package com.ncrowd.scrum.servicetracker;

import com.liferay.document.library.kernel.service.DLAppLocalService;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

public class DlAppServiceTracker extends ServiceTracker<DLAppLocalService, DLAppLocalService>{
	public DlAppServiceTracker(BundleContext bundleContext) {
		super(bundleContext,DLAppLocalService.class,null);
	}
}
