package com.ncrowd.scrum.servicetracker;

import com.liferay.ratings.kernel.service.RatingsEntryLocalService;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

public class RatingsEntryServiceTracker extends ServiceTracker<RatingsEntryLocalService, RatingsEntryLocalService>{
	public RatingsEntryServiceTracker(BundleContext bundleContext) {
		super(bundleContext,RatingsEntryLocalService.class,null);
	}
}
