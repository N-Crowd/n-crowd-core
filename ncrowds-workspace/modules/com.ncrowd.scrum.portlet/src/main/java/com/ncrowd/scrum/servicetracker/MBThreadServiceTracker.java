package com.ncrowd.scrum.servicetracker;

import com.liferay.message.boards.service.MBThreadLocalService;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

public class MBThreadServiceTracker extends ServiceTracker<MBThreadLocalService, MBThreadLocalService>{
	public MBThreadServiceTracker(BundleContext bundleContext) {
		super(bundleContext,MBThreadLocalService.class,null);
	}
}
