package com.ncrowd.scrum.util;

import com.liferay.faces.util.logging.Logger;
import com.liferay.faces.util.logging.LoggerFactory;
import com.liferay.portal.kernel.language.LanguageUtil;

import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

public class FacesMessageUtil {
	private static final Logger logger = LoggerFactory.getLogger(FacesMessageUtil.class);

	public static void addErrorMessage(String clientId, FacesContext facesContext, String key, Object[] args) {
		addMessage(clientId, facesContext, FacesMessage.SEVERITY_ERROR, key, args);
	}

	public static void addErrorMessage(String clientId, FacesContext facesContext, String key) {
		addMessage(clientId, facesContext, FacesMessage.SEVERITY_ERROR, key, new Object[0]);
	}

	public static void addErrorMessage(String clientId, FacesContext facesContext, String key, String value) {
		addMessage(clientId, facesContext, FacesMessage.SEVERITY_ERROR, key, new Object[] { value });
	}

	public static void addGlobalErrorMessage(FacesContext facesContext, String key, Object[] args) {
		String clientId = null;
		addErrorMessage((String) clientId, facesContext, key, (Object[]) args);
	}

	public static void addGlobalErrorMessage(FacesContext facesContext, String key) {
		String clientId = null;
		addErrorMessage((String) clientId, facesContext, key, (Object[]) (new Object[0]));
	}

	public static void addGlobalErrorMessage(FacesContext facesContext, String key, String value) {
		String clientId = null;
		addErrorMessage((String) clientId, facesContext, key, (Object[]) (new Object[] { value }));
	}

	public static void addGlobalInfoMessage(FacesContext facesContext, String key, Object[] args) {
		String clientId = null;
		addInfoMessage((String) clientId, facesContext, key, (Object[]) args);
	}

	public static void addGlobalInfoMessage(FacesContext facesContext, String key) {
		String clientId = null;
		addInfoMessage((String) clientId, facesContext, key, (Object[]) (new Object[0]));
	}

	public static void addGlobalInfoMessage(FacesContext facesContext, String key, String value) {
		String clientId = null;
		addInfoMessage((String) clientId, facesContext, key, (Object[]) (new Object[] { value }));
	}

	public static void addGlobalInfoMessage(FacesContext facesContext, String key, Object arg) {
		addGlobalInfoMessage(facesContext, key, new Object[] { arg });
	}

	public static void addGlobalSuccessInfoMessage(FacesContext facesContext) {
		String key = "your-request-processed-successfully";
		Object[] args = null;
		addGlobalInfoMessage(facesContext, key, args);
	}

	public static void addGlobalUnexpectedErrorMessage(FacesContext facesContext) {
		String key = "an-unexpected-error-occurred";
		Object[] args =  null;
		addGlobalErrorMessage(facesContext, key, args);
	}

	public static void addInfoMessage(String clientId, FacesContext facesContext, String key, Object[] args) {
		addMessage(clientId, facesContext, FacesMessage.SEVERITY_INFO, key, args);
	}

	public static void addInfoMessage(String clientId, FacesContext facesContext, String key) {
		addMessage(clientId, facesContext, FacesMessage.SEVERITY_INFO, key, new Object[0]);
	}

	public static void addInfoMessage(String clientId, FacesContext facesContext, String key, String value) {
		addMessage(clientId, facesContext, FacesMessage.SEVERITY_INFO, key, new Object[] { value });
	}

	public static void addMessage(String clientId, FacesContext facesContext, Severity severity, String key,
			Object[] args) {
		String message = getMessage(key, args, facesContext);
		FacesMessage facesMessage = new FacesMessage(severity, message, message);
		facesContext.addMessage(clientId, facesMessage);
	}

	public static String getMessage(String key, Object[] args, FacesContext facesContext) {
		String message = key;
		try {
			Locale locale = facesContext.getViewRoot().getLocale();
			if (locale == null) {
				facesContext.getApplication().getDefaultLocale();
			}

			message = LanguageUtil.format(locale, key, args);
		} catch (Exception var5) {
			logger.error(var5.getMessage(), new Object[] { var5 });
		}

		return message;
	}
}
