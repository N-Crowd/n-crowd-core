package com.ncrowd.scrum.model;

import java.util.ArrayList;
import java.util.List;

public class Theme{
	
	private String name;
	private String image;

	public Theme() {
	}

	public Theme(String name, String image) {
		this.name = name;
		this.image = image;
	}

	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() {
		return this.name;
	}

	public static List<Theme> getAvailableThemes() {
		List<Theme> availableThemes = new ArrayList<>();
		availableThemes.add(new Theme("afterdark", "afterdark.png"));
		availableThemes.add(new Theme("afternoon", "afternoon.png"));
		availableThemes.add(new Theme("afterwork", "afterwork.png"));
		availableThemes.add(new Theme("aristo", "aristo.png"));
		availableThemes.add(new Theme("black-tie", "black-tie.png"));
		availableThemes.add(new Theme("blitzer", "blitzer.png"));
		availableThemes.add(new Theme("bluesky", "bluesky.png"));
		availableThemes.add(new Theme("casablanca", "casablanca.png"));
		availableThemes.add(new Theme("cruze", "cruze.png"));
		availableThemes.add(new Theme("cupertino", "cupertino.png"));
		availableThemes.add(new Theme("dark-hive", "dark-hive.png"));
		availableThemes.add(new Theme("dot-luv", "dot-luv.png"));
		availableThemes.add(new Theme("eggplant", "eggplant.png"));
		availableThemes.add(new Theme("excite-bike", "excite-bike.png"));
		availableThemes.add(new Theme("flick", "flick.png"));
		availableThemes.add(new Theme("glass-x", "glass-x.png"));
		availableThemes.add(new Theme("home", "home.png"));
		availableThemes.add(new Theme("hot-sneaks", "hot-sneaks.png"));
		availableThemes.add(new Theme("humanity", "humanity.png"));
		availableThemes.add(new Theme("le-frog", "le-frog.png"));
		availableThemes.add(new Theme("midnight", "midnight.png"));
		availableThemes.add(new Theme("mint-choc", "mint-choc.png"));
		availableThemes.add(new Theme("overcast", "overcast.png"));
		availableThemes.add(new Theme("pepper-grinder", "pepper-grinder.png"));
		availableThemes.add(new Theme("redmond", "redmond.png"));
		availableThemes.add(new Theme("rocket", "rocket.png"));
		availableThemes.add(new Theme("sam", "sam.png"));
		availableThemes.add(new Theme("smoothness", "smoothness.png"));
		availableThemes.add(new Theme("south-street", "south-street.png"));
		availableThemes.add(new Theme("start", "start.png"));
		availableThemes.add(new Theme("sunny", "sunny.png"));
		availableThemes.add(new Theme("swanky-purse", "swanky-purse.png"));
		availableThemes.add(new Theme("trontastic", "trontastic.png"));
		availableThemes.add(new Theme("ui-darkness", "ui-darkness.png"));
		availableThemes.add(new Theme("ui-lightness", "ui-lightness.png"));
		availableThemes.add(new Theme("vader", "vader.png"));
		return availableThemes;
	}
}
