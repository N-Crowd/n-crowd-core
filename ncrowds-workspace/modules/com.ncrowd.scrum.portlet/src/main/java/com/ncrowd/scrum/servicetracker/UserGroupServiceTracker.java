package com.ncrowd.scrum.servicetracker;

import com.liferay.portal.kernel.service.UserGroupLocalService;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

public class UserGroupServiceTracker extends ServiceTracker<UserGroupLocalService, UserGroupLocalService>{
	public UserGroupServiceTracker(BundleContext bundleContext) {
		super(bundleContext,UserGroupLocalService.class,null);
	}
}
