package com.ncrowd.scrum.model;

public enum ColorMode {
	DEFAULT, INDIVIDUAL, GROUP;
}
