package com.ncrowd.scrum.servicetracker;

import com.ncrowd.service.TaskLocalService;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

public class TaskServiceTracker extends ServiceTracker<TaskLocalService, TaskLocalService>{

	public TaskServiceTracker(BundleContext bundleContext){
		super(bundleContext, TaskLocalService.class, null);
	}
	
}
