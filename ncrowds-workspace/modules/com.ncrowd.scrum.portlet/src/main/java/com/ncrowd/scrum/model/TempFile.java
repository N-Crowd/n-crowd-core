package com.ncrowd.scrum.model;

import java.io.File;

public class TempFile {
	private File file;
	private String fileName;
	private String mimeType;
	private long fileSize;
	private boolean alreadyAssigned;

	public TempFile(File file, String fileName, String mimeType, long fileSize) {
		this.file = file;
		this.fileName = fileName;
		this.mimeType = mimeType;
		this.fileSize = fileSize;
	}

	public File getFile() {
		return this.file;
	}

	public void setFile(File file) {
		this.file = file;
		this.file.deleteOnExit();
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getMimeType() {
		return this.mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public long getFileSize() {
		return this.fileSize;
	}

	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	public boolean isAlreadyAssigned() {
		return this.alreadyAssigned;
	}

	public void setAlreadyAssigned(boolean alreadyAssigned) {
		this.alreadyAssigned = alreadyAssigned;
	}
}
