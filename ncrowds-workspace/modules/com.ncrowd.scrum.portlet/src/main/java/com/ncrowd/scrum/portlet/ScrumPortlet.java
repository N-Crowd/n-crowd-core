package com.ncrowd.scrum.portlet;

import com.liferay.document.library.kernel.document.conversion.DocumentConversionUtil;
import com.liferay.document.library.kernel.service.DLAppServiceUtil;
import com.liferay.document.library.kernel.util.DLUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.FileVersion;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.faces.GenericFacesPortlet;
import javax.servlet.http.HttpServletResponse;

public class ScrumPortlet extends GenericFacesPortlet{
	
	private static final Log LOG=LogFactoryUtil.getLog(ScrumPortlet.class);
    
	@Override
    public void serveResource(final ResourceRequest request, final ResourceResponse response) throws PortletException, IOException {
        if ("pdfPreview".equals(request.getResourceID())) {
            final long fileEntryId = ParamUtil.getLong((PortletRequest)request, "fileEntryId");
            final String fileVersion = ParamUtil.getString((PortletRequest)request, "fileVersion");
            this.getPdfPreview(response, fileEntryId, fileVersion);
        }
        else {
            super.serveResource(request, response);
        }
    }
    
    private void getPdfPreview(final ResourceResponse response, final long fileEntryId, final String fileVersion) throws IOException {
        OutputStream os = null;
        try {
            final FileEntry fileEntry = DLAppServiceUtil.getFileEntry(fileEntryId);
            final FileVersion version = fileEntry.getFileVersion(fileVersion);
            final InputStream documentStream = version.getContentStream(true);
            final String id = DLUtil.getTempFileId(fileEntry.getFileEntryId(), fileVersion);
            File convertedFile = null;
            if (!"pdf".equals(fileEntry.getExtension())) {
                convertedFile = DocumentConversionUtil.convert(id, documentStream, fileEntry.getExtension(), "pdf");
            }
            else {
                convertedFile = FileUtil.createTempFile(documentStream);
            }
            final HttpServletResponse servletResponse = PortalUtil.getHttpServletResponse((PortletResponse)response);
            final byte[] content = FileUtil.getBytes(convertedFile);
            servletResponse.setContentType("application/pdf");
            servletResponse.setContentLength(content.length);
            os = servletResponse.getOutputStream();
            os.write(content);
        }
        catch (IOException e) {
            LOG.error((Object)"Error writing stream to output stream", (Throwable)e);
        }
        catch (Exception e2) {
            LOG.error((Throwable)e2);
        }
        finally {
            if (os != null) {
                os.flush();
                os.close();
            }
        }
        if (os != null) {
            os.flush();
            os.close();
        }
    }
}
