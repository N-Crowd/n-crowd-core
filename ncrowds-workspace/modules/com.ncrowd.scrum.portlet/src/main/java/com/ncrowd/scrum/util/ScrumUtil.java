package com.ncrowd.scrum.util;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;

import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.portlet.PortletURL;
import javax.portlet.WindowState;
import javax.portlet.WindowStateException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

public class ScrumUtil {

	private static final Log LOG = LogFactoryUtil.getLog(ScrumUtil.class);

	private ScrumUtil() {
	}

	public static Date getDefaultStartDate() {
		Calendar startDate = Calendar.getInstance();
		startDate.set(10, 9);
		startDate.set(12, 0);
		startDate.set(13, 0);
		startDate.set(9, 0);
		return startDate.getTime();
	}

	public static Date addDays(Date date, int days) {
		Calendar newDate = Calendar.getInstance();
		newDate.setTime(date);
		newDate.add(5, days);
		return newDate.getTime();
	}

	public static Date addHours(Date date, double h) {
		Calendar newDate = Calendar.getInstance();
		newDate.setTime(date);
		newDate.add(11, (int) Math.floor(h));
		newDate.add(12, (int) (h % 1.0D * 60.0D));
		return newDate.getTime();
	}

	public static boolean isToday(Date checkDate) {
		Calendar today = Calendar.getInstance();
		Calendar check = Calendar.getInstance();
		check.setTime(checkDate);
		return today.get(5) == check.get(5) && today.get(2) == check.get(2) && today.get(1) == check.get(1);
	}

	public static Date removeTime(Date date) {
		Calendar newDate = convertToCalendarWithoutTime(date);
		return newDate.getTime();
	}

	public static Calendar convertToCalendar(Date date) {
		Calendar newDate = Calendar.getInstance();
		newDate.setTime(date);
		return newDate;
	}

	public static Calendar convertToCalendarWithoutTime(Date date) {
		Calendar calendar = convertToCalendar(date);
		calendar.set(11, 0);
		calendar.set(12, 0);
		calendar.set(13, 0);
		calendar.set(14, 0);
		return calendar;
	}

	public static Object getCurrentBean(String name) {
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		return context.getSessionMap().get(name);
	}

	public static ThemeDisplay getThemeDisplay() {
		PortletRequest request = getPortletRequest();
		return (ThemeDisplay) request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY");
	}

	public static ServiceContext getServiceContext() throws PortalException {
		PortletRequest request = getPortletRequest();
		return ServiceContextFactory.getInstance(request);
	}

	public static PortletRequest getPortletRequest() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext context = facesContext.getExternalContext();
		return (PortletRequest) context.getRequest();
	}

	public static void writeStreamToOutputStream(InputStream stream, String title, String mimeType) throws IOException {
		ServletOutputStream os = null;

		try {
			PortletResponse portletResponse = (PortletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();
			HttpServletResponse response = PortalUtil.getHttpServletResponse(portletResponse);
			response.setHeader("Content-Disposition", "attachment; filename=\"" + title + "\"");
			response.setHeader("Content-Transfer-Encoding", "binary");
			response.setContentType(mimeType);
			response.flushBuffer();
			os = response.getOutputStream();

			int bytesRead;
			for (byte[] buffer = new byte[4096]; (bytesRead = stream.read(buffer)) != -1; buffer = new byte[4096]) {
				os.write(buffer, 0, bytesRead);
			}
		} catch (IOException e) {
			LOG.error("Error writing stream to output stream", e);
		} finally {
			if (os != null) {
				os.flush();
				os.close();
			}

		}

	}

	public static void doRedirect(String redirect, Map<String, String> params)
			throws WindowStateException, PortletModeException, IOException {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		PortletRequest portletRequest = (PortletRequest) externalContext.getRequest();
		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute("LIFERAY_SHARED_THEME_DISPLAY");
		PortletURL url = PortletURLFactoryUtil.create(portletRequest, PortalUtil.getPortletId(portletRequest),
				themeDisplay.getLayout().getPlid(), "ACTION_PHASE");
		url.setParameter("_facesViewIdRender", redirect);
		Iterator<String> urlItr = params.keySet().iterator();

		while (urlItr.hasNext()) {
			String key = urlItr.next();
			url.setParameter(key, params.get(key));
		}

		url.setWindowState(WindowState.NORMAL);
		url.setPortletMode(PortletMode.VIEW);
		FacesContext.getCurrentInstance().getExternalContext().redirect(url.toString());
	}

	public static String getLocaleString(String key, String defaultValue) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		PortletRequest request = (PortletRequest) externalContext.getRequest();
		Locale locale = request.getLocale();
		String value = LanguageUtil.get(locale, key, defaultValue);
		return Validator.isNull(value) ? defaultValue : value;
	}
}
