package com.ncrowd.scrum.servicetracker;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.ncrowd.service.DocumentMappingLocalService;
import com.ncrowd.service.StatusLocalService;
import com.ncrowd.service.TaskLocalService;
import com.ncrowd.service.WorkingHoursLocalService;

public class ServiceTrackerFactory {

	private static final Log log=LogFactoryUtil.getLog(ServiceTrackerFactory.class);
	
	/*
	 * @SuppressWarnings("unchecked") public static <T extends ServiceTracker<T,T>>
	 * T getService(String className) { if(Validator.isNotNull(className)) { if
	 * (className.contains("StatusLocalService")) { return (T) new
	 * StatusServiceTracker(StatusLocalService.class); } else if
	 * (className.contains("TaskLocalService")) { return (T) new
	 * TaskServiceTracker(TaskLocalService.class); } else if
	 * (className.contains("SprintLocalService")) { return (T) new
	 * SprintServiceTracker(SprintLocalService.class); } else if
	 * (className.contains("WorkingHoursLocalService")) { return (T) new
	 * WorkingHoursServiceTracker(WorkingHoursLocalService.class); } else if
	 * (className.contains("DocumentMappingLocalService")) { return (T) new
	 * DocumentMappingServiceTracker(DocumentMappingLocalService.class); }
	 * log.info("None of the services matched.so returning null."); }else {
	 * log.info("classname is null."); } return null; }
	 */
	
	/*
	 * public static StatusServiceTracker getStatusTracker() { return new
	 * StatusServiceTracker(StatusLocalService.class); }
	 */
	
	/*
	 * public static TaskServiceTracker getTaskTracker() { return new
	 * TaskServiceTracker(TaskLocalService.class); }
	 */
	
	/*
	 * public static DocumentMappingServiceTracker getDocumentMappingTracker() {
	 * return new DocumentMappingServiceTracker(DocumentMappingLocalService.class);
	 * }
	 */
	
	/*
	 * public static WorkingHoursServiceTracker getWorkingHoursTracker() { return
	 * new WorkingHoursServiceTracker(WorkingHoursLocalService.class); }
	 */

}
