package com.ncrowd.scrum.exception;

public class ServiceNotFoundException extends Exception{
	
	private static final long serialVersionUID = 1L;

	public ServiceNotFoundException(String msg) {
		super(msg);
	}
}
