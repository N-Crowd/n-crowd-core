package com.ncrowd.scrum.servicetracker;

import com.liferay.document.library.kernel.service.DLFileEntryLocalService;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

public class DlFileServiceTracker extends ServiceTracker<DLFileEntryLocalService, DLFileEntryLocalService>{
	public DlFileServiceTracker(BundleContext bundleContext) {
		super(bundleContext,DLFileEntryLocalService.class,null);
	}
}
