package com.ncrowd.scrum.converter;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.ncrowd.model.Status;
import com.ncrowd.scrum.bean.ServiceTrackerBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

@ManagedBean(name="statusConverter")
public class StatusConverter implements Converter {
   private static final Log LOG = LogFactoryUtil.getLog(StatusConverter.class);
   @ManagedProperty("#{trackerBean}")
   private ServiceTrackerBean trackerBean;
   
   public Object getAsObject(FacesContext context, UIComponent component, String value) {
      long statusId = Long.parseLong(value);
      Status status = null;
      try {
         status = trackerBean.getStatusService().getStatus(statusId);
      } catch (Exception var8) {
         LOG.error("Error getting the status object", var8);
      }

      return status;
   }

   public String getAsString(FacesContext context, UIComponent component, Object value) {
      return String.valueOf(((Status)value).getPrimaryKey());
   }

	public void setTrackerBean(ServiceTrackerBean bean) {
		this.trackerBean = bean;
	}
   
}