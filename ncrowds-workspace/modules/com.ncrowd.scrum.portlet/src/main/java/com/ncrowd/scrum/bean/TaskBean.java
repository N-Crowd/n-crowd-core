package com.ncrowd.scrum.bean;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.util.Validator;
import com.ncrowd.model.Status;
import com.ncrowd.model.Task;
import com.ncrowd.model.WorkingHours;
import com.ncrowd.scrum.model.ColorMode;
import com.ncrowd.scrum.util.FacesMessageUtil;
import com.ncrowd.scrum.util.ScrumUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.primefaces.event.RowEditEvent;
import org.primefaces.json.JSONObject;

@ManagedBean(name = "taskBean")
@ViewScoped
public class TaskBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ManagedProperty("#{prefBean}")
	private PreferencesBean prefBean;
	@ManagedProperty("#{sprintBean}")
	private SprintBean sprintBean;
	@ManagedProperty("#{editTaskBean}")
	private EditTaskBean editTaskBean;
	private Map<Long, Task> tasks;
	private List<Status> statusValues;
	private SelectItem[] statusOptions;
	private static final Log LOG = LogFactoryUtil.getLog(TaskBean.class);
	@ManagedProperty("#{trackerBean}")
	private ServiceTrackerBean serviceTrackerBean;
	private WorkingHours timeToSave;
	
	@PostConstruct
	public void init() {
		if (prefBean.isPortletReady(true)) {
			try {
				List<Status> loadedStatusValues = serviceTrackerBean.getStatusService()
						.findByUserGroupId(prefBean.getScrumGroupId());
				statusValues = new ArrayList<>();
				statusValues.addAll(loadedStatusValues);
				Collections.sort(statusValues);
			} catch (SystemException var2) {
				LOG.error("Error getting the status values of group " + prefBean.getScrumGroupId(), var2);
			}

			if (statusValues.isEmpty()) {
				statusValues = createDefaultStatusValues();
			}

			statusOptions = createFilterOptions(statusValues);
			if (statusValues.isEmpty()) {
				FacesContext facesContext = FacesContext.getCurrentInstance();
				FacesMessageUtil.addGlobalErrorMessage(facesContext, "scrum-no-status-values-available");
			}

			loadTasks();
		}

	}

	private List<Status> createDefaultStatusValues() {
		List<Status> statusVals = new ArrayList<>();

		try {
			String statusName = ScrumUtil.getLocaleString("scrum-status-default-todo", "Todo");
			statusVals.add(createNewStatusValue(statusName, 0, false));
			statusName = ScrumUtil.getLocaleString("scrum-status-default-work", "In work");
			statusVals.add(createNewStatusValue(statusName, 1, false));
			statusName = ScrumUtil.getLocaleString("scrum-status-default-fixed", "Fixed");
			statusVals.add(createNewStatusValue(statusName, 2, true));
		} catch (SystemException var3) {
			LOG.error("Error creating default status values", var3);
		}

		return statusVals;
	}
	
	public void saveWorkingHours(PreferencesBean prefBean) {
		editTaskBean.saveWorkingHours(timeToSave, prefBean);
	}
	
	public void submitTimeForm(Task task) {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			timeToSave=task.getTimeToSave();
			if (timeToSave != null && timeToSave.getHours() > 0.0D) {
				Map<String, Object> sessionMap = context.getExternalContext().getSessionMap();
				PreferencesBean prefrenceBean = (PreferencesBean) sessionMap.get("prefBean");
				saveWorkingHours(prefrenceBean);
				FacesMessageUtil.addGlobalInfoMessage(context, "scrum-save-time-successful");
				timeToSave.persist();
				timeToSave = null;
				loadTasks();
			}
		} catch (Exception e) {
			LOG.error("Failed to save working hours for task " , e);
			FacesMessageUtil.addGlobalErrorMessage(context, "scrum-save-time-failed");
		}
	}

	private Status createNewStatusValue(String name, int sortIndex, boolean fixedStatus) {
		long statusId = serviceTrackerBean.getCounterService().increment(Status.class.getName());
		Status status = serviceTrackerBean.getStatusService().createStatus(statusId);
		status.setStatusId(statusId);
		status.setCompanyId(prefBean.getCompanyId());
		status.setFixedStatus(fixedStatus);
		status.setGroupId(prefBean.getGroupId());
		status.setName(name);
		status.setSortIndex(sortIndex);
		status.setUserGroupId(prefBean.getScrumGroupId());
		status.persist();
		return status;
	}

	private void loadTasks() {
		try {
			List<Task> loadedTasks = serviceTrackerBean.getTaskService().findByG_S(prefBean.getScrumGroupId(),
					prefBean.getCurrentSprintId());
			this.tasks = new TreeMap<>();
			Iterator<Task> taskItr = loadedTasks.iterator();

			while (taskItr.hasNext()) {
				Task task = taskItr.next();
				tasks.put(task.getPrimaryKey(), task);
			}
		} catch (SystemException var4) {
			LOG.error("Error getting the tasks of group " + prefBean.getScrumGroupId(), var4);
		}

	}

	public void refreshTasks() {
		prefBean = (PreferencesBean) ScrumUtil.getCurrentBean("prefBean");
		this.loadTasks();
	}

	public void setPrefBean(PreferencesBean prefBean) {
		this.prefBean = prefBean;
	}

	public void setSprintBean(SprintBean sprintBean) {
		this.sprintBean = sprintBean;
	}

	public void setEditTaskBean(EditTaskBean editTaskBean) {
		this.editTaskBean = editTaskBean;
	}

	public Map<Long, Task> getTasks() {
		return tasks;
	}

	private SelectItem[] createFilterOptions(List<Status> data) {
		SelectItem[] options = new SelectItem[data.size() + 1];
		options[0] = new SelectItem("", "-");

		for (int i = 0; i < data.size(); ++i) {
			options[i + 1] = new SelectItem(data.get(i).getName(), data.get(i).getName());
		}

		return options;
	}

	public List<Status> getStatusValues() {
		return this.statusValues;
	}

	public int getRoundedColumnWidth() {
		return (int) Math.floor((double) (100.0F / (float) this.statusValues.size()) - 0.5D);
	}

	public SelectItem[] getStatusOptions() {
		return this.statusOptions;
	}

	public List<Task> getGroupList() {
		List<Task> taskGroups = new ArrayList<>();

		try {
			List<Task> loadedTasks = serviceTrackerBean.getTaskService().findTaskGroups(prefBean.getScrumGroupId());
			for (Task task : loadedTasks) {
				taskGroups.add(task);
			}
		} catch (SystemException var5) {
			LOG.error("Error getting the tasks of group " + prefBean.getScrumGroupId(), var5);
		}

		return taskGroups;
	}

	public List<Task> getTasksByStatus(int statusIndex) {
		List<Task> statusTasks = new ArrayList<>();
		Iterator<Task> taskItr = tasks.values().iterator();

		while (taskItr.hasNext()) {
			Task task = taskItr.next();
			Status taskStatus = task.getStatus();
			if (taskStatus != null
					&& taskStatus.getPrimaryKey() == (statusValues.get(statusIndex)).getPrimaryKey()) {
				statusTasks.add(task);
			}
		}

		Collections.sort(statusTasks);
		return statusTasks;
	}

	public String getAjaxCommand() {
		return "";
	}

	public void sendAjaxCommand() {
		String commandAsString = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get("ajaxCommand");
		if (!Validator.isNull(commandAsString)) {
			LOG.debug("AJAX command: " + commandAsString);
			try {
				JSONObject jsonCommand = new JSONObject(commandAsString);
				String command = jsonCommand.getString("command");
				if ("updateColor".equals(command)) {
					this.updateTaskColor(jsonCommand);
				} else if ("updateStatus".equals(command)) {
					this.updateTaskStatus(jsonCommand);
				} else if ("updateSorting".equals(command)) {
					this.updateTaskSorting(jsonCommand);
				}
			} catch (JSONException var4) {
				LOG.error("Error reading JSON command", var4);
			}

		}
	}

	public void onEditTask(RowEditEvent event) {
		Task editedTask = (Task) event.getObject();
		editedTask.setModifiedDate(new Date());
		FacesContext facesContext = FacesContext.getCurrentInstance();

		try {
			this.editTaskBean.saveTask(editedTask, prefBean.getCurrentSprintId());
			FacesMessageUtil.addGlobalInfoMessage(facesContext, "scrum-save-task-successful");
		} catch (Exception var5) {
			LOG.error("Error saving task", var5);
			FacesMessageUtil.addGlobalErrorMessage(facesContext, "scrum-save-task-failed");
		}

	}

	private void updateTaskColor(JSONObject jsonCommand) throws JSONException {
		long taskId = jsonCommand.getLong("taskId");
		String hexColor = jsonCommand.getString("hexColor");
		String mode = jsonCommand.getString("mode");
		Task task = this.tasks.get(taskId);
		if (mode.equals(ColorMode.GROUP.toString())) {
			task = task.getGroupTask();
		}

		task.setIndividualColor(hexColor);

		try {
			editTaskBean.saveTask(task, prefBean.getCurrentSprintId());
		} catch (Exception var8) {
			LOG.error("Error saving task color", var8);
		}

//		PushContext pushContext = PushContextFactory.getDefault().getPushContext();
//		pushContext.push("/updateTask", taskId);
	}

	private void updateTaskStatus(JSONObject jsonCommand) throws JSONException {
		long taskId = jsonCommand.getLong("taskId");
		int newStatusIndex = jsonCommand.getInt("newStatusIndex");
		int oldStatusIndex = jsonCommand.getInt("oldStatusIndex");
		Task task = (Task) this.tasks.get(taskId);
		task.setStatus((Status) this.statusValues.get(newStatusIndex));
		if (task.getAssignedTo() == 0L) {
			task.setAssignedTo(prefBean.getUserId());
		}

		this.updateTaskSortingForList(this.getTasksByStatus(oldStatusIndex));
		this.updateTaskSorting(jsonCommand);
	}

	private void updateTaskSorting(JSONObject jsonCommand) {
		long taskId = jsonCommand.getLong("taskId");
		int newSortingIndex = jsonCommand.getInt("newSortingIndex");
		Task movedTask = this.tasks.get(taskId);
		int statusIndex = this.statusValues.indexOf(movedTask.getStatus());
		List<Task> listToSort = this.getTasksByStatus(statusIndex);
		listToSort.remove(movedTask);
		if (!listToSort.isEmpty()) {
			listToSort.add(newSortingIndex, movedTask);
		} else {
			listToSort.add(movedTask);
		}

		updateTaskSortingForList(listToSort);
		prefBean.sendPushMessage("/updateTask", String.valueOf(taskId));
	}

	private void updateTaskSortingForList(List<Task> taskList) {
		int i = 0;
		for (Task task : taskList) {
			task.setSortIndex(i);
			this.editTaskBean.saveTaskAndLogErrors(task, prefBean.getCurrentSprintId());
		}

	}

	public void submitPreferencesForm() {
		PreferencesBean prefrBean = (PreferencesBean) ScrumUtil.getCurrentBean("prefBean");
		prefBean = prefrBean.submitForm();
		init();
	}

	public List<Task> getTaskList() {
		List<Task> taskList = new ArrayList<>();
		taskList.addAll(tasks.values());
		return taskList;
	}

	public List<User> completeAvailableUsers(String userName) {
		List<User> results = new ArrayList<>();
		List<User> groupUsers = new ArrayList<>();

		try {
			groupUsers = serviceTrackerBean.getUserService().getUserGroupUsers(prefBean.getScrumGroupId());
		} catch (SystemException var6) {
			LOG.error("Error getting users of group", var6);
		}

		for (User user : groupUsers) {
			if (user.getScreenName().toLowerCase().contains(userName.toLowerCase())) {
				results.add(user);
			}
		}

		return results;
	}


	public void setServiceTrackerBean(ServiceTrackerBean serviceTrackerBean) {
		this.serviceTrackerBean = serviceTrackerBean;
	}

	public boolean isPortletReady() {
		return prefBean.isPortletReady(false) && !statusValues.isEmpty();
	}

	public void addNewStatusValue() {
		Status newStatus = serviceTrackerBean.getStatusService().createStatus(serviceTrackerBean.getCounterService().increment(Status.class.getName()));
		newStatus.setSortIndex(this.statusValues.size());
		newStatus.setNew(true);
		this.statusValues.add(newStatus);
	}

	public void onEditStatus(RowEditEvent event) {
		Status editedStatus = (Status) event.getObject();
		FacesContext facesContext = FacesContext.getCurrentInstance();

		try {
			if (editedStatus.isNew()) {
				long statusId = serviceTrackerBean.getCounterService().increment(Status.class.getName());
				editedStatus.setStatusId(statusId);
				editedStatus.setCompanyId(prefBean.getCompanyId());
				editedStatus.setGroupId(prefBean.getGroupId());
				editedStatus.setUserGroupId(prefBean.getScrumGroupId());
			}

			editedStatus.persist();
			FacesMessageUtil.addGlobalInfoMessage(facesContext, "scrum-save-status-successful");
		} catch (Exception var6) {
			LOG.error("Error saving status", var6);
			FacesMessageUtil.addGlobalErrorMessage(facesContext, "scrum-save-status-failed");
		}

	}

	public void setStatusToDelete(Status statusToDelete) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		if (statusToDelete.getUserGroupId() == prefBean.getScrumGroupId()) {
			try {
				List<Task> statusTasks = serviceTrackerBean.getTaskService().findByG_S(prefBean.getScrumGroupId(),
						statusToDelete.getStatusId());
				if (!statusTasks.isEmpty()) {
					serviceTrackerBean.getStatusService().deleteStatus(statusToDelete);
					statusValues.remove(statusToDelete);
					FacesMessageUtil.addGlobalInfoMessage(facesContext, "scrum-delete-status-successful");
				} else {
					FacesMessageUtil.addGlobalErrorMessage(facesContext, "scrum-delete-status-impossible");
				}
			} catch (Exception var4) {
				LOG.error("Failed to delete status with ID " + statusToDelete.getStatusId());
				FacesMessageUtil.addGlobalErrorMessage(facesContext, "scrum-delete-status-failed");
			}

		}
	}

	public void setHigherStatusIndex(Status status) {
		int currentIndex = status.getSortIndex();
		if (currentIndex != statusValues.size() - 1) {
			status.setSortIndex(currentIndex + 1);
			statusValues.remove(status);
			statusValues.add(currentIndex + 1, status);
			updateStatusSorting();
		}

	}

	public void setLowerStatusIndex(Status status) {
		int currentIndex = status.getSortIndex();
		if (currentIndex != 0) {
			status.setSortIndex(currentIndex - 1);
			statusValues.remove(status);
			statusValues.add(currentIndex - 1, status);
			updateStatusSorting();
		}

	}

	private void updateStatusSorting() {
		int i = 0;

		try {
			for (Status statusValue : statusValues) {
				statusValue.setSortIndex(i);
				statusValue.persist();
			}
		} catch (SystemException var4) {
			LOG.error("Failed to update status sorting", var4);
		}

	}
}
