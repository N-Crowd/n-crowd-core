package com.ncrowd.scrum.servicetracker;

import com.liferay.portal.kernel.service.UserLocalService;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

public class UserServiceTracker extends ServiceTracker<UserLocalService, UserLocalService>{
	public UserServiceTracker(BundleContext bundleContext) {
		super(bundleContext,UserLocalService.class,null);
	}
}
