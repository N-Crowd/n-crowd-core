package com.ncrowd.scrum.bean;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.UserGroup;
import com.liferay.portal.kernel.service.persistence.UserGroupUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.ncrowd.model.Status;
import com.ncrowd.scrum.model.ColorMode;
import com.ncrowd.scrum.model.Theme;
import com.ncrowd.scrum.util.FacesMessageUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;

import org.primefaces.component.fieldset.Fieldset;
import org.primefaces.event.ToggleEvent;
import org.primefaces.push.PushContext;
import org.primefaces.push.PushContextFactory;

@ManagedBean(name = "prefBean")
@SessionScoped
public class PreferencesBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long scrumGroupId;
	private ColorMode colorMode;
	private String theme;
	private long currentSprintId = -1L;
	private boolean userInGroup;
	private transient List<Theme> availableThemes;
	private boolean pushEnabled;
	private boolean taskFieldsetHidden;
	private boolean timeFieldsetHidden;
	private boolean prefFieldsetHidden;
	private boolean statusFieldsetHidden;
	private boolean discussionFieldsetHidden;
	private boolean fileFieldsetHidden;
	private boolean sprintStatusFieldsetHidden;
	private boolean sprintMoveFieldsetHidden;
	private long companyId;
	private long groupId;
	private long userId;
	private static final Log LOG = LogFactoryUtil.getLog(PreferencesBean.class);
	@ManagedProperty("#{trackerBean}")
	private ServiceTrackerBean serviceTrackerBean;

	public void setServiceTrackerBean(ServiceTrackerBean serviceTrackerBean) {
		this.serviceTrackerBean = serviceTrackerBean;
	}
	
	public PreferencesBean() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext context = facesContext.getExternalContext();
		PortletRequest request = (PortletRequest) context.getRequest();
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute("LIFERAY_SHARED_THEME_DISPLAY");
		this.companyId = themeDisplay.getCompanyId();
		this.groupId = themeDisplay.getScopeGroupId();
		this.getUserInformation(themeDisplay);
		PortletPreferences portletPreferences = request.getPreferences();
		this.scrumGroupId = Long.parseLong(portletPreferences.getValue("scrumGroupId", "0"));
		if (this.scrumGroupId != 0L) {
			this.userInGroup = this.isUserInGroup();
		}

		this.theme = portletPreferences.getValue(this.userId + "_theme", "humanity");
		this.availableThemes = Theme.getAvailableThemes();
		this.taskFieldsetHidden = Boolean
				.parseBoolean(portletPreferences.getValue(this.userId + "_taskFieldsetHidden", Boolean.FALSE.toString()));
		this.timeFieldsetHidden = Boolean
				.parseBoolean(portletPreferences.getValue(this.userId + "_timeFieldsetHidden", Boolean.FALSE.toString()));
		this.prefFieldsetHidden = Boolean
				.parseBoolean(portletPreferences.getValue(this.userId + "_prefFieldsetHidden", Boolean.FALSE.toString()));
		this.statusFieldsetHidden = Boolean
				.parseBoolean(portletPreferences.getValue(this.userId + "_statusFieldsetHidden", Boolean.TRUE.toString()));
		this.discussionFieldsetHidden = Boolean
				.parseBoolean(portletPreferences.getValue(this.userId + "_discussionFieldsetHidden", Boolean.FALSE.toString()));
		this.fileFieldsetHidden = Boolean
				.parseBoolean(portletPreferences.getValue(this.userId + "_fileFieldsetHidden", Boolean.FALSE.toString()));
		this.sprintStatusFieldsetHidden = Boolean
				.parseBoolean(portletPreferences.getValue(this.userId + "_sprintStatusFieldsetHidden", Boolean.FALSE.toString()));
		this.sprintMoveFieldsetHidden = Boolean
				.parseBoolean(portletPreferences.getValue(this.userId + "_sprintMoveFieldsetHidden", Boolean.TRUE.toString()));
		String colorModeString = portletPreferences.getValue(this.userId + "_colorMode", ColorMode.GROUP.name());
		if (Validator.isNotNull(colorModeString)) {
			this.colorMode = ColorMode.valueOf(colorModeString);
		} else {
			this.colorMode = ColorMode.DEFAULT;
		}

		this.pushEnabled = Boolean.parseBoolean(portletPreferences.getValue(this.userId + "_pushEnabled", "false"));
	}

	private void getUserInformation(ThemeDisplay themeDisplay) {
		long currentUserId = themeDisplay.getUserId();
		long defaultUserId = 0L;

		try {
			defaultUserId = themeDisplay.getDefaultUserId();
		} catch (Exception var8) {
			LOG.error("Error getting default user ID", var8);
		}

		this.userId = currentUserId == defaultUserId ? 0L : currentUserId;
	}

	public long getScrumGroupId() {
		return this.scrumGroupId;
	}

	public void setScrumGroupId(long scrumGroupId) {
		this.scrumGroupId = scrumGroupId;
		this.userInGroup = isUserInGroup();
	}

	public ColorMode getColorMode() {
		return colorMode;
	}

	public void setColorMode(ColorMode colorMode) {
		this.saveColorMode(colorMode);
		this.colorMode = colorMode;
	}

	public String getTheme() {
		return this.theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public long getCurrentSprintId() {
		return this.currentSprintId;
	}

	public void setCurrentSprintId(long currentSprintId) {
		this.currentSprintId = currentSprintId;
	}

	public List<Theme> getAvailableThemes() {
		return this.availableThemes;
	}

	public boolean isTaskFieldsetHidden() {
		return this.taskFieldsetHidden;
	}

	public void setTaskFieldsetHidden(boolean taskFieldsetHidden) {
		this.taskFieldsetHidden = taskFieldsetHidden;
	}

	public boolean isTimeFieldsetHidden() {
		return this.timeFieldsetHidden;
	}

	public void setTimeFieldsetHidden(boolean timeFieldsetHidden) {
		this.timeFieldsetHidden = timeFieldsetHidden;
	}

	public boolean isPrefFieldsetHidden() {
		return this.prefFieldsetHidden;
	}

	public void setPrefFieldsetHidden(boolean prefFieldsetHidden) {
		this.prefFieldsetHidden = prefFieldsetHidden;
	}

	public boolean isStatusFieldsetHidden() {
		return this.statusFieldsetHidden;
	}

	public void setStatusFieldsetHidden(boolean statusFieldsetHidden) {
		this.statusFieldsetHidden = statusFieldsetHidden;
	}

	public boolean isDiscussionFieldsetHidden() {
		return this.discussionFieldsetHidden;
	}

	public void setDiscussionFieldsetHidden(boolean discussionFieldsetHidden) {
		this.discussionFieldsetHidden = discussionFieldsetHidden;
	}

	public boolean isFileFieldsetHidden() {
		return this.fileFieldsetHidden;
	}

	public void setFileFieldsetHidden(boolean fileFieldsetHidden) {
		this.fileFieldsetHidden = fileFieldsetHidden;
	}

	public boolean isSprintStatusFieldsetHidden() {
		return this.sprintStatusFieldsetHidden;
	}

	public void setSprintStatusFieldsetHidden(boolean sprintStatusFieldsetHidden) {
		this.sprintStatusFieldsetHidden = sprintStatusFieldsetHidden;
	}

	public boolean isSprintMoveFieldsetHidden() {
		return this.sprintMoveFieldsetHidden;
	}

	public void setSprintMoveFieldsetHidden(boolean sprintMoveFieldsetHidden) {
		this.sprintMoveFieldsetHidden = sprintMoveFieldsetHidden;
	}

	public boolean isPushEnabled() {
		return this.pushEnabled;
	}

	public void setPushEnabled(boolean pushEnabled) {
		this.pushEnabled = pushEnabled;
	}

	public long getCompanyId() {
		return this.companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public long getGroupId() {
		return this.groupId;
	}

	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}

	public long getUserId() {
		return this.userId;
	}

	public String getUserIdString() {
		return String.valueOf(this.userId);
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public boolean isPortletReady(boolean showErrorMessages) {
		this.userInGroup = this.isUserInGroup();
		if (this.scrumGroupId == 0L) {
			if (showErrorMessages) {
				FacesMessageUtil.addGlobalErrorMessage(FacesContext.getCurrentInstance(),
						"scrum-please-configure-group");
			}

			return false;
		} else if (this.userId == 0L) {
			if (showErrorMessages) {
				FacesMessageUtil.addGlobalErrorMessage(FacesContext.getCurrentInstance(), "scrum-please-login");
			}

			return false;
		} else if (!this.userInGroup) {
			if (showErrorMessages) {
				FacesMessageUtil.addGlobalErrorMessage(FacesContext.getCurrentInstance(),
						"scrum-permission-not-in-group");
			}

			return false;
		} else {
			return true;
		}
	}

	private boolean isUserInGroup() {
		boolean isUserInGroup = false;

		try {
			isUserInGroup = UserGroupUtil.containsUser(this.scrumGroupId, this.userId);
		} catch (SystemException var3) {
			LOG.error("Failed to check if user " + this.userId, var3);
		}

		return isUserInGroup;
	}

	public List<UserGroup> getAvailableUserGroups() {
		List<UserGroup> userGroups = new ArrayList<>();
		try {
			userGroups = serviceTrackerBean.getUserGroupService().getUserGroups(this.companyId);
		} catch (SystemException var3) {
			LOG.error("Error getting user groups of company " + this.companyId, var3);
		}
		return userGroups;
	}

	public PreferencesBean submitForm() {
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		PortletRequest request = (PortletRequest) context.getRequest();
		PortletPreferences portletPreferences = request.getPreferences();

		try {
			portletPreferences.setValue("scrumGroupId", String.valueOf(this.scrumGroupId));
			portletPreferences.setValue(this.userId + "_theme", this.theme);
			portletPreferences.setValue(this.userId + "_pushEnabled", String.valueOf(this.pushEnabled));
			portletPreferences.store();
		} catch (Exception var4) {
			FacesMessageUtil.addGlobalErrorMessage(FacesContext.getCurrentInstance(),
					"an-error-occurred-while-updating-your-preferences");
		}

		FacesMessageUtil.addGlobalInfoMessage(FacesContext.getCurrentInstance(),
				"you-have-successfully-updated-your-preferences");
		return this;
	}

	public List<Status> getStatusValues() {
		List<Status> statusValues = new ArrayList<>();

		try {
			statusValues = serviceTrackerBean.getStatusService().findByUserGroupId(this.scrumGroupId);
		} catch (SystemException var3) {
			LOG.error("Error getting the status values of group " + this.scrumGroupId, var3);
		}

		return  statusValues;
	}

	public void handleToggleFieldset(ToggleEvent event) {
		Object source = event.getSource();
		if (source instanceof Fieldset) {
			String id = ((Fieldset) source).getId();
			if (id.equals("taskFieldset")) {
				this.taskFieldsetHidden = !this.taskFieldsetHidden;
			} else if (id.equals("timeFieldset")) {
				this.timeFieldsetHidden = !this.timeFieldsetHidden;
			} else if (id.equals("preferencesFieldset")) {
				this.prefFieldsetHidden = !this.prefFieldsetHidden;
			} else if (id.equals("statusFieldset")) {
				this.statusFieldsetHidden = !this.statusFieldsetHidden;
			} else if (id.equals("discussionFieldset")) {
				this.discussionFieldsetHidden = !this.discussionFieldsetHidden;
			} else if (id.equals("fileFieldset")) {
				this.fileFieldsetHidden = !this.fileFieldsetHidden;
			} else if (id.equals("sprintStatusFieldset")) {
				this.sprintStatusFieldsetHidden = !this.sprintStatusFieldsetHidden;
			} else if (id.equals("sprintMoveFieldset")) {
				this.sprintMoveFieldsetHidden = !this.sprintMoveFieldsetHidden;
			}

			this.saveFieldsetVisibility();
		}

	}

	private void saveFieldsetVisibility() {
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		PortletRequest request = (PortletRequest) context.getRequest();
		PortletPreferences portletPreferences = request.getPreferences();
		try {
			portletPreferences.setValue(this.userId + "_taskFieldsetHidden", String.valueOf(this.taskFieldsetHidden));
			portletPreferences.setValue(this.userId + "_timeFieldsetHidden", String.valueOf(this.timeFieldsetHidden));
			portletPreferences.setValue(this.userId + "_prefFieldsetHidden", String.valueOf(this.prefFieldsetHidden));
			portletPreferences.setValue(this.userId + "_statusFieldsetHidden",
					String.valueOf(this.statusFieldsetHidden));
			portletPreferences.setValue(this.userId + "_discussionFieldsetHidden",
					String.valueOf(this.discussionFieldsetHidden));
			portletPreferences.setValue(this.userId + "_fileFieldsetHidden", String.valueOf(this.fileFieldsetHidden));
			portletPreferences.setValue(this.userId + "_sprintStatusFieldsetHidden",
					String.valueOf(this.sprintStatusFieldsetHidden));
			portletPreferences.setValue(this.userId + "_sprintMoveFieldsetHidden",
					String.valueOf(this.sprintMoveFieldsetHidden));
			portletPreferences.store();
		} catch (Exception var5) {
			LOG.error("Error saving portlet preferences", var5);
		}

	}

	private void saveColorMode(ColorMode colorMode) {
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		PortletRequest request = (PortletRequest) context.getRequest();
		PortletPreferences portletPreferences = request.getPreferences();
		try {
			portletPreferences.setValue(userId + "_colorMode", colorMode.name());
			portletPreferences.store();
		} catch (Exception var6) {
			LOG.error("Error saving portlet preferences", var6);
		}

	}

	@SuppressWarnings("deprecation")
	public void sendPushMessage(String channel, String value) {
//		PushContextFactory factory=PushContextFactory.getDefault();
//		System.out.println("push context factory is : "+factory);
//		PushContext context=factory.getPushContext();
//		System.out.println("push context is : "+context);
//		PushContext pushContext = PushContextFactory.getDefault().getPushContext();
//		pushContext.push(channel, value);
	}
}
