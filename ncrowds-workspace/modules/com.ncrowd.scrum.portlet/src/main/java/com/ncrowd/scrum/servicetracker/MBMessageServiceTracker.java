package com.ncrowd.scrum.servicetracker;

import com.liferay.message.boards.service.MBMessageLocalService;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

public class MBMessageServiceTracker extends ServiceTracker<MBMessageLocalService, MBMessageLocalService>{
	public MBMessageServiceTracker(BundleContext bundleContext){
		super(bundleContext, MBMessageLocalService.class, null);
	}
}
