/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.ncrowd.model.Sprint;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The cache model class for representing Sprint in entity cache.
 *
 * @author Sudhanshu
 * @generated
 */
@ProviderType
public class SprintCacheModel implements CacheModel<Sprint>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof SprintCacheModel)) {
			return false;
		}

		SprintCacheModel sprintCacheModel = (SprintCacheModel)obj;

		if (sprintId == sprintCacheModel.sprintId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, sprintId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(17);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", sprintId=");
		sb.append(sprintId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", userGroupId=");
		sb.append(userGroupId);
		sb.append(", startDate=");
		sb.append(startDate);
		sb.append(", endDate=");
		sb.append(endDate);
		sb.append(", sprintNumber=");
		sb.append(sprintNumber);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Sprint toEntityModel() {
		SprintImpl sprintImpl = new SprintImpl();

		if (uuid == null) {
			sprintImpl.setUuid("");
		}
		else {
			sprintImpl.setUuid(uuid);
		}

		sprintImpl.setSprintId(sprintId);
		sprintImpl.setCompanyId(companyId);
		sprintImpl.setGroupId(groupId);
		sprintImpl.setUserGroupId(userGroupId);

		if (startDate == Long.MIN_VALUE) {
			sprintImpl.setStartDate(null);
		}
		else {
			sprintImpl.setStartDate(new Date(startDate));
		}

		if (endDate == Long.MIN_VALUE) {
			sprintImpl.setEndDate(null);
		}
		else {
			sprintImpl.setEndDate(new Date(endDate));
		}

		sprintImpl.setSprintNumber(sprintNumber);

		sprintImpl.resetOriginalValues();

		return sprintImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		sprintId = objectInput.readLong();

		companyId = objectInput.readLong();

		groupId = objectInput.readLong();

		userGroupId = objectInput.readLong();
		startDate = objectInput.readLong();
		endDate = objectInput.readLong();

		sprintNumber = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(sprintId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(userGroupId);
		objectOutput.writeLong(startDate);
		objectOutput.writeLong(endDate);

		objectOutput.writeInt(sprintNumber);
	}

	public String uuid;
	public long sprintId;
	public long companyId;
	public long groupId;
	public long userGroupId;
	public long startDate;
	public long endDate;
	public int sprintNumber;

}