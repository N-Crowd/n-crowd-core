/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.ncrowd.model.DocumentMapping;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The cache model class for representing DocumentMapping in entity cache.
 *
 * @author Sudhanshu
 * @generated
 */
@ProviderType
public class DocumentMappingCacheModel
	implements CacheModel<DocumentMapping>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DocumentMappingCacheModel)) {
			return false;
		}

		DocumentMappingCacheModel documentMappingCacheModel =
			(DocumentMappingCacheModel)obj;

		if (mappingId == documentMappingCacheModel.mappingId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, mappingId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", mappingId=");
		sb.append(mappingId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", userGroupId=");
		sb.append(userGroupId);
		sb.append(", taskId=");
		sb.append(taskId);
		sb.append(", dlFileEntryId=");
		sb.append(dlFileEntryId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public DocumentMapping toEntityModel() {
		DocumentMappingImpl documentMappingImpl = new DocumentMappingImpl();

		if (uuid == null) {
			documentMappingImpl.setUuid("");
		}
		else {
			documentMappingImpl.setUuid(uuid);
		}

		documentMappingImpl.setMappingId(mappingId);
		documentMappingImpl.setCompanyId(companyId);
		documentMappingImpl.setGroupId(groupId);
		documentMappingImpl.setUserGroupId(userGroupId);
		documentMappingImpl.setTaskId(taskId);
		documentMappingImpl.setDlFileEntryId(dlFileEntryId);

		documentMappingImpl.resetOriginalValues();

		return documentMappingImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		mappingId = objectInput.readLong();

		companyId = objectInput.readLong();

		groupId = objectInput.readLong();

		userGroupId = objectInput.readLong();

		taskId = objectInput.readLong();

		dlFileEntryId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(mappingId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(userGroupId);

		objectOutput.writeLong(taskId);

		objectOutput.writeLong(dlFileEntryId);
	}

	public String uuid;
	public long mappingId;
	public long companyId;
	public long groupId;
	public long userGroupId;
	public long taskId;
	public long dlFileEntryId;

}