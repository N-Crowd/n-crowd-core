/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.model.impl;

import com.ncrowd.model.Status;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The extended model implementation for the Status service. Represents a row in the &quot;NCrowd_Status&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>com.ncrowd.model.Status<code> interface.
 * </p>
 *
 * @author Sudhanshu
 */
@ProviderType
public class StatusImpl extends StatusBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a status model instance should use the {@link com.ncrowd.model.Status} interface instead.
	 */
	public StatusImpl() {
	}

	public int compareTo(Status status) {
		return (new Integer(this.getSortIndex())).compareTo(status.getSortIndex());
	}

	public String getFormattedName() {
		String formattedName = this.getName().trim();
		formattedName = formattedName.replace(" ", "_");
		formattedName = formattedName.toLowerCase();
		return formattedName;
	}

}