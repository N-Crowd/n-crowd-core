/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.ncrowd.model.Status;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The cache model class for representing Status in entity cache.
 *
 * @author Sudhanshu
 * @generated
 */
@ProviderType
public class StatusCacheModel implements CacheModel<Status>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof StatusCacheModel)) {
			return false;
		}

		StatusCacheModel statusCacheModel = (StatusCacheModel)obj;

		if (statusId == statusCacheModel.statusId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, statusId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(17);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", statusId=");
		sb.append(statusId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", userGroupId=");
		sb.append(userGroupId);
		sb.append(", name=");
		sb.append(name);
		sb.append(", sortIndex=");
		sb.append(sortIndex);
		sb.append(", fixedStatus=");
		sb.append(fixedStatus);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Status toEntityModel() {
		StatusImpl statusImpl = new StatusImpl();

		if (uuid == null) {
			statusImpl.setUuid("");
		}
		else {
			statusImpl.setUuid(uuid);
		}

		statusImpl.setStatusId(statusId);
		statusImpl.setCompanyId(companyId);
		statusImpl.setGroupId(groupId);
		statusImpl.setUserGroupId(userGroupId);

		if (name == null) {
			statusImpl.setName("");
		}
		else {
			statusImpl.setName(name);
		}

		statusImpl.setSortIndex(sortIndex);
		statusImpl.setFixedStatus(fixedStatus);

		statusImpl.resetOriginalValues();

		return statusImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		statusId = objectInput.readLong();

		companyId = objectInput.readLong();

		groupId = objectInput.readLong();

		userGroupId = objectInput.readLong();
		name = objectInput.readUTF();

		sortIndex = objectInput.readInt();

		fixedStatus = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(statusId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(userGroupId);

		if (name == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(name);
		}

		objectOutput.writeInt(sortIndex);

		objectOutput.writeBoolean(fixedStatus);
	}

	public String uuid;
	public long statusId;
	public long companyId;
	public long groupId;
	public long userGroupId;
	public String name;
	public int sortIndex;
	public boolean fixedStatus;

}