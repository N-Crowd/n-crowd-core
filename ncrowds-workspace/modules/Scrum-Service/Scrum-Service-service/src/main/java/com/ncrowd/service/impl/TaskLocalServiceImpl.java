/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.service.impl;


import com.liferay.message.boards.service.MBMessageLocalService;
import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.GroupLocalServiceUtil;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.ncrowd.model.Task;
import com.ncrowd.service.base.TaskLocalServiceBaseImpl;

import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * The implementation of the task local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>com.ncrowd.service.TaskLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Sudhanshu
 * @see TaskLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=com.ncrowd.model.Task",
	service = AopService.class
)
public class TaskLocalServiceImpl extends TaskLocalServiceBaseImpl {
 
	@Reference
	private MBMessageLocalService mbLocalService;
	private static final Log LOG=LogFactoryUtil.getLog(TaskLocalServiceImpl.class);
    
    public List<Task> findByUserGroupId(final long userGroupId)  {
        return taskPersistence.findByUserGroupId(userGroupId, false);
    }
    
    public List<Task> findByG_S_S(final long userGroupId, final long sprintId, final long statusId)  {
        return taskPersistence.findByG_S_S(userGroupId, sprintId, statusId, false);
    }
    
    public List<Task> findByG_S(final long userGroupId, final long sprintId)  {
        return taskPersistence.findByG_S(userGroupId, sprintId, false);
    }
    
    public List<Task> findTaskGroups(final long userGroupId)  {
        return taskPersistence.findByTaskGroup(userGroupId, true);
    }
    
    public List<Task> findTaskGroupByName(final long userGroupId, final String name)  {
        return taskPersistence.findByTaskGroupName(userGroupId, name, true);
    }
    
    public Task addTask(final Task task)  {
        this.addDefaultDiscussion(task);
        return super.addTask(task);
    }
    
	private void addDefaultDiscussion(final Task task)  {
		final long taskId = task.getTaskId();
		final long companyId = task.getCompanyId();
		long defaultUserId = 0L;
		long groupId = 0L;
		try {
			final User defaultUser = UserLocalServiceUtil.getDefaultUser(companyId);
			defaultUserId = defaultUser.getUserId();
			groupId = GroupLocalServiceUtil.getGroup(companyId, "Guest").getGroupId();
		} catch (PortalException e) {
			TaskLocalServiceImpl.LOG.error(
					(Object) ("Error getting default user or guest group of company " + companyId), (Throwable) e);
		}
		try {
			mbLocalService.addDiscussionMessage(defaultUserId, "", groupId, Task.class.getName(), taskId, 1);
		} catch (PortalException e) {
			TaskLocalServiceImpl.LOG.error(("Error creating default discussion message for task " + taskId),
					(Throwable) e);
		}
	}
	
	 
}