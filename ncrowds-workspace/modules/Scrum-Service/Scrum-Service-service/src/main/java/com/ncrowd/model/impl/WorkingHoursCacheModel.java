/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.ncrowd.model.WorkingHours;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The cache model class for representing WorkingHours in entity cache.
 *
 * @author Sudhanshu
 * @generated
 */
@ProviderType
public class WorkingHoursCacheModel
	implements CacheModel<WorkingHours>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof WorkingHoursCacheModel)) {
			return false;
		}

		WorkingHoursCacheModel workingHoursCacheModel =
			(WorkingHoursCacheModel)obj;

		if (workingHoursId == workingHoursCacheModel.workingHoursId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, workingHoursId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", workingHoursId=");
		sb.append(workingHoursId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", userGroupId=");
		sb.append(userGroupId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", taskId=");
		sb.append(taskId);
		sb.append(", hours=");
		sb.append(hours);
		sb.append(", startDate=");
		sb.append(startDate);
		sb.append(", comment=");
		sb.append(comment);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public WorkingHours toEntityModel() {
		WorkingHoursImpl workingHoursImpl = new WorkingHoursImpl();

		if (uuid == null) {
			workingHoursImpl.setUuid("");
		}
		else {
			workingHoursImpl.setUuid(uuid);
		}

		workingHoursImpl.setWorkingHoursId(workingHoursId);
		workingHoursImpl.setCompanyId(companyId);
		workingHoursImpl.setGroupId(groupId);
		workingHoursImpl.setUserGroupId(userGroupId);
		workingHoursImpl.setUserId(userId);

		if (createDate == Long.MIN_VALUE) {
			workingHoursImpl.setCreateDate(null);
		}
		else {
			workingHoursImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			workingHoursImpl.setModifiedDate(null);
		}
		else {
			workingHoursImpl.setModifiedDate(new Date(modifiedDate));
		}

		workingHoursImpl.setTaskId(taskId);
		workingHoursImpl.setHours(hours);

		if (startDate == Long.MIN_VALUE) {
			workingHoursImpl.setStartDate(null);
		}
		else {
			workingHoursImpl.setStartDate(new Date(startDate));
		}

		if (comment == null) {
			workingHoursImpl.setComment("");
		}
		else {
			workingHoursImpl.setComment(comment);
		}

		workingHoursImpl.resetOriginalValues();

		return workingHoursImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		workingHoursId = objectInput.readLong();

		companyId = objectInput.readLong();

		groupId = objectInput.readLong();

		userGroupId = objectInput.readLong();

		userId = objectInput.readLong();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		taskId = objectInput.readLong();

		hours = objectInput.readDouble();
		startDate = objectInput.readLong();
		comment = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(workingHoursId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(userGroupId);

		objectOutput.writeLong(userId);
		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(taskId);

		objectOutput.writeDouble(hours);
		objectOutput.writeLong(startDate);

		if (comment == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(comment);
		}
	}

	public String uuid;
	public long workingHoursId;
	public long companyId;
	public long groupId;
	public long userGroupId;
	public long userId;
	public long createDate;
	public long modifiedDate;
	public long taskId;
	public double hours;
	public long startDate;
	public String comment;

}