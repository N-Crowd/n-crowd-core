/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.model.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.ncrowd.model.Status;
import com.ncrowd.model.Task;
import com.ncrowd.model.WorkingHours;
import com.ncrowd.service.StatusLocalServiceUtil;
import com.ncrowd.service.TaskLocalServiceUtil;
import com.ncrowd.service.WorkingHoursLocalServiceUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The extended model implementation for the Task service. Represents a row in the &quot;NCrowd_Task&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>com.ncrowd.model.Task<code> interface.
 * </p>
 *
 * @author Sudhanshu
 */
@ProviderType
public class TaskImpl extends TaskBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a task model instance should use the {@link com.ncrowd.model.Task} interface instead.
	 */
	public TaskImpl() {
	}
	
	private WorkingHours timeToSave;
	private List<WorkingHours> workingHours;
	private Task groupTask;
	private double timeForCopy;
	
	private static final float DEFAULT_WORKING_HOURS = 4.0F;
	private static final Log LOG = LogFactoryUtil.getLog(TaskImpl.class);

	public double getTimeForCopy() {
		return this.timeForCopy;
	}

	public void setTimeForCopy(double timeForCopy) {
		this.timeForCopy = timeForCopy;
	}
	
	@Override
	public int compareTo(Task task) {
		return (new Integer(this.getSortIndex())).compareTo(task.getSortIndex());
	}

	public WorkingHours getTimeToSave() {
		if (this.timeToSave == null) {
			this.timeToSave = new WorkingHoursImpl();
			this.timeToSave.setNew(true);
			this.timeToSave.setHours((double) DEFAULT_WORKING_HOURS);
			this.timeToSave.setTaskId(this.getTaskId());
		}

		return this.timeToSave;
	}
	
	public void setWorkingHours(List<WorkingHours> hours) {
		this.workingHours=hours;
	}
	
	public void setTimeToSave(WorkingHours timeToSave) {
		this.timeToSave = timeToSave;
	}

	public void setGroupTask(Task groupTask) {
		this.groupTask = groupTask;
	}

	public Task getGroupTask() {
		if (this.getTaskGroupId() != 0L) {
			try {
				this.groupTask = TaskLocalServiceUtil.getTask(this.getTaskGroupId());
			} catch (Exception var2) {
				LOG.error("Error getting task group of task " + this.getTaskId(), var2);
			}
		}

		return this.groupTask;
	}

	public Status getStatus() {
		try {
			return StatusLocalServiceUtil.getStatus(this.getStatusId());
		} catch (Exception var2) {
			LOG.error("Error getting status of task", var2);
			return null;
		}
	}

	public void setStatus(Status status) {
		this.setStatusId(status.getPrimaryKey());
	}

	public String getAssignedToName() {
		User user = this.getAssignedToUser();
		return user != null ? user.getScreenName() : "-";
	}

	public User getAssignedToUser() {
		User user = null;
		try {
			user = UserLocalServiceUtil.fetchUser(this.getAssignedTo());
		} catch (SystemException var3) {
			LOG.error("Error getting user assigned to task " + this.getTaskId(), var3);
		}

		return user;
	}

	public void setAssignedToUser(User user) {
		if (user != null) {
			this.setAssignedTo(user.getUserId());
		} else {
			this.setAssignedTo(0L);
		}

	}

	public List<WorkingHours> getWorkingHours() {
		if (workingHours == null) {
			workingHours = new ArrayList<>();
			try {
				List<WorkingHours> loadedWorkingHours = WorkingHoursLocalServiceUtil.findByTaskId(this.getTaskId());
				workingHours.addAll(loadedWorkingHours);
			} catch (SystemException e) {
				LOG.error("Failed to get the working hours for task " + this.getTaskId(), e);
			}
		}
		return workingHours;
	}

	public float getTotalWorkingHours() {
		float totalHours = 0.0F;

		for (WorkingHours hours : getWorkingHours()) {
			totalHours=(float)((double)totalHours + hours.getHours());
		}

		return totalHours;
	}

	public String getStatusTableClass() {
		if ((double) this.getTotalWorkingHours() - this.getEvaluation() > 0.0D) {
			return "overtimeTask";
		} else {
			return this.getStatus().isFixedStatus()
					&& (double) this.getTotalWorkingHours() - this.getEvaluation() < 0.0D ? "undertimeTask" : "";
		}
	}
	
	public static Date addHours(Date date, double h) {
		Calendar newDate = Calendar.getInstance();
		newDate.setTime(date);
		newDate.add(11, (int) Math.floor(h));
		newDate.add(12, (int) (h % 1.0D * 60.0D));
		return newDate.getTime();
	}
	
	public static boolean isToday(Date checkDate) {
		Calendar today = Calendar.getInstance();
		Calendar check = Calendar.getInstance();
		check.setTime(checkDate);
		return today.get(5) == check.get(5) && today.get(2) == check.get(2) && today.get(1) == check.get(1);
	}
}