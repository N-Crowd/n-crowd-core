/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.model.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The extended model implementation for the WorkingHours service. Represents a row in the &quot;NCrowd_WorkingHours&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>com.ncrowd.model.WorkingHours<code> interface.
 * </p>
 *
 * @author Sudhanshu
 */
@ProviderType
public class WorkingHoursImpl extends WorkingHoursBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a working hours model instance should use the {@link com.ncrowd.model.WorkingHours} interface instead.
	 */
	public WorkingHoursImpl() {
	}
	private static final Log LOG = LogFactoryUtil.getLog(WorkingHoursImpl.class);

	   public String getEmployeeName() {
	      String employeeName = "-";

	      try {
	         User user = UserLocalServiceUtil.fetchUser(this.getUserId());
	         if (user != null) {
	            employeeName = user.getScreenName();
	         }
	      } catch (SystemException var3) {
	         LOG.error("Failed to get user with ID " + this.getUserId(), var3);
	      }

	      return employeeName;
	   }

}