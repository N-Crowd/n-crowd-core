/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.SystemException;
import com.ncrowd.model.DocumentMapping;
import com.ncrowd.service.base.DocumentMappingLocalServiceBaseImpl;

import java.util.List;

import org.osgi.service.component.annotations.Component;

/**
 * The implementation of the document mapping local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>com.ncrowd.service.DocumentMappingLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Sudhanshu
 * @see DocumentMappingLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=com.ncrowd.model.DocumentMapping",
	service = AopService.class
)
public class DocumentMappingLocalServiceImpl
	extends DocumentMappingLocalServiceBaseImpl {

	public List<DocumentMapping> findByTaskId(final long taskId) throws SystemException {
		return documentMappingPersistence.findByTaskId(taskId);
	}

	public List<DocumentMapping> findByT_D(final long taskId, final long dlFileEntryId) throws SystemException {
		return documentMappingPersistence.findByT_D(taskId, dlFileEntryId);
	}
}