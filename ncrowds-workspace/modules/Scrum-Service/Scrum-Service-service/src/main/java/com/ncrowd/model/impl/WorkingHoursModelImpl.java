/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.model.impl;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.expando.kernel.util.ExpandoBridgeFactoryUtil;
import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.impl.BaseModelImpl;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.ProxyUtil;

import com.ncrowd.model.WorkingHours;
import com.ncrowd.model.WorkingHoursModel;

import java.io.Serializable;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;

import java.sql.Types;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The base model implementation for the WorkingHours service. Represents a row in the &quot;Scrum_WorkingHours&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface </code>WorkingHoursModel</code> exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link WorkingHoursImpl}.
 * </p>
 *
 * @author Sudhanshu
 * @see WorkingHoursImpl
 * @generated
 */
@ProviderType
public class WorkingHoursModelImpl
	extends BaseModelImpl<WorkingHours> implements WorkingHoursModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a working hours model instance should use the <code>WorkingHours</code> interface instead.
	 */
	public static final String TABLE_NAME = "Scrum_WorkingHours";

	public static final Object[][] TABLE_COLUMNS = {
		{"uuid_", Types.VARCHAR}, {"workingHoursId", Types.BIGINT},
		{"companyId", Types.BIGINT}, {"groupId", Types.BIGINT},
		{"userGroupId", Types.BIGINT}, {"userId", Types.BIGINT},
		{"createDate", Types.TIMESTAMP}, {"modifiedDate", Types.TIMESTAMP},
		{"taskId", Types.BIGINT}, {"hours", Types.DOUBLE},
		{"startDate", Types.TIMESTAMP}, {"comment_", Types.VARCHAR}
	};

	public static final Map<String, Integer> TABLE_COLUMNS_MAP =
		new HashMap<String, Integer>();

	static {
		TABLE_COLUMNS_MAP.put("uuid_", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("workingHoursId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("companyId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("groupId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("userGroupId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("userId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("createDate", Types.TIMESTAMP);
		TABLE_COLUMNS_MAP.put("modifiedDate", Types.TIMESTAMP);
		TABLE_COLUMNS_MAP.put("taskId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("hours", Types.DOUBLE);
		TABLE_COLUMNS_MAP.put("startDate", Types.TIMESTAMP);
		TABLE_COLUMNS_MAP.put("comment_", Types.VARCHAR);
	}

	public static final String TABLE_SQL_CREATE =
		"create table Scrum_WorkingHours (uuid_ VARCHAR(75) null,workingHoursId LONG not null primary key,companyId LONG,groupId LONG,userGroupId LONG,userId LONG,createDate DATE null,modifiedDate DATE null,taskId LONG,hours DOUBLE,startDate DATE null,comment_ TEXT null)";

	public static final String TABLE_SQL_DROP = "drop table Scrum_WorkingHours";

	public static final String ORDER_BY_JPQL =
		" ORDER BY workingHours.startDate ASC";

	public static final String ORDER_BY_SQL =
		" ORDER BY Scrum_WorkingHours.startDate ASC";

	public static final String DATA_SOURCE = "liferayDataSource";

	public static final String SESSION_FACTORY = "liferaySessionFactory";

	public static final String TX_MANAGER = "liferayTransactionManager";

	public static final long COMPANYID_COLUMN_BITMASK = 1L;

	public static final long GROUPID_COLUMN_BITMASK = 2L;

	public static final long TASKID_COLUMN_BITMASK = 4L;

	public static final long USERGROUPID_COLUMN_BITMASK = 8L;

	public static final long USERID_COLUMN_BITMASK = 16L;

	public static final long UUID_COLUMN_BITMASK = 32L;

	public static final long STARTDATE_COLUMN_BITMASK = 64L;

	public static void setEntityCacheEnabled(boolean entityCacheEnabled) {
		_entityCacheEnabled = entityCacheEnabled;
	}

	public static void setFinderCacheEnabled(boolean finderCacheEnabled) {
		_finderCacheEnabled = finderCacheEnabled;
	}

	public WorkingHoursModelImpl() {
	}

	@Override
	public long getPrimaryKey() {
		return _workingHoursId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setWorkingHoursId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _workingHoursId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Class<?> getModelClass() {
		return WorkingHours.class;
	}

	@Override
	public String getModelClassName() {
		return WorkingHours.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		Map<String, Function<WorkingHours, Object>> attributeGetterFunctions =
			getAttributeGetterFunctions();

		for (Map.Entry<String, Function<WorkingHours, Object>> entry :
				attributeGetterFunctions.entrySet()) {

			String attributeName = entry.getKey();
			Function<WorkingHours, Object> attributeGetterFunction =
				entry.getValue();

			attributes.put(
				attributeName,
				attributeGetterFunction.apply((WorkingHours)this));
		}

		attributes.put("entityCacheEnabled", isEntityCacheEnabled());
		attributes.put("finderCacheEnabled", isFinderCacheEnabled());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Map<String, BiConsumer<WorkingHours, Object>>
			attributeSetterBiConsumers = getAttributeSetterBiConsumers();

		for (Map.Entry<String, Object> entry : attributes.entrySet()) {
			String attributeName = entry.getKey();

			BiConsumer<WorkingHours, Object> attributeSetterBiConsumer =
				attributeSetterBiConsumers.get(attributeName);

			if (attributeSetterBiConsumer != null) {
				attributeSetterBiConsumer.accept(
					(WorkingHours)this, entry.getValue());
			}
		}
	}

	public Map<String, Function<WorkingHours, Object>>
		getAttributeGetterFunctions() {

		return _attributeGetterFunctions;
	}

	public Map<String, BiConsumer<WorkingHours, Object>>
		getAttributeSetterBiConsumers() {

		return _attributeSetterBiConsumers;
	}

	private static Function<InvocationHandler, WorkingHours>
		_getProxyProviderFunction() {

		Class<?> proxyClass = ProxyUtil.getProxyClass(
			WorkingHours.class.getClassLoader(), WorkingHours.class,
			ModelWrapper.class);

		try {
			Constructor<WorkingHours> constructor =
				(Constructor<WorkingHours>)proxyClass.getConstructor(
					InvocationHandler.class);

			return invocationHandler -> {
				try {
					return constructor.newInstance(invocationHandler);
				}
				catch (ReflectiveOperationException roe) {
					throw new InternalError(roe);
				}
			};
		}
		catch (NoSuchMethodException nsme) {
			throw new InternalError(nsme);
		}
	}

	private static final Map<String, Function<WorkingHours, Object>>
		_attributeGetterFunctions;
	private static final Map<String, BiConsumer<WorkingHours, Object>>
		_attributeSetterBiConsumers;

	static {
		Map<String, Function<WorkingHours, Object>> attributeGetterFunctions =
			new LinkedHashMap<String, Function<WorkingHours, Object>>();
		Map<String, BiConsumer<WorkingHours, ?>> attributeSetterBiConsumers =
			new LinkedHashMap<String, BiConsumer<WorkingHours, ?>>();

		attributeGetterFunctions.put("uuid", WorkingHours::getUuid);
		attributeSetterBiConsumers.put(
			"uuid", (BiConsumer<WorkingHours, String>)WorkingHours::setUuid);
		attributeGetterFunctions.put(
			"workingHoursId", WorkingHours::getWorkingHoursId);
		attributeSetterBiConsumers.put(
			"workingHoursId",
			(BiConsumer<WorkingHours, Long>)WorkingHours::setWorkingHoursId);
		attributeGetterFunctions.put("companyId", WorkingHours::getCompanyId);
		attributeSetterBiConsumers.put(
			"companyId",
			(BiConsumer<WorkingHours, Long>)WorkingHours::setCompanyId);
		attributeGetterFunctions.put("groupId", WorkingHours::getGroupId);
		attributeSetterBiConsumers.put(
			"groupId",
			(BiConsumer<WorkingHours, Long>)WorkingHours::setGroupId);
		attributeGetterFunctions.put(
			"userGroupId", WorkingHours::getUserGroupId);
		attributeSetterBiConsumers.put(
			"userGroupId",
			(BiConsumer<WorkingHours, Long>)WorkingHours::setUserGroupId);
		attributeGetterFunctions.put("userId", WorkingHours::getUserId);
		attributeSetterBiConsumers.put(
			"userId", (BiConsumer<WorkingHours, Long>)WorkingHours::setUserId);
		attributeGetterFunctions.put("createDate", WorkingHours::getCreateDate);
		attributeSetterBiConsumers.put(
			"createDate",
			(BiConsumer<WorkingHours, Date>)WorkingHours::setCreateDate);
		attributeGetterFunctions.put(
			"modifiedDate", WorkingHours::getModifiedDate);
		attributeSetterBiConsumers.put(
			"modifiedDate",
			(BiConsumer<WorkingHours, Date>)WorkingHours::setModifiedDate);
		attributeGetterFunctions.put("taskId", WorkingHours::getTaskId);
		attributeSetterBiConsumers.put(
			"taskId", (BiConsumer<WorkingHours, Long>)WorkingHours::setTaskId);
		attributeGetterFunctions.put("hours", WorkingHours::getHours);
		attributeSetterBiConsumers.put(
			"hours", (BiConsumer<WorkingHours, Double>)WorkingHours::setHours);
		attributeGetterFunctions.put("startDate", WorkingHours::getStartDate);
		attributeSetterBiConsumers.put(
			"startDate",
			(BiConsumer<WorkingHours, Date>)WorkingHours::setStartDate);
		attributeGetterFunctions.put("comment", WorkingHours::getComment);
		attributeSetterBiConsumers.put(
			"comment",
			(BiConsumer<WorkingHours, String>)WorkingHours::setComment);

		_attributeGetterFunctions = Collections.unmodifiableMap(
			attributeGetterFunctions);
		_attributeSetterBiConsumers = Collections.unmodifiableMap(
			(Map)attributeSetterBiConsumers);
	}

	@Override
	public String getUuid() {
		if (_uuid == null) {
			return "";
		}
		else {
			return _uuid;
		}
	}

	@Override
	public void setUuid(String uuid) {
		_columnBitmask |= UUID_COLUMN_BITMASK;

		if (_originalUuid == null) {
			_originalUuid = _uuid;
		}

		_uuid = uuid;
	}

	public String getOriginalUuid() {
		return GetterUtil.getString(_originalUuid);
	}

	@Override
	public long getWorkingHoursId() {
		return _workingHoursId;
	}

	@Override
	public void setWorkingHoursId(long workingHoursId) {
		_workingHoursId = workingHoursId;
	}

	@Override
	public long getCompanyId() {
		return _companyId;
	}

	@Override
	public void setCompanyId(long companyId) {
		_columnBitmask |= COMPANYID_COLUMN_BITMASK;

		if (!_setOriginalCompanyId) {
			_setOriginalCompanyId = true;

			_originalCompanyId = _companyId;
		}

		_companyId = companyId;
	}

	public long getOriginalCompanyId() {
		return _originalCompanyId;
	}

	@Override
	public long getGroupId() {
		return _groupId;
	}

	@Override
	public void setGroupId(long groupId) {
		_columnBitmask |= GROUPID_COLUMN_BITMASK;

		if (!_setOriginalGroupId) {
			_setOriginalGroupId = true;

			_originalGroupId = _groupId;
		}

		_groupId = groupId;
	}

	public long getOriginalGroupId() {
		return _originalGroupId;
	}

	@Override
	public long getUserGroupId() {
		return _userGroupId;
	}

	@Override
	public void setUserGroupId(long userGroupId) {
		_columnBitmask |= USERGROUPID_COLUMN_BITMASK;

		if (!_setOriginalUserGroupId) {
			_setOriginalUserGroupId = true;

			_originalUserGroupId = _userGroupId;
		}

		_userGroupId = userGroupId;
	}

	public long getOriginalUserGroupId() {
		return _originalUserGroupId;
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_columnBitmask |= USERID_COLUMN_BITMASK;

		if (!_setOriginalUserId) {
			_setOriginalUserId = true;

			_originalUserId = _userId;
		}

		_userId = userId;
	}

	@Override
	public String getUserUuid() {
		try {
			User user = UserLocalServiceUtil.getUserById(getUserId());

			return user.getUuid();
		}
		catch (PortalException pe) {
			return "";
		}
	}

	@Override
	public void setUserUuid(String userUuid) {
	}

	public long getOriginalUserId() {
		return _originalUserId;
	}

	@Override
	public Date getCreateDate() {
		return _createDate;
	}

	@Override
	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	@Override
	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public boolean hasSetModifiedDate() {
		return _setModifiedDate;
	}

	@Override
	public void setModifiedDate(Date modifiedDate) {
		_setModifiedDate = true;

		_modifiedDate = modifiedDate;
	}

	@Override
	public long getTaskId() {
		return _taskId;
	}

	@Override
	public void setTaskId(long taskId) {
		_columnBitmask |= TASKID_COLUMN_BITMASK;

		if (!_setOriginalTaskId) {
			_setOriginalTaskId = true;

			_originalTaskId = _taskId;
		}

		_taskId = taskId;
	}

	public long getOriginalTaskId() {
		return _originalTaskId;
	}

	@Override
	public double getHours() {
		return _hours;
	}

	@Override
	public void setHours(double hours) {
		_hours = hours;
	}

	@Override
	public Date getStartDate() {
		return _startDate;
	}

	@Override
	public void setStartDate(Date startDate) {
		_columnBitmask = -1L;

		_startDate = startDate;
	}

	@Override
	public String getComment() {
		if (_comment == null) {
			return "";
		}
		else {
			return _comment;
		}
	}

	@Override
	public void setComment(String comment) {
		_comment = comment;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return new StagedModelType(
			PortalUtil.getClassNameId(WorkingHours.class.getName()));
	}

	public long getColumnBitmask() {
		return _columnBitmask;
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return ExpandoBridgeFactoryUtil.getExpandoBridge(
			getCompanyId(), WorkingHours.class.getName(), getPrimaryKey());
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		ExpandoBridge expandoBridge = getExpandoBridge();

		expandoBridge.setAttributes(serviceContext);
	}

	@Override
	public WorkingHours toEscapedModel() {
		if (_escapedModel == null) {
			_escapedModel = _escapedModelProxyProviderFunction.apply(
				new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		WorkingHoursImpl workingHoursImpl = new WorkingHoursImpl();

		workingHoursImpl.setUuid(getUuid());
		workingHoursImpl.setWorkingHoursId(getWorkingHoursId());
		workingHoursImpl.setCompanyId(getCompanyId());
		workingHoursImpl.setGroupId(getGroupId());
		workingHoursImpl.setUserGroupId(getUserGroupId());
		workingHoursImpl.setUserId(getUserId());
		workingHoursImpl.setCreateDate(getCreateDate());
		workingHoursImpl.setModifiedDate(getModifiedDate());
		workingHoursImpl.setTaskId(getTaskId());
		workingHoursImpl.setHours(getHours());
		workingHoursImpl.setStartDate(getStartDate());
		workingHoursImpl.setComment(getComment());

		workingHoursImpl.resetOriginalValues();

		return workingHoursImpl;
	}

	@Override
	public int compareTo(WorkingHours workingHours) {
		int value = 0;

		value = DateUtil.compareTo(getStartDate(), workingHours.getStartDate());

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof WorkingHours)) {
			return false;
		}

		WorkingHours workingHours = (WorkingHours)obj;

		long primaryKey = workingHours.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _entityCacheEnabled;
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _finderCacheEnabled;
	}

	@Override
	public void resetOriginalValues() {
		WorkingHoursModelImpl workingHoursModelImpl = this;

		workingHoursModelImpl._originalUuid = workingHoursModelImpl._uuid;

		workingHoursModelImpl._originalCompanyId =
			workingHoursModelImpl._companyId;

		workingHoursModelImpl._setOriginalCompanyId = false;

		workingHoursModelImpl._originalGroupId = workingHoursModelImpl._groupId;

		workingHoursModelImpl._setOriginalGroupId = false;

		workingHoursModelImpl._originalUserGroupId =
			workingHoursModelImpl._userGroupId;

		workingHoursModelImpl._setOriginalUserGroupId = false;

		workingHoursModelImpl._originalUserId = workingHoursModelImpl._userId;

		workingHoursModelImpl._setOriginalUserId = false;

		workingHoursModelImpl._setModifiedDate = false;

		workingHoursModelImpl._originalTaskId = workingHoursModelImpl._taskId;

		workingHoursModelImpl._setOriginalTaskId = false;

		workingHoursModelImpl._columnBitmask = 0;
	}

	@Override
	public CacheModel<WorkingHours> toCacheModel() {
		WorkingHoursCacheModel workingHoursCacheModel =
			new WorkingHoursCacheModel();

		workingHoursCacheModel.uuid = getUuid();

		String uuid = workingHoursCacheModel.uuid;

		if ((uuid != null) && (uuid.length() == 0)) {
			workingHoursCacheModel.uuid = null;
		}

		workingHoursCacheModel.workingHoursId = getWorkingHoursId();

		workingHoursCacheModel.companyId = getCompanyId();

		workingHoursCacheModel.groupId = getGroupId();

		workingHoursCacheModel.userGroupId = getUserGroupId();

		workingHoursCacheModel.userId = getUserId();

		Date createDate = getCreateDate();

		if (createDate != null) {
			workingHoursCacheModel.createDate = createDate.getTime();
		}
		else {
			workingHoursCacheModel.createDate = Long.MIN_VALUE;
		}

		Date modifiedDate = getModifiedDate();

		if (modifiedDate != null) {
			workingHoursCacheModel.modifiedDate = modifiedDate.getTime();
		}
		else {
			workingHoursCacheModel.modifiedDate = Long.MIN_VALUE;
		}

		workingHoursCacheModel.taskId = getTaskId();

		workingHoursCacheModel.hours = getHours();

		Date startDate = getStartDate();

		if (startDate != null) {
			workingHoursCacheModel.startDate = startDate.getTime();
		}
		else {
			workingHoursCacheModel.startDate = Long.MIN_VALUE;
		}

		workingHoursCacheModel.comment = getComment();

		String comment = workingHoursCacheModel.comment;

		if ((comment != null) && (comment.length() == 0)) {
			workingHoursCacheModel.comment = null;
		}

		return workingHoursCacheModel;
	}

	@Override
	public String toString() {
		Map<String, Function<WorkingHours, Object>> attributeGetterFunctions =
			getAttributeGetterFunctions();

		StringBundler sb = new StringBundler(
			4 * attributeGetterFunctions.size() + 2);

		sb.append("{");

		for (Map.Entry<String, Function<WorkingHours, Object>> entry :
				attributeGetterFunctions.entrySet()) {

			String attributeName = entry.getKey();
			Function<WorkingHours, Object> attributeGetterFunction =
				entry.getValue();

			sb.append(attributeName);
			sb.append("=");
			sb.append(attributeGetterFunction.apply((WorkingHours)this));
			sb.append(", ");
		}

		if (sb.index() > 1) {
			sb.setIndex(sb.index() - 1);
		}

		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		Map<String, Function<WorkingHours, Object>> attributeGetterFunctions =
			getAttributeGetterFunctions();

		StringBundler sb = new StringBundler(
			5 * attributeGetterFunctions.size() + 4);

		sb.append("<model><model-name>");
		sb.append(getModelClassName());
		sb.append("</model-name>");

		for (Map.Entry<String, Function<WorkingHours, Object>> entry :
				attributeGetterFunctions.entrySet()) {

			String attributeName = entry.getKey();
			Function<WorkingHours, Object> attributeGetterFunction =
				entry.getValue();

			sb.append("<column><column-name>");
			sb.append(attributeName);
			sb.append("</column-name><column-value><![CDATA[");
			sb.append(attributeGetterFunction.apply((WorkingHours)this));
			sb.append("]]></column-value></column>");
		}

		sb.append("</model>");

		return sb.toString();
	}

	private static final Function<InvocationHandler, WorkingHours>
		_escapedModelProxyProviderFunction = _getProxyProviderFunction();
	private static boolean _entityCacheEnabled;
	private static boolean _finderCacheEnabled;

	private String _uuid;
	private String _originalUuid;
	private long _workingHoursId;
	private long _companyId;
	private long _originalCompanyId;
	private boolean _setOriginalCompanyId;
	private long _groupId;
	private long _originalGroupId;
	private boolean _setOriginalGroupId;
	private long _userGroupId;
	private long _originalUserGroupId;
	private boolean _setOriginalUserGroupId;
	private long _userId;
	private long _originalUserId;
	private boolean _setOriginalUserId;
	private Date _createDate;
	private Date _modifiedDate;
	private boolean _setModifiedDate;
	private long _taskId;
	private long _originalTaskId;
	private boolean _setOriginalTaskId;
	private double _hours;
	private Date _startDate;
	private String _comment;
	private long _columnBitmask;
	private WorkingHours _escapedModel;

}