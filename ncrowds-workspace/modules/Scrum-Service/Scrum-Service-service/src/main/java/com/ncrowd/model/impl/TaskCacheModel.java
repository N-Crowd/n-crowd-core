/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.ncrowd.model.Task;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The cache model class for representing Task in entity cache.
 *
 * @author Sudhanshu
 * @generated
 */
@ProviderType
public class TaskCacheModel implements CacheModel<Task>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TaskCacheModel)) {
			return false;
		}

		TaskCacheModel taskCacheModel = (TaskCacheModel)obj;

		if (taskId == taskCacheModel.taskId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, taskId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(39);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", taskId=");
		sb.append(taskId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", userGroupId=");
		sb.append(userGroupId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", sprintId=");
		sb.append(sprintId);
		sb.append(", statusId=");
		sb.append(statusId);
		sb.append(", description=");
		sb.append(description);
		sb.append(", text=");
		sb.append(text);
		sb.append(", assignedTo=");
		sb.append(assignedTo);
		sb.append(", evaluation=");
		sb.append(evaluation);
		sb.append(", sortIndex=");
		sb.append(sortIndex);
		sb.append(", individualColor=");
		sb.append(individualColor);
		sb.append(", fixedSince=");
		sb.append(fixedSince);
		sb.append(", taskGroupId=");
		sb.append(taskGroupId);
		sb.append(", taskGroup=");
		sb.append(taskGroup);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Task toEntityModel() {
		TaskImpl taskImpl = new TaskImpl();

		if (uuid == null) {
			taskImpl.setUuid("");
		}
		else {
			taskImpl.setUuid(uuid);
		}

		taskImpl.setTaskId(taskId);
		taskImpl.setCompanyId(companyId);
		taskImpl.setGroupId(groupId);
		taskImpl.setUserGroupId(userGroupId);
		taskImpl.setUserId(userId);

		if (createDate == Long.MIN_VALUE) {
			taskImpl.setCreateDate(null);
		}
		else {
			taskImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			taskImpl.setModifiedDate(null);
		}
		else {
			taskImpl.setModifiedDate(new Date(modifiedDate));
		}

		taskImpl.setSprintId(sprintId);
		taskImpl.setStatusId(statusId);

		if (description == null) {
			taskImpl.setDescription("");
		}
		else {
			taskImpl.setDescription(description);
		}

		if (text == null) {
			taskImpl.setText("");
		}
		else {
			taskImpl.setText(text);
		}

		taskImpl.setAssignedTo(assignedTo);
		taskImpl.setEvaluation(evaluation);
		taskImpl.setSortIndex(sortIndex);

		if (individualColor == null) {
			taskImpl.setIndividualColor("");
		}
		else {
			taskImpl.setIndividualColor(individualColor);
		}

		if (fixedSince == Long.MIN_VALUE) {
			taskImpl.setFixedSince(null);
		}
		else {
			taskImpl.setFixedSince(new Date(fixedSince));
		}

		taskImpl.setTaskGroupId(taskGroupId);
		taskImpl.setTaskGroup(taskGroup);

		taskImpl.resetOriginalValues();

		return taskImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		taskId = objectInput.readLong();

		companyId = objectInput.readLong();

		groupId = objectInput.readLong();

		userGroupId = objectInput.readLong();

		userId = objectInput.readLong();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		sprintId = objectInput.readLong();

		statusId = objectInput.readLong();
		description = objectInput.readUTF();
		text = objectInput.readUTF();

		assignedTo = objectInput.readLong();

		evaluation = objectInput.readDouble();

		sortIndex = objectInput.readInt();
		individualColor = objectInput.readUTF();
		fixedSince = objectInput.readLong();

		taskGroupId = objectInput.readLong();

		taskGroup = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(taskId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(userGroupId);

		objectOutput.writeLong(userId);
		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(sprintId);

		objectOutput.writeLong(statusId);

		if (description == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(description);
		}

		if (text == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(text);
		}

		objectOutput.writeLong(assignedTo);

		objectOutput.writeDouble(evaluation);

		objectOutput.writeInt(sortIndex);

		if (individualColor == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(individualColor);
		}

		objectOutput.writeLong(fixedSince);

		objectOutput.writeLong(taskGroupId);

		objectOutput.writeBoolean(taskGroup);
	}

	public String uuid;
	public long taskId;
	public long companyId;
	public long groupId;
	public long userGroupId;
	public long userId;
	public long createDate;
	public long modifiedDate;
	public long sprintId;
	public long statusId;
	public String description;
	public String text;
	public long assignedTo;
	public double evaluation;
	public int sortIndex;
	public String individualColor;
	public long fixedSince;
	public long taskGroupId;
	public boolean taskGroup;

}