/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.util.Validator;
import com.ncrowd.model.WorkingHours;
import com.ncrowd.service.base.WorkingHoursLocalServiceBaseImpl;

import java.util.List;

import org.osgi.service.component.annotations.Component;

/**
 * The implementation of the working hours local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>com.ncrowd.service.WorkingHoursLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Sudhanshu
 * @see WorkingHoursLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=com.ncrowd.model.WorkingHours",
	service = AopService.class
)
public class WorkingHoursLocalServiceImpl
	extends WorkingHoursLocalServiceBaseImpl {

	public List<WorkingHours> findByTaskId(final long taskId) {
        return workingHoursPersistence.findByTaskId(taskId);
    }
    
    public List<WorkingHours> findByUserGroupId(final long userGroupId) {
        return workingHoursPersistence.findByUserGroupId(userGroupId);
    }
    
    public List<WorkingHours> findByUserId(final long userId) {
        return workingHoursPersistence.findByUserId(userId);
    }
    
    public boolean saveWorkingHours(WorkingHours hours) {
    	WorkingHours workingHours=workingHoursLocalService.addWorkingHours(hours);
    	if(Validator.isNotNull(workingHours))
    		return true;
    	else
    		return false;
    }
    
}