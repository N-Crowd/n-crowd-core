create table NCrowd_DocumentMapping (
	uuid_ VARCHAR(75) null,
	mappingId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	userGroupId LONG,
	taskId LONG,
	dlFileEntryId LONG
);

create table NCrowd_Sprint (
	uuid_ VARCHAR(75) null,
	sprintId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	userGroupId LONG,
	startDate DATE null,
	endDate DATE null,
	sprintNumber INTEGER
);

create table NCrowd_Status (
	uuid_ VARCHAR(75) null,
	statusId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	userGroupId LONG,
	name VARCHAR(75) null,
	sortIndex INTEGER,
	fixedStatus BOOLEAN
);

create table NCrowd_Task (
	uuid_ VARCHAR(75) null,
	taskId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	userGroupId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	sprintId LONG,
	statusId LONG,
	description TEXT null,
	text_ TEXT null,
	assignedTo LONG,
	evaluation DOUBLE,
	sortIndex INTEGER,
	individualColor VARCHAR(75) null,
	fixedSince DATE null,
	taskGroupId LONG,
	taskGroup BOOLEAN
);

create table NCrowd_WorkingHours (
	uuid_ VARCHAR(75) null,
	workingHoursId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	userGroupId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	taskId LONG,
	hours DOUBLE,
	startDate DATE null,
	comment_ TEXT null
);

create table Scrum_DocumentMapping (
	uuid_ VARCHAR(75) null,
	mappingId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	userGroupId LONG,
	taskId LONG,
	dlFileEntryId LONG
);

create table Scrum_Sprint (
	uuid_ VARCHAR(75) null,
	sprintId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	userGroupId LONG,
	startDate DATE null,
	endDate DATE null,
	sprintNumber INTEGER
);

create table Scrum_Status (
	uuid_ VARCHAR(75) null,
	statusId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	userGroupId LONG,
	name VARCHAR(75) null,
	sortIndex INTEGER,
	fixedStatus BOOLEAN
);

create table Scrum_Task (
	uuid_ VARCHAR(75) null,
	taskId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	userGroupId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	sprintId LONG,
	statusId LONG,
	description TEXT null,
	text_ TEXT null,
	assignedTo LONG,
	evaluation DOUBLE,
	sortIndex INTEGER,
	individualColor VARCHAR(75) null,
	fixedSince DATE null,
	taskGroupId LONG,
	taskGroup BOOLEAN
);

create table Scrum_WorkingHours (
	uuid_ VARCHAR(75) null,
	workingHoursId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	userGroupId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	taskId LONG,
	hours DOUBLE,
	startDate DATE null,
	comment_ TEXT null
);