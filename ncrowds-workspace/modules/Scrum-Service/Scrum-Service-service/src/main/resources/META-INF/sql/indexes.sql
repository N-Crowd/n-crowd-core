create index IX_CA1F266C on NCrowd_DocumentMapping (taskId, dlFileEntryId);
create index IX_3A548C8D on NCrowd_DocumentMapping (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_A0C0D24F on NCrowd_DocumentMapping (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_B345835F on NCrowd_Sprint (userGroupId);
create index IX_CB90D4B4 on NCrowd_Sprint (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_84EEF836 on NCrowd_Sprint (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_EA8DBA90 on NCrowd_Status (companyId);
create index IX_6ED44A87 on NCrowd_Status (userGroupId);
create index IX_F168468C on NCrowd_Status (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_D129000E on NCrowd_Status (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_905BD12 on NCrowd_Task (userGroupId, description[$COLUMN_LENGTH:4001$], taskGroup);
create index IX_F961002C on NCrowd_Task (userGroupId, sprintId, statusId, taskGroup);
create index IX_3E72B6ED on NCrowd_Task (userGroupId, sprintId, taskGroup);
create index IX_2E32D416 on NCrowd_Task (userGroupId, taskGroup);
create index IX_588A7C9F on NCrowd_Task (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_11779EE1 on NCrowd_Task (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_D26B6AF8 on NCrowd_WorkingHours (taskId);
create index IX_E9DB7A9B on NCrowd_WorkingHours (userGroupId);
create index IX_DAFB8E3E on NCrowd_WorkingHours (userId);
create index IX_4D8002F8 on NCrowd_WorkingHours (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_BA9CE77A on NCrowd_WorkingHours (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_47C62FB1 on Scrum_DocumentMapping (taskId, dlFileEntryId);
create index IX_F2040C28 on Scrum_DocumentMapping (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_E1F17CAA on Scrum_DocumentMapping (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_FD369E3A on Scrum_Sprint (userGroupId);
create index IX_E3F54AB9 on Scrum_Sprint (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_35932B7B on Scrum_Sprint (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_1C2DD02B on Scrum_Status (companyId);
create index IX_B8C56562 on Scrum_Status (userGroupId);
create index IX_9CCBC91 on Scrum_Status (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_81CD3353 on Scrum_Status (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_5A3571AD on Scrum_Task (userGroupId, description[$COLUMN_LENGTH:4001$], taskGroup);
create index IX_FE34F731 on Scrum_Task (userGroupId, sprintId, statusId, taskGroup);
create index IX_BC19C032 on Scrum_Task (userGroupId, sprintId, taskGroup);
create index IX_F3B2BF9B on Scrum_Task (userGroupId, taskGroup);
create index IX_92EAFE4 on Scrum_Task (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_5A9DF66 on Scrum_Task (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_C69DAB7D on Scrum_WorkingHours (taskId);
create index IX_DE05C536 on Scrum_WorkingHours (userGroupId);
create index IX_CF2DCEC3 on Scrum_WorkingHours (userId);
create index IX_B2B3213D on Scrum_WorkingHours (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_801CD2FF on Scrum_WorkingHours (uuid_[$COLUMN_LENGTH:75$], groupId);