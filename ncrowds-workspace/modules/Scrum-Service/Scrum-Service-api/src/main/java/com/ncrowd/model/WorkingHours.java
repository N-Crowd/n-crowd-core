/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.model;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The extended model interface for the WorkingHours service. Represents a row in the &quot;Scrum_WorkingHours&quot; database table, with each column mapped to a property of this class.
 *
 * @author Sudhanshu
 * @see WorkingHoursModel
 * @generated
 */
@ImplementationClassName("com.ncrowd.model.impl.WorkingHoursImpl")
@ProviderType
public interface WorkingHours extends PersistedModel, WorkingHoursModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to <code>com.ncrowd.model.impl.WorkingHoursImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<WorkingHours, Long> WORKING_HOURS_ID_ACCESSOR =
		new Accessor<WorkingHours, Long>() {

			@Override
			public Long get(WorkingHours workingHours) {
				return workingHours.getWorkingHoursId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<WorkingHours> getTypeClass() {
				return WorkingHours.class;
			}

		};

	public String getEmployeeName();

}