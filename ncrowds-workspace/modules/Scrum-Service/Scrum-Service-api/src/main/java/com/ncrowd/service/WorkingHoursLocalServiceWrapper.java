/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides a wrapper for {@link WorkingHoursLocalService}.
 *
 * @author Sudhanshu
 * @see WorkingHoursLocalService
 * @generated
 */
@ProviderType
public class WorkingHoursLocalServiceWrapper
	implements WorkingHoursLocalService,
			   ServiceWrapper<WorkingHoursLocalService> {

	public WorkingHoursLocalServiceWrapper(
		WorkingHoursLocalService workingHoursLocalService) {

		_workingHoursLocalService = workingHoursLocalService;
	}

	/**
	 * Adds the working hours to the database. Also notifies the appropriate model listeners.
	 *
	 * @param workingHours the working hours
	 * @return the working hours that was added
	 */
	@Override
	public com.ncrowd.model.WorkingHours addWorkingHours(
		com.ncrowd.model.WorkingHours workingHours) {

		return _workingHoursLocalService.addWorkingHours(workingHours);
	}

	/**
	 * Creates a new working hours with the primary key. Does not add the working hours to the database.
	 *
	 * @param workingHoursId the primary key for the new working hours
	 * @return the new working hours
	 */
	@Override
	public com.ncrowd.model.WorkingHours createWorkingHours(
		long workingHoursId) {

		return _workingHoursLocalService.createWorkingHours(workingHoursId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _workingHoursLocalService.deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the working hours with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param workingHoursId the primary key of the working hours
	 * @return the working hours that was removed
	 * @throws PortalException if a working hours with the primary key could not be found
	 */
	@Override
	public com.ncrowd.model.WorkingHours deleteWorkingHours(long workingHoursId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _workingHoursLocalService.deleteWorkingHours(workingHoursId);
	}

	/**
	 * Deletes the working hours from the database. Also notifies the appropriate model listeners.
	 *
	 * @param workingHours the working hours
	 * @return the working hours that was removed
	 */
	@Override
	public com.ncrowd.model.WorkingHours deleteWorkingHours(
		com.ncrowd.model.WorkingHours workingHours) {

		return _workingHoursLocalService.deleteWorkingHours(workingHours);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _workingHoursLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _workingHoursLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.ncrowd.model.impl.WorkingHoursModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _workingHoursLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.ncrowd.model.impl.WorkingHoursModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _workingHoursLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _workingHoursLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _workingHoursLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public com.ncrowd.model.WorkingHours fetchWorkingHours(
		long workingHoursId) {

		return _workingHoursLocalService.fetchWorkingHours(workingHoursId);
	}

	/**
	 * Returns the working hours matching the UUID and group.
	 *
	 * @param uuid the working hours's UUID
	 * @param groupId the primary key of the group
	 * @return the matching working hours, or <code>null</code> if a matching working hours could not be found
	 */
	@Override
	public com.ncrowd.model.WorkingHours fetchWorkingHoursByUuidAndGroupId(
		String uuid, long groupId) {

		return _workingHoursLocalService.fetchWorkingHoursByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public java.util.List<com.ncrowd.model.WorkingHours> findByTaskId(
		long taskId) {

		return _workingHoursLocalService.findByTaskId(taskId);
	}

	@Override
	public java.util.List<com.ncrowd.model.WorkingHours> findByUserGroupId(
		long userGroupId) {

		return _workingHoursLocalService.findByUserGroupId(userGroupId);
	}

	@Override
	public java.util.List<com.ncrowd.model.WorkingHours> findByUserId(
		long userId) {

		return _workingHoursLocalService.findByUserId(userId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _workingHoursLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _workingHoursLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _workingHoursLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _workingHoursLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _workingHoursLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the working hours with the primary key.
	 *
	 * @param workingHoursId the primary key of the working hours
	 * @return the working hours
	 * @throws PortalException if a working hours with the primary key could not be found
	 */
	@Override
	public com.ncrowd.model.WorkingHours getWorkingHours(long workingHoursId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _workingHoursLocalService.getWorkingHours(workingHoursId);
	}

	/**
	 * Returns the working hours matching the UUID and group.
	 *
	 * @param uuid the working hours's UUID
	 * @param groupId the primary key of the group
	 * @return the matching working hours
	 * @throws PortalException if a matching working hours could not be found
	 */
	@Override
	public com.ncrowd.model.WorkingHours getWorkingHoursByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _workingHoursLocalService.getWorkingHoursByUuidAndGroupId(
			uuid, groupId);
	}

	/**
	 * Returns a range of all the working hourses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.ncrowd.model.impl.WorkingHoursModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of working hourses
	 * @param end the upper bound of the range of working hourses (not inclusive)
	 * @return the range of working hourses
	 */
	@Override
	public java.util.List<com.ncrowd.model.WorkingHours> getWorkingHourses(
		int start, int end) {

		return _workingHoursLocalService.getWorkingHourses(start, end);
	}

	/**
	 * Returns all the working hourses matching the UUID and company.
	 *
	 * @param uuid the UUID of the working hourses
	 * @param companyId the primary key of the company
	 * @return the matching working hourses, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<com.ncrowd.model.WorkingHours>
		getWorkingHoursesByUuidAndCompanyId(String uuid, long companyId) {

		return _workingHoursLocalService.getWorkingHoursesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of working hourses matching the UUID and company.
	 *
	 * @param uuid the UUID of the working hourses
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of working hourses
	 * @param end the upper bound of the range of working hourses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching working hourses, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<com.ncrowd.model.WorkingHours>
		getWorkingHoursesByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.ncrowd.model.WorkingHours> orderByComparator) {

		return _workingHoursLocalService.getWorkingHoursesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of working hourses.
	 *
	 * @return the number of working hourses
	 */
	@Override
	public int getWorkingHoursesCount() {
		return _workingHoursLocalService.getWorkingHoursesCount();
	}

	@Override
	public boolean saveWorkingHours(com.ncrowd.model.WorkingHours hours) {
		return _workingHoursLocalService.saveWorkingHours(hours);
	}

	/**
	 * Updates the working hours in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param workingHours the working hours
	 * @return the working hours that was updated
	 */
	@Override
	public com.ncrowd.model.WorkingHours updateWorkingHours(
		com.ncrowd.model.WorkingHours workingHours) {

		return _workingHoursLocalService.updateWorkingHours(workingHours);
	}

	@Override
	public WorkingHoursLocalService getWrappedService() {
		return _workingHoursLocalService;
	}

	@Override
	public void setWrappedService(
		WorkingHoursLocalService workingHoursLocalService) {

		_workingHoursLocalService = workingHoursLocalService;
	}

	private WorkingHoursLocalService _workingHoursLocalService;

}