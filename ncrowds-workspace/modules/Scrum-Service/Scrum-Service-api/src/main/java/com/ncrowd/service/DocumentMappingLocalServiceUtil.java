/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.service;

import org.osgi.annotation.versioning.ProviderType;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for DocumentMapping. This utility wraps
 * <code>com.ncrowd.service.impl.DocumentMappingLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Sudhanshu
 * @see DocumentMappingLocalService
 * @generated
 */
@ProviderType
public class DocumentMappingLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.ncrowd.service.impl.DocumentMappingLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the document mapping to the database. Also notifies the appropriate model listeners.
	 *
	 * @param documentMapping the document mapping
	 * @return the document mapping that was added
	 */
	public static com.ncrowd.model.DocumentMapping addDocumentMapping(
		com.ncrowd.model.DocumentMapping documentMapping) {

		return getService().addDocumentMapping(documentMapping);
	}

	/**
	 * Creates a new document mapping with the primary key. Does not add the document mapping to the database.
	 *
	 * @param mappingId the primary key for the new document mapping
	 * @return the new document mapping
	 */
	public static com.ncrowd.model.DocumentMapping createDocumentMapping(
		long mappingId) {

		return getService().createDocumentMapping(mappingId);
	}

	/**
	 * Deletes the document mapping from the database. Also notifies the appropriate model listeners.
	 *
	 * @param documentMapping the document mapping
	 * @return the document mapping that was removed
	 */
	public static com.ncrowd.model.DocumentMapping deleteDocumentMapping(
		com.ncrowd.model.DocumentMapping documentMapping) {

		return getService().deleteDocumentMapping(documentMapping);
	}

	/**
	 * Deletes the document mapping with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param mappingId the primary key of the document mapping
	 * @return the document mapping that was removed
	 * @throws PortalException if a document mapping with the primary key could not be found
	 */
	public static com.ncrowd.model.DocumentMapping deleteDocumentMapping(
			long mappingId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deleteDocumentMapping(mappingId);
	}

	/**
	 * @throws PortalException
	 */
	public static com.liferay.portal.kernel.model.PersistedModel
			deletePersistedModel(
				com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery
		dynamicQuery() {

		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.ncrowd.model.impl.DocumentMappingModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.ncrowd.model.impl.DocumentMappingModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static com.ncrowd.model.DocumentMapping fetchDocumentMapping(
		long mappingId) {

		return getService().fetchDocumentMapping(mappingId);
	}

	/**
	 * Returns the document mapping matching the UUID and group.
	 *
	 * @param uuid the document mapping's UUID
	 * @param groupId the primary key of the group
	 * @return the matching document mapping, or <code>null</code> if a matching document mapping could not be found
	 */
	public static com.ncrowd.model.DocumentMapping
		fetchDocumentMappingByUuidAndGroupId(String uuid, long groupId) {

		return getService().fetchDocumentMappingByUuidAndGroupId(uuid, groupId);
	}

	public static java.util.List<com.ncrowd.model.DocumentMapping> findByT_D(
			long taskId, long dlFileEntryId)
		throws com.liferay.portal.kernel.exception.SystemException {

		return getService().findByT_D(taskId, dlFileEntryId);
	}

	public static java.util.List<com.ncrowd.model.DocumentMapping> findByTaskId(
			long taskId)
		throws com.liferay.portal.kernel.exception.SystemException {

		return getService().findByTaskId(taskId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	/**
	 * Returns the document mapping with the primary key.
	 *
	 * @param mappingId the primary key of the document mapping
	 * @return the document mapping
	 * @throws PortalException if a document mapping with the primary key could not be found
	 */
	public static com.ncrowd.model.DocumentMapping getDocumentMapping(
			long mappingId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getDocumentMapping(mappingId);
	}

	/**
	 * Returns the document mapping matching the UUID and group.
	 *
	 * @param uuid the document mapping's UUID
	 * @param groupId the primary key of the group
	 * @return the matching document mapping
	 * @throws PortalException if a matching document mapping could not be found
	 */
	public static com.ncrowd.model.DocumentMapping
			getDocumentMappingByUuidAndGroupId(String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getDocumentMappingByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the document mappings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.ncrowd.model.impl.DocumentMappingModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of document mappings
	 * @param end the upper bound of the range of document mappings (not inclusive)
	 * @return the range of document mappings
	 */
	public static java.util.List<com.ncrowd.model.DocumentMapping>
		getDocumentMappings(int start, int end) {

		return getService().getDocumentMappings(start, end);
	}

	/**
	 * Returns all the document mappings matching the UUID and company.
	 *
	 * @param uuid the UUID of the document mappings
	 * @param companyId the primary key of the company
	 * @return the matching document mappings, or an empty list if no matches were found
	 */
	public static java.util.List<com.ncrowd.model.DocumentMapping>
		getDocumentMappingsByUuidAndCompanyId(String uuid, long companyId) {

		return getService().getDocumentMappingsByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of document mappings matching the UUID and company.
	 *
	 * @param uuid the UUID of the document mappings
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of document mappings
	 * @param end the upper bound of the range of document mappings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching document mappings, or an empty list if no matches were found
	 */
	public static java.util.List<com.ncrowd.model.DocumentMapping>
		getDocumentMappingsByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.ncrowd.model.DocumentMapping> orderByComparator) {

		return getService().getDocumentMappingsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of document mappings.
	 *
	 * @return the number of document mappings
	 */
	public static int getDocumentMappingsCount() {
		return getService().getDocumentMappingsCount();
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static com.liferay.portal.kernel.model.PersistedModel
			getPersistedModel(java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the document mapping in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param documentMapping the document mapping
	 * @return the document mapping that was updated
	 */
	public static com.ncrowd.model.DocumentMapping updateDocumentMapping(
		com.ncrowd.model.DocumentMapping documentMapping) {

		return getService().updateDocumentMapping(documentMapping);
	}

	public static DocumentMappingLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker
		<DocumentMappingLocalService, DocumentMappingLocalService>
			_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(
			DocumentMappingLocalService.class);

		ServiceTracker<DocumentMappingLocalService, DocumentMappingLocalService>
			serviceTracker =
				new ServiceTracker
					<DocumentMappingLocalService, DocumentMappingLocalService>(
						bundle.getBundleContext(),
						DocumentMappingLocalService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}