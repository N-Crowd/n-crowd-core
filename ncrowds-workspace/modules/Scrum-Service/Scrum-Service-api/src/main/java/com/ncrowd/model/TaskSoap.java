/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

/**
 * This class is used by SOAP remote services.
 *
 * @author Sudhanshu
 * @generated
 */
@ProviderType
public class TaskSoap implements Serializable {

	public static TaskSoap toSoapModel(Task model) {
		TaskSoap soapModel = new TaskSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setTaskId(model.getTaskId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setUserGroupId(model.getUserGroupId());
		soapModel.setUserId(model.getUserId());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setSprintId(model.getSprintId());
		soapModel.setStatusId(model.getStatusId());
		soapModel.setDescription(model.getDescription());
		soapModel.setText(model.getText());
		soapModel.setAssignedTo(model.getAssignedTo());
		soapModel.setEvaluation(model.getEvaluation());
		soapModel.setSortIndex(model.getSortIndex());
		soapModel.setIndividualColor(model.getIndividualColor());
		soapModel.setFixedSince(model.getFixedSince());
		soapModel.setTaskGroupId(model.getTaskGroupId());
		soapModel.setTaskGroup(model.isTaskGroup());

		return soapModel;
	}

	public static TaskSoap[] toSoapModels(Task[] models) {
		TaskSoap[] soapModels = new TaskSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static TaskSoap[][] toSoapModels(Task[][] models) {
		TaskSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new TaskSoap[models.length][models[0].length];
		}
		else {
			soapModels = new TaskSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static TaskSoap[] toSoapModels(List<Task> models) {
		List<TaskSoap> soapModels = new ArrayList<TaskSoap>(models.size());

		for (Task model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new TaskSoap[soapModels.size()]);
	}

	public TaskSoap() {
	}

	public long getPrimaryKey() {
		return _taskId;
	}

	public void setPrimaryKey(long pk) {
		setTaskId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getTaskId() {
		return _taskId;
	}

	public void setTaskId(long taskId) {
		_taskId = taskId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getUserGroupId() {
		return _userGroupId;
	}

	public void setUserGroupId(long userGroupId) {
		_userGroupId = userGroupId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getSprintId() {
		return _sprintId;
	}

	public void setSprintId(long sprintId) {
		_sprintId = sprintId;
	}

	public long getStatusId() {
		return _statusId;
	}

	public void setStatusId(long statusId) {
		_statusId = statusId;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public String getText() {
		return _text;
	}

	public void setText(String text) {
		_text = text;
	}

	public long getAssignedTo() {
		return _assignedTo;
	}

	public void setAssignedTo(long assignedTo) {
		_assignedTo = assignedTo;
	}

	public double getEvaluation() {
		return _evaluation;
	}

	public void setEvaluation(double evaluation) {
		_evaluation = evaluation;
	}

	public int getSortIndex() {
		return _sortIndex;
	}

	public void setSortIndex(int sortIndex) {
		_sortIndex = sortIndex;
	}

	public String getIndividualColor() {
		return _individualColor;
	}

	public void setIndividualColor(String individualColor) {
		_individualColor = individualColor;
	}

	public Date getFixedSince() {
		return _fixedSince;
	}

	public void setFixedSince(Date fixedSince) {
		_fixedSince = fixedSince;
	}

	public long getTaskGroupId() {
		return _taskGroupId;
	}

	public void setTaskGroupId(long taskGroupId) {
		_taskGroupId = taskGroupId;
	}

	public boolean getTaskGroup() {
		return _taskGroup;
	}

	public boolean isTaskGroup() {
		return _taskGroup;
	}

	public void setTaskGroup(boolean taskGroup) {
		_taskGroup = taskGroup;
	}

	private String _uuid;
	private long _taskId;
	private long _companyId;
	private long _groupId;
	private long _userGroupId;
	private long _userId;
	private Date _createDate;
	private Date _modifiedDate;
	private long _sprintId;
	private long _statusId;
	private String _description;
	private String _text;
	private long _assignedTo;
	private double _evaluation;
	private int _sortIndex;
	private String _individualColor;
	private Date _fixedSince;
	private long _taskGroupId;
	private boolean _taskGroup;

}