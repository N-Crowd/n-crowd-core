/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.model;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.HashMap;
import java.util.Map;

import org.osgi.annotation.versioning.ProviderType;

/**
 * <p>
 * This class is a wrapper for {@link Status}.
 * </p>
 *
 * @author Sudhanshu
 * @see Status
 * @generated
 */
@ProviderType
public class StatusWrapper
	extends BaseModelWrapper<Status> implements Status, ModelWrapper<Status> {

	public StatusWrapper(Status status) {
		super(status);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("statusId", getStatusId());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("userGroupId", getUserGroupId());
		attributes.put("name", getName());
		attributes.put("sortIndex", getSortIndex());
		attributes.put("fixedStatus", isFixedStatus());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long statusId = (Long)attributes.get("statusId");

		if (statusId != null) {
			setStatusId(statusId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long userGroupId = (Long)attributes.get("userGroupId");

		if (userGroupId != null) {
			setUserGroupId(userGroupId);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		Integer sortIndex = (Integer)attributes.get("sortIndex");

		if (sortIndex != null) {
			setSortIndex(sortIndex);
		}

		Boolean fixedStatus = (Boolean)attributes.get("fixedStatus");

		if (fixedStatus != null) {
			setFixedStatus(fixedStatus);
		}
	}

	@Override
	public int compareTo(Status status) {
		return model.compareTo(status);
	}

	/**
	 * Returns the company ID of this status.
	 *
	 * @return the company ID of this status
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the fixed status of this status.
	 *
	 * @return the fixed status of this status
	 */
	@Override
	public boolean getFixedStatus() {
		return model.getFixedStatus();
	}

	@Override
	public String getFormattedName() {
		return model.getFormattedName();
	}

	/**
	 * Returns the group ID of this status.
	 *
	 * @return the group ID of this status
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the name of this status.
	 *
	 * @return the name of this status
	 */
	@Override
	public String getName() {
		return model.getName();
	}

	/**
	 * Returns the primary key of this status.
	 *
	 * @return the primary key of this status
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the sort index of this status.
	 *
	 * @return the sort index of this status
	 */
	@Override
	public int getSortIndex() {
		return model.getSortIndex();
	}

	/**
	 * Returns the status ID of this status.
	 *
	 * @return the status ID of this status
	 */
	@Override
	public long getStatusId() {
		return model.getStatusId();
	}

	/**
	 * Returns the user group ID of this status.
	 *
	 * @return the user group ID of this status
	 */
	@Override
	public long getUserGroupId() {
		return model.getUserGroupId();
	}

	/**
	 * Returns the uuid of this status.
	 *
	 * @return the uuid of this status
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	/**
	 * Returns <code>true</code> if this status is fixed status.
	 *
	 * @return <code>true</code> if this status is fixed status; <code>false</code> otherwise
	 */
	@Override
	public boolean isFixedStatus() {
		return model.isFixedStatus();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the company ID of this status.
	 *
	 * @param companyId the company ID of this status
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets whether this status is fixed status.
	 *
	 * @param fixedStatus the fixed status of this status
	 */
	@Override
	public void setFixedStatus(boolean fixedStatus) {
		model.setFixedStatus(fixedStatus);
	}

	/**
	 * Sets the group ID of this status.
	 *
	 * @param groupId the group ID of this status
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the name of this status.
	 *
	 * @param name the name of this status
	 */
	@Override
	public void setName(String name) {
		model.setName(name);
	}

	/**
	 * Sets the primary key of this status.
	 *
	 * @param primaryKey the primary key of this status
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the sort index of this status.
	 *
	 * @param sortIndex the sort index of this status
	 */
	@Override
	public void setSortIndex(int sortIndex) {
		model.setSortIndex(sortIndex);
	}

	/**
	 * Sets the status ID of this status.
	 *
	 * @param statusId the status ID of this status
	 */
	@Override
	public void setStatusId(long statusId) {
		model.setStatusId(statusId);
	}

	/**
	 * Sets the user group ID of this status.
	 *
	 * @param userGroupId the user group ID of this status
	 */
	@Override
	public void setUserGroupId(long userGroupId) {
		model.setUserGroupId(userGroupId);
	}

	/**
	 * Sets the uuid of this status.
	 *
	 * @param uuid the uuid of this status
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	protected StatusWrapper wrap(Status status) {
		return new StatusWrapper(status);
	}

}