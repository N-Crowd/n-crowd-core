/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

/**
 * This class is used by SOAP remote services.
 *
 * @author Sudhanshu
 * @generated
 */
@ProviderType
public class StatusSoap implements Serializable {

	public static StatusSoap toSoapModel(Status model) {
		StatusSoap soapModel = new StatusSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setStatusId(model.getStatusId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setUserGroupId(model.getUserGroupId());
		soapModel.setName(model.getName());
		soapModel.setSortIndex(model.getSortIndex());
		soapModel.setFixedStatus(model.isFixedStatus());

		return soapModel;
	}

	public static StatusSoap[] toSoapModels(Status[] models) {
		StatusSoap[] soapModels = new StatusSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static StatusSoap[][] toSoapModels(Status[][] models) {
		StatusSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new StatusSoap[models.length][models[0].length];
		}
		else {
			soapModels = new StatusSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static StatusSoap[] toSoapModels(List<Status> models) {
		List<StatusSoap> soapModels = new ArrayList<StatusSoap>(models.size());

		for (Status model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new StatusSoap[soapModels.size()]);
	}

	public StatusSoap() {
	}

	public long getPrimaryKey() {
		return _statusId;
	}

	public void setPrimaryKey(long pk) {
		setStatusId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getStatusId() {
		return _statusId;
	}

	public void setStatusId(long statusId) {
		_statusId = statusId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getUserGroupId() {
		return _userGroupId;
	}

	public void setUserGroupId(long userGroupId) {
		_userGroupId = userGroupId;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public int getSortIndex() {
		return _sortIndex;
	}

	public void setSortIndex(int sortIndex) {
		_sortIndex = sortIndex;
	}

	public boolean getFixedStatus() {
		return _fixedStatus;
	}

	public boolean isFixedStatus() {
		return _fixedStatus;
	}

	public void setFixedStatus(boolean fixedStatus) {
		_fixedStatus = fixedStatus;
	}

	private String _uuid;
	private long _statusId;
	private long _companyId;
	private long _groupId;
	private long _userGroupId;
	private String _name;
	private int _sortIndex;
	private boolean _fixedStatus;

}