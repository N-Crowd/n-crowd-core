/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides a wrapper for {@link DocumentMappingLocalService}.
 *
 * @author Sudhanshu
 * @see DocumentMappingLocalService
 * @generated
 */
@ProviderType
public class DocumentMappingLocalServiceWrapper
	implements DocumentMappingLocalService,
			   ServiceWrapper<DocumentMappingLocalService> {

	public DocumentMappingLocalServiceWrapper(
		DocumentMappingLocalService documentMappingLocalService) {

		_documentMappingLocalService = documentMappingLocalService;
	}

	/**
	 * Adds the document mapping to the database. Also notifies the appropriate model listeners.
	 *
	 * @param documentMapping the document mapping
	 * @return the document mapping that was added
	 */
	@Override
	public com.ncrowd.model.DocumentMapping addDocumentMapping(
		com.ncrowd.model.DocumentMapping documentMapping) {

		return _documentMappingLocalService.addDocumentMapping(documentMapping);
	}

	/**
	 * Creates a new document mapping with the primary key. Does not add the document mapping to the database.
	 *
	 * @param mappingId the primary key for the new document mapping
	 * @return the new document mapping
	 */
	@Override
	public com.ncrowd.model.DocumentMapping createDocumentMapping(
		long mappingId) {

		return _documentMappingLocalService.createDocumentMapping(mappingId);
	}

	/**
	 * Deletes the document mapping from the database. Also notifies the appropriate model listeners.
	 *
	 * @param documentMapping the document mapping
	 * @return the document mapping that was removed
	 */
	@Override
	public com.ncrowd.model.DocumentMapping deleteDocumentMapping(
		com.ncrowd.model.DocumentMapping documentMapping) {

		return _documentMappingLocalService.deleteDocumentMapping(
			documentMapping);
	}

	/**
	 * Deletes the document mapping with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param mappingId the primary key of the document mapping
	 * @return the document mapping that was removed
	 * @throws PortalException if a document mapping with the primary key could not be found
	 */
	@Override
	public com.ncrowd.model.DocumentMapping deleteDocumentMapping(
			long mappingId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _documentMappingLocalService.deleteDocumentMapping(mappingId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _documentMappingLocalService.deletePersistedModel(
			persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _documentMappingLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _documentMappingLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.ncrowd.model.impl.DocumentMappingModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _documentMappingLocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.ncrowd.model.impl.DocumentMappingModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _documentMappingLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _documentMappingLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _documentMappingLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public com.ncrowd.model.DocumentMapping fetchDocumentMapping(
		long mappingId) {

		return _documentMappingLocalService.fetchDocumentMapping(mappingId);
	}

	/**
	 * Returns the document mapping matching the UUID and group.
	 *
	 * @param uuid the document mapping's UUID
	 * @param groupId the primary key of the group
	 * @return the matching document mapping, or <code>null</code> if a matching document mapping could not be found
	 */
	@Override
	public com.ncrowd.model.DocumentMapping
		fetchDocumentMappingByUuidAndGroupId(String uuid, long groupId) {

		return _documentMappingLocalService.
			fetchDocumentMappingByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public java.util.List<com.ncrowd.model.DocumentMapping> findByT_D(
			long taskId, long dlFileEntryId)
		throws com.liferay.portal.kernel.exception.SystemException {

		return _documentMappingLocalService.findByT_D(taskId, dlFileEntryId);
	}

	@Override
	public java.util.List<com.ncrowd.model.DocumentMapping> findByTaskId(
			long taskId)
		throws com.liferay.portal.kernel.exception.SystemException {

		return _documentMappingLocalService.findByTaskId(taskId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _documentMappingLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns the document mapping with the primary key.
	 *
	 * @param mappingId the primary key of the document mapping
	 * @return the document mapping
	 * @throws PortalException if a document mapping with the primary key could not be found
	 */
	@Override
	public com.ncrowd.model.DocumentMapping getDocumentMapping(long mappingId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _documentMappingLocalService.getDocumentMapping(mappingId);
	}

	/**
	 * Returns the document mapping matching the UUID and group.
	 *
	 * @param uuid the document mapping's UUID
	 * @param groupId the primary key of the group
	 * @return the matching document mapping
	 * @throws PortalException if a matching document mapping could not be found
	 */
	@Override
	public com.ncrowd.model.DocumentMapping getDocumentMappingByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _documentMappingLocalService.getDocumentMappingByUuidAndGroupId(
			uuid, groupId);
	}

	/**
	 * Returns a range of all the document mappings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.ncrowd.model.impl.DocumentMappingModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of document mappings
	 * @param end the upper bound of the range of document mappings (not inclusive)
	 * @return the range of document mappings
	 */
	@Override
	public java.util.List<com.ncrowd.model.DocumentMapping> getDocumentMappings(
		int start, int end) {

		return _documentMappingLocalService.getDocumentMappings(start, end);
	}

	/**
	 * Returns all the document mappings matching the UUID and company.
	 *
	 * @param uuid the UUID of the document mappings
	 * @param companyId the primary key of the company
	 * @return the matching document mappings, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<com.ncrowd.model.DocumentMapping>
		getDocumentMappingsByUuidAndCompanyId(String uuid, long companyId) {

		return _documentMappingLocalService.
			getDocumentMappingsByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of document mappings matching the UUID and company.
	 *
	 * @param uuid the UUID of the document mappings
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of document mappings
	 * @param end the upper bound of the range of document mappings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching document mappings, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<com.ncrowd.model.DocumentMapping>
		getDocumentMappingsByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.ncrowd.model.DocumentMapping> orderByComparator) {

		return _documentMappingLocalService.
			getDocumentMappingsByUuidAndCompanyId(
				uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of document mappings.
	 *
	 * @return the number of document mappings
	 */
	@Override
	public int getDocumentMappingsCount() {
		return _documentMappingLocalService.getDocumentMappingsCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _documentMappingLocalService.
			getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _documentMappingLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _documentMappingLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the document mapping in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param documentMapping the document mapping
	 * @return the document mapping that was updated
	 */
	@Override
	public com.ncrowd.model.DocumentMapping updateDocumentMapping(
		com.ncrowd.model.DocumentMapping documentMapping) {

		return _documentMappingLocalService.updateDocumentMapping(
			documentMapping);
	}

	@Override
	public DocumentMappingLocalService getWrappedService() {
		return _documentMappingLocalService;
	}

	@Override
	public void setWrappedService(
		DocumentMappingLocalService documentMappingLocalService) {

		_documentMappingLocalService = documentMappingLocalService;
	}

	private DocumentMappingLocalService _documentMappingLocalService;

}