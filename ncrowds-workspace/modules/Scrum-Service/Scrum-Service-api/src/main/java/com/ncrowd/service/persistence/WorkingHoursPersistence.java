/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.ncrowd.exception.NoSuchWorkingHoursException;
import com.ncrowd.model.WorkingHours;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the working hours service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Sudhanshu
 * @see WorkingHoursUtil
 * @generated
 */
@ProviderType
public interface WorkingHoursPersistence extends BasePersistence<WorkingHours> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link WorkingHoursUtil} to access the working hours persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the working hourses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching working hourses
	 */
	public java.util.List<WorkingHours> findByUuid(String uuid);

	/**
	 * Returns a range of all the working hourses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>WorkingHoursModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of working hourses
	 * @param end the upper bound of the range of working hourses (not inclusive)
	 * @return the range of matching working hourses
	 */
	public java.util.List<WorkingHours> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the working hourses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>WorkingHoursModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of working hourses
	 * @param end the upper bound of the range of working hourses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching working hourses
	 */
	public java.util.List<WorkingHours> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
			orderByComparator);

	/**
	 * Returns an ordered range of all the working hourses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>WorkingHoursModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of working hourses
	 * @param end the upper bound of the range of working hourses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching working hourses
	 */
	public java.util.List<WorkingHours> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Returns the first working hours in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching working hours
	 * @throws NoSuchWorkingHoursException if a matching working hours could not be found
	 */
	public WorkingHours findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
				orderByComparator)
		throws NoSuchWorkingHoursException;

	/**
	 * Returns the first working hours in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching working hours, or <code>null</code> if a matching working hours could not be found
	 */
	public WorkingHours fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
			orderByComparator);

	/**
	 * Returns the last working hours in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching working hours
	 * @throws NoSuchWorkingHoursException if a matching working hours could not be found
	 */
	public WorkingHours findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
				orderByComparator)
		throws NoSuchWorkingHoursException;

	/**
	 * Returns the last working hours in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching working hours, or <code>null</code> if a matching working hours could not be found
	 */
	public WorkingHours fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
			orderByComparator);

	/**
	 * Returns the working hourses before and after the current working hours in the ordered set where uuid = &#63;.
	 *
	 * @param workingHoursId the primary key of the current working hours
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next working hours
	 * @throws NoSuchWorkingHoursException if a working hours with the primary key could not be found
	 */
	public WorkingHours[] findByUuid_PrevAndNext(
			long workingHoursId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
				orderByComparator)
		throws NoSuchWorkingHoursException;

	/**
	 * Removes all the working hourses where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of working hourses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching working hourses
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the working hours where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchWorkingHoursException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching working hours
	 * @throws NoSuchWorkingHoursException if a matching working hours could not be found
	 */
	public WorkingHours findByUUID_G(String uuid, long groupId)
		throws NoSuchWorkingHoursException;

	/**
	 * Returns the working hours where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching working hours, or <code>null</code> if a matching working hours could not be found
	 */
	public WorkingHours fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the working hours where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching working hours, or <code>null</code> if a matching working hours could not be found
	 */
	public WorkingHours fetchByUUID_G(
		String uuid, long groupId, boolean retrieveFromCache);

	/**
	 * Removes the working hours where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the working hours that was removed
	 */
	public WorkingHours removeByUUID_G(String uuid, long groupId)
		throws NoSuchWorkingHoursException;

	/**
	 * Returns the number of working hourses where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching working hourses
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the working hourses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching working hourses
	 */
	public java.util.List<WorkingHours> findByUuid_C(
		String uuid, long companyId);

	/**
	 * Returns a range of all the working hourses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>WorkingHoursModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of working hourses
	 * @param end the upper bound of the range of working hourses (not inclusive)
	 * @return the range of matching working hourses
	 */
	public java.util.List<WorkingHours> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the working hourses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>WorkingHoursModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of working hourses
	 * @param end the upper bound of the range of working hourses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching working hourses
	 */
	public java.util.List<WorkingHours> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
			orderByComparator);

	/**
	 * Returns an ordered range of all the working hourses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>WorkingHoursModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of working hourses
	 * @param end the upper bound of the range of working hourses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching working hourses
	 */
	public java.util.List<WorkingHours> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Returns the first working hours in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching working hours
	 * @throws NoSuchWorkingHoursException if a matching working hours could not be found
	 */
	public WorkingHours findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
				orderByComparator)
		throws NoSuchWorkingHoursException;

	/**
	 * Returns the first working hours in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching working hours, or <code>null</code> if a matching working hours could not be found
	 */
	public WorkingHours fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
			orderByComparator);

	/**
	 * Returns the last working hours in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching working hours
	 * @throws NoSuchWorkingHoursException if a matching working hours could not be found
	 */
	public WorkingHours findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
				orderByComparator)
		throws NoSuchWorkingHoursException;

	/**
	 * Returns the last working hours in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching working hours, or <code>null</code> if a matching working hours could not be found
	 */
	public WorkingHours fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
			orderByComparator);

	/**
	 * Returns the working hourses before and after the current working hours in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param workingHoursId the primary key of the current working hours
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next working hours
	 * @throws NoSuchWorkingHoursException if a working hours with the primary key could not be found
	 */
	public WorkingHours[] findByUuid_C_PrevAndNext(
			long workingHoursId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
				orderByComparator)
		throws NoSuchWorkingHoursException;

	/**
	 * Removes all the working hourses where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of working hourses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching working hourses
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the working hourses where taskId = &#63;.
	 *
	 * @param taskId the task ID
	 * @return the matching working hourses
	 */
	public java.util.List<WorkingHours> findByTaskId(long taskId);

	/**
	 * Returns a range of all the working hourses where taskId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>WorkingHoursModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param taskId the task ID
	 * @param start the lower bound of the range of working hourses
	 * @param end the upper bound of the range of working hourses (not inclusive)
	 * @return the range of matching working hourses
	 */
	public java.util.List<WorkingHours> findByTaskId(
		long taskId, int start, int end);

	/**
	 * Returns an ordered range of all the working hourses where taskId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>WorkingHoursModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param taskId the task ID
	 * @param start the lower bound of the range of working hourses
	 * @param end the upper bound of the range of working hourses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching working hourses
	 */
	public java.util.List<WorkingHours> findByTaskId(
		long taskId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
			orderByComparator);

	/**
	 * Returns an ordered range of all the working hourses where taskId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>WorkingHoursModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param taskId the task ID
	 * @param start the lower bound of the range of working hourses
	 * @param end the upper bound of the range of working hourses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching working hourses
	 */
	public java.util.List<WorkingHours> findByTaskId(
		long taskId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Returns the first working hours in the ordered set where taskId = &#63;.
	 *
	 * @param taskId the task ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching working hours
	 * @throws NoSuchWorkingHoursException if a matching working hours could not be found
	 */
	public WorkingHours findByTaskId_First(
			long taskId,
			com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
				orderByComparator)
		throws NoSuchWorkingHoursException;

	/**
	 * Returns the first working hours in the ordered set where taskId = &#63;.
	 *
	 * @param taskId the task ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching working hours, or <code>null</code> if a matching working hours could not be found
	 */
	public WorkingHours fetchByTaskId_First(
		long taskId,
		com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
			orderByComparator);

	/**
	 * Returns the last working hours in the ordered set where taskId = &#63;.
	 *
	 * @param taskId the task ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching working hours
	 * @throws NoSuchWorkingHoursException if a matching working hours could not be found
	 */
	public WorkingHours findByTaskId_Last(
			long taskId,
			com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
				orderByComparator)
		throws NoSuchWorkingHoursException;

	/**
	 * Returns the last working hours in the ordered set where taskId = &#63;.
	 *
	 * @param taskId the task ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching working hours, or <code>null</code> if a matching working hours could not be found
	 */
	public WorkingHours fetchByTaskId_Last(
		long taskId,
		com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
			orderByComparator);

	/**
	 * Returns the working hourses before and after the current working hours in the ordered set where taskId = &#63;.
	 *
	 * @param workingHoursId the primary key of the current working hours
	 * @param taskId the task ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next working hours
	 * @throws NoSuchWorkingHoursException if a working hours with the primary key could not be found
	 */
	public WorkingHours[] findByTaskId_PrevAndNext(
			long workingHoursId, long taskId,
			com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
				orderByComparator)
		throws NoSuchWorkingHoursException;

	/**
	 * Removes all the working hourses where taskId = &#63; from the database.
	 *
	 * @param taskId the task ID
	 */
	public void removeByTaskId(long taskId);

	/**
	 * Returns the number of working hourses where taskId = &#63;.
	 *
	 * @param taskId the task ID
	 * @return the number of matching working hourses
	 */
	public int countByTaskId(long taskId);

	/**
	 * Returns all the working hourses where userGroupId = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @return the matching working hourses
	 */
	public java.util.List<WorkingHours> findByUserGroupId(long userGroupId);

	/**
	 * Returns a range of all the working hourses where userGroupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>WorkingHoursModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param start the lower bound of the range of working hourses
	 * @param end the upper bound of the range of working hourses (not inclusive)
	 * @return the range of matching working hourses
	 */
	public java.util.List<WorkingHours> findByUserGroupId(
		long userGroupId, int start, int end);

	/**
	 * Returns an ordered range of all the working hourses where userGroupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>WorkingHoursModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param start the lower bound of the range of working hourses
	 * @param end the upper bound of the range of working hourses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching working hourses
	 */
	public java.util.List<WorkingHours> findByUserGroupId(
		long userGroupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
			orderByComparator);

	/**
	 * Returns an ordered range of all the working hourses where userGroupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>WorkingHoursModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param start the lower bound of the range of working hourses
	 * @param end the upper bound of the range of working hourses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching working hourses
	 */
	public java.util.List<WorkingHours> findByUserGroupId(
		long userGroupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Returns the first working hours in the ordered set where userGroupId = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching working hours
	 * @throws NoSuchWorkingHoursException if a matching working hours could not be found
	 */
	public WorkingHours findByUserGroupId_First(
			long userGroupId,
			com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
				orderByComparator)
		throws NoSuchWorkingHoursException;

	/**
	 * Returns the first working hours in the ordered set where userGroupId = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching working hours, or <code>null</code> if a matching working hours could not be found
	 */
	public WorkingHours fetchByUserGroupId_First(
		long userGroupId,
		com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
			orderByComparator);

	/**
	 * Returns the last working hours in the ordered set where userGroupId = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching working hours
	 * @throws NoSuchWorkingHoursException if a matching working hours could not be found
	 */
	public WorkingHours findByUserGroupId_Last(
			long userGroupId,
			com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
				orderByComparator)
		throws NoSuchWorkingHoursException;

	/**
	 * Returns the last working hours in the ordered set where userGroupId = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching working hours, or <code>null</code> if a matching working hours could not be found
	 */
	public WorkingHours fetchByUserGroupId_Last(
		long userGroupId,
		com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
			orderByComparator);

	/**
	 * Returns the working hourses before and after the current working hours in the ordered set where userGroupId = &#63;.
	 *
	 * @param workingHoursId the primary key of the current working hours
	 * @param userGroupId the user group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next working hours
	 * @throws NoSuchWorkingHoursException if a working hours with the primary key could not be found
	 */
	public WorkingHours[] findByUserGroupId_PrevAndNext(
			long workingHoursId, long userGroupId,
			com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
				orderByComparator)
		throws NoSuchWorkingHoursException;

	/**
	 * Removes all the working hourses where userGroupId = &#63; from the database.
	 *
	 * @param userGroupId the user group ID
	 */
	public void removeByUserGroupId(long userGroupId);

	/**
	 * Returns the number of working hourses where userGroupId = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @return the number of matching working hourses
	 */
	public int countByUserGroupId(long userGroupId);

	/**
	 * Returns all the working hourses where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching working hourses
	 */
	public java.util.List<WorkingHours> findByUserId(long userId);

	/**
	 * Returns a range of all the working hourses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>WorkingHoursModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of working hourses
	 * @param end the upper bound of the range of working hourses (not inclusive)
	 * @return the range of matching working hourses
	 */
	public java.util.List<WorkingHours> findByUserId(
		long userId, int start, int end);

	/**
	 * Returns an ordered range of all the working hourses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>WorkingHoursModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of working hourses
	 * @param end the upper bound of the range of working hourses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching working hourses
	 */
	public java.util.List<WorkingHours> findByUserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
			orderByComparator);

	/**
	 * Returns an ordered range of all the working hourses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>WorkingHoursModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of working hourses
	 * @param end the upper bound of the range of working hourses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching working hourses
	 */
	public java.util.List<WorkingHours> findByUserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Returns the first working hours in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching working hours
	 * @throws NoSuchWorkingHoursException if a matching working hours could not be found
	 */
	public WorkingHours findByUserId_First(
			long userId,
			com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
				orderByComparator)
		throws NoSuchWorkingHoursException;

	/**
	 * Returns the first working hours in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching working hours, or <code>null</code> if a matching working hours could not be found
	 */
	public WorkingHours fetchByUserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
			orderByComparator);

	/**
	 * Returns the last working hours in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching working hours
	 * @throws NoSuchWorkingHoursException if a matching working hours could not be found
	 */
	public WorkingHours findByUserId_Last(
			long userId,
			com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
				orderByComparator)
		throws NoSuchWorkingHoursException;

	/**
	 * Returns the last working hours in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching working hours, or <code>null</code> if a matching working hours could not be found
	 */
	public WorkingHours fetchByUserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
			orderByComparator);

	/**
	 * Returns the working hourses before and after the current working hours in the ordered set where userId = &#63;.
	 *
	 * @param workingHoursId the primary key of the current working hours
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next working hours
	 * @throws NoSuchWorkingHoursException if a working hours with the primary key could not be found
	 */
	public WorkingHours[] findByUserId_PrevAndNext(
			long workingHoursId, long userId,
			com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
				orderByComparator)
		throws NoSuchWorkingHoursException;

	/**
	 * Removes all the working hourses where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 */
	public void removeByUserId(long userId);

	/**
	 * Returns the number of working hourses where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching working hourses
	 */
	public int countByUserId(long userId);

	/**
	 * Caches the working hours in the entity cache if it is enabled.
	 *
	 * @param workingHours the working hours
	 */
	public void cacheResult(WorkingHours workingHours);

	/**
	 * Caches the working hourses in the entity cache if it is enabled.
	 *
	 * @param workingHourses the working hourses
	 */
	public void cacheResult(java.util.List<WorkingHours> workingHourses);

	/**
	 * Creates a new working hours with the primary key. Does not add the working hours to the database.
	 *
	 * @param workingHoursId the primary key for the new working hours
	 * @return the new working hours
	 */
	public WorkingHours create(long workingHoursId);

	/**
	 * Removes the working hours with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param workingHoursId the primary key of the working hours
	 * @return the working hours that was removed
	 * @throws NoSuchWorkingHoursException if a working hours with the primary key could not be found
	 */
	public WorkingHours remove(long workingHoursId)
		throws NoSuchWorkingHoursException;

	public WorkingHours updateImpl(WorkingHours workingHours);

	/**
	 * Returns the working hours with the primary key or throws a <code>NoSuchWorkingHoursException</code> if it could not be found.
	 *
	 * @param workingHoursId the primary key of the working hours
	 * @return the working hours
	 * @throws NoSuchWorkingHoursException if a working hours with the primary key could not be found
	 */
	public WorkingHours findByPrimaryKey(long workingHoursId)
		throws NoSuchWorkingHoursException;

	/**
	 * Returns the working hours with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param workingHoursId the primary key of the working hours
	 * @return the working hours, or <code>null</code> if a working hours with the primary key could not be found
	 */
	public WorkingHours fetchByPrimaryKey(long workingHoursId);

	/**
	 * Returns all the working hourses.
	 *
	 * @return the working hourses
	 */
	public java.util.List<WorkingHours> findAll();

	/**
	 * Returns a range of all the working hourses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>WorkingHoursModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of working hourses
	 * @param end the upper bound of the range of working hourses (not inclusive)
	 * @return the range of working hourses
	 */
	public java.util.List<WorkingHours> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the working hourses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>WorkingHoursModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of working hourses
	 * @param end the upper bound of the range of working hourses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of working hourses
	 */
	public java.util.List<WorkingHours> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
			orderByComparator);

	/**
	 * Returns an ordered range of all the working hourses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>WorkingHoursModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of working hourses
	 * @param end the upper bound of the range of working hourses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of working hourses
	 */
	public java.util.List<WorkingHours> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<WorkingHours>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Removes all the working hourses from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of working hourses.
	 *
	 * @return the number of working hourses
	 */
	public int countAll();

}