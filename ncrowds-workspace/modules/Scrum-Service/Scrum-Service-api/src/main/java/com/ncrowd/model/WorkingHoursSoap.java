/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

/**
 * This class is used by SOAP remote services.
 *
 * @author Sudhanshu
 * @generated
 */
@ProviderType
public class WorkingHoursSoap implements Serializable {

	public static WorkingHoursSoap toSoapModel(WorkingHours model) {
		WorkingHoursSoap soapModel = new WorkingHoursSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setWorkingHoursId(model.getWorkingHoursId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setUserGroupId(model.getUserGroupId());
		soapModel.setUserId(model.getUserId());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setTaskId(model.getTaskId());
		soapModel.setHours(model.getHours());
		soapModel.setStartDate(model.getStartDate());
		soapModel.setComment(model.getComment());

		return soapModel;
	}

	public static WorkingHoursSoap[] toSoapModels(WorkingHours[] models) {
		WorkingHoursSoap[] soapModels = new WorkingHoursSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static WorkingHoursSoap[][] toSoapModels(WorkingHours[][] models) {
		WorkingHoursSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new WorkingHoursSoap[models.length][models[0].length];
		}
		else {
			soapModels = new WorkingHoursSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static WorkingHoursSoap[] toSoapModels(List<WorkingHours> models) {
		List<WorkingHoursSoap> soapModels = new ArrayList<WorkingHoursSoap>(
			models.size());

		for (WorkingHours model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new WorkingHoursSoap[soapModels.size()]);
	}

	public WorkingHoursSoap() {
	}

	public long getPrimaryKey() {
		return _workingHoursId;
	}

	public void setPrimaryKey(long pk) {
		setWorkingHoursId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getWorkingHoursId() {
		return _workingHoursId;
	}

	public void setWorkingHoursId(long workingHoursId) {
		_workingHoursId = workingHoursId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getUserGroupId() {
		return _userGroupId;
	}

	public void setUserGroupId(long userGroupId) {
		_userGroupId = userGroupId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getTaskId() {
		return _taskId;
	}

	public void setTaskId(long taskId) {
		_taskId = taskId;
	}

	public double getHours() {
		return _hours;
	}

	public void setHours(double hours) {
		_hours = hours;
	}

	public Date getStartDate() {
		return _startDate;
	}

	public void setStartDate(Date startDate) {
		_startDate = startDate;
	}

	public String getComment() {
		return _comment;
	}

	public void setComment(String comment) {
		_comment = comment;
	}

	private String _uuid;
	private long _workingHoursId;
	private long _companyId;
	private long _groupId;
	private long _userGroupId;
	private long _userId;
	private Date _createDate;
	private Date _modifiedDate;
	private long _taskId;
	private double _hours;
	private Date _startDate;
	private String _comment;

}