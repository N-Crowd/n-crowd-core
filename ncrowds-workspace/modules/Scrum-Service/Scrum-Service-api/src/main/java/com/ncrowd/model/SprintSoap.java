/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

/**
 * This class is used by SOAP remote services.
 *
 * @author Sudhanshu
 * @generated
 */
@ProviderType
public class SprintSoap implements Serializable {

	public static SprintSoap toSoapModel(Sprint model) {
		SprintSoap soapModel = new SprintSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setSprintId(model.getSprintId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setUserGroupId(model.getUserGroupId());
		soapModel.setStartDate(model.getStartDate());
		soapModel.setEndDate(model.getEndDate());
		soapModel.setSprintNumber(model.getSprintNumber());

		return soapModel;
	}

	public static SprintSoap[] toSoapModels(Sprint[] models) {
		SprintSoap[] soapModels = new SprintSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static SprintSoap[][] toSoapModels(Sprint[][] models) {
		SprintSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new SprintSoap[models.length][models[0].length];
		}
		else {
			soapModels = new SprintSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static SprintSoap[] toSoapModels(List<Sprint> models) {
		List<SprintSoap> soapModels = new ArrayList<SprintSoap>(models.size());

		for (Sprint model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new SprintSoap[soapModels.size()]);
	}

	public SprintSoap() {
	}

	public long getPrimaryKey() {
		return _sprintId;
	}

	public void setPrimaryKey(long pk) {
		setSprintId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getSprintId() {
		return _sprintId;
	}

	public void setSprintId(long sprintId) {
		_sprintId = sprintId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getUserGroupId() {
		return _userGroupId;
	}

	public void setUserGroupId(long userGroupId) {
		_userGroupId = userGroupId;
	}

	public Date getStartDate() {
		return _startDate;
	}

	public void setStartDate(Date startDate) {
		_startDate = startDate;
	}

	public Date getEndDate() {
		return _endDate;
	}

	public void setEndDate(Date endDate) {
		_endDate = endDate;
	}

	public int getSprintNumber() {
		return _sprintNumber;
	}

	public void setSprintNumber(int sprintNumber) {
		_sprintNumber = sprintNumber;
	}

	private String _uuid;
	private long _sprintId;
	private long _companyId;
	private long _groupId;
	private long _userGroupId;
	private Date _startDate;
	private Date _endDate;
	private int _sprintNumber;

}