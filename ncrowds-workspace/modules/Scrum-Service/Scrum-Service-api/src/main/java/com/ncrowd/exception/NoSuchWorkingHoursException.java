/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.ncrowd.exception;

import org.osgi.annotation.versioning.ProviderType;

import com.liferay.portal.kernel.exception.NoSuchModelException;

/**
 * @author Sudhanshu
 */
@ProviderType
public class NoSuchWorkingHoursException extends NoSuchModelException {

	public NoSuchWorkingHoursException() {
	}

	public NoSuchWorkingHoursException(String msg) {
		super(msg);
	}

	public NoSuchWorkingHoursException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public NoSuchWorkingHoursException(Throwable cause) {
		super(cause);
	}

}