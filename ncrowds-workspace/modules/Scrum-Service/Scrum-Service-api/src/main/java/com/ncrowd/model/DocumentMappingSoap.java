/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

/**
 * This class is used by SOAP remote services.
 *
 * @author Sudhanshu
 * @generated
 */
@ProviderType
public class DocumentMappingSoap implements Serializable {

	public static DocumentMappingSoap toSoapModel(DocumentMapping model) {
		DocumentMappingSoap soapModel = new DocumentMappingSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setMappingId(model.getMappingId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setUserGroupId(model.getUserGroupId());
		soapModel.setTaskId(model.getTaskId());
		soapModel.setDlFileEntryId(model.getDlFileEntryId());

		return soapModel;
	}

	public static DocumentMappingSoap[] toSoapModels(DocumentMapping[] models) {
		DocumentMappingSoap[] soapModels =
			new DocumentMappingSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static DocumentMappingSoap[][] toSoapModels(
		DocumentMapping[][] models) {

		DocumentMappingSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels =
				new DocumentMappingSoap[models.length][models[0].length];
		}
		else {
			soapModels = new DocumentMappingSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static DocumentMappingSoap[] toSoapModels(
		List<DocumentMapping> models) {

		List<DocumentMappingSoap> soapModels =
			new ArrayList<DocumentMappingSoap>(models.size());

		for (DocumentMapping model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new DocumentMappingSoap[soapModels.size()]);
	}

	public DocumentMappingSoap() {
	}

	public long getPrimaryKey() {
		return _mappingId;
	}

	public void setPrimaryKey(long pk) {
		setMappingId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getMappingId() {
		return _mappingId;
	}

	public void setMappingId(long mappingId) {
		_mappingId = mappingId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getUserGroupId() {
		return _userGroupId;
	}

	public void setUserGroupId(long userGroupId) {
		_userGroupId = userGroupId;
	}

	public long getTaskId() {
		return _taskId;
	}

	public void setTaskId(long taskId) {
		_taskId = taskId;
	}

	public long getDlFileEntryId() {
		return _dlFileEntryId;
	}

	public void setDlFileEntryId(long dlFileEntryId) {
		_dlFileEntryId = dlFileEntryId;
	}

	private String _uuid;
	private long _mappingId;
	private long _companyId;
	private long _groupId;
	private long _userGroupId;
	private long _taskId;
	private long _dlFileEntryId;

}