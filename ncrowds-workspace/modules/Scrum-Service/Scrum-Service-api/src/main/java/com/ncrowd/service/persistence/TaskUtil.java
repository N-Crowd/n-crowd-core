/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.ncrowd.model.Task;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osgi.annotation.versioning.ProviderType;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The persistence utility for the task service. This utility wraps <code>com.ncrowd.service.persistence.impl.TaskPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Sudhanshu
 * @see TaskPersistence
 * @generated
 */
@ProviderType
public class TaskUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Task task) {
		getPersistence().clearCache(task);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Task> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Task> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Task> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Task> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Task> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Task update(Task task) {
		return getPersistence().update(task);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Task update(Task task, ServiceContext serviceContext) {
		return getPersistence().update(task, serviceContext);
	}

	/**
	 * Returns all the tasks where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching tasks
	 */
	public static List<Task> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the tasks where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @return the range of matching tasks
	 */
	public static List<Task> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the tasks where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching tasks
	 */
	public static List<Task> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Task> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the tasks where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching tasks
	 */
	public static List<Task> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Task> orderByComparator, boolean retrieveFromCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, retrieveFromCache);
	}

	/**
	 * Returns the first task in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public static Task findByUuid_First(
			String uuid, OrderByComparator<Task> orderByComparator)
		throws com.ncrowd.exception.NoSuchTaskException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first task in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task, or <code>null</code> if a matching task could not be found
	 */
	public static Task fetchByUuid_First(
		String uuid, OrderByComparator<Task> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last task in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public static Task findByUuid_Last(
			String uuid, OrderByComparator<Task> orderByComparator)
		throws com.ncrowd.exception.NoSuchTaskException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last task in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task, or <code>null</code> if a matching task could not be found
	 */
	public static Task fetchByUuid_Last(
		String uuid, OrderByComparator<Task> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the tasks before and after the current task in the ordered set where uuid = &#63;.
	 *
	 * @param taskId the primary key of the current task
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next task
	 * @throws NoSuchTaskException if a task with the primary key could not be found
	 */
	public static Task[] findByUuid_PrevAndNext(
			long taskId, String uuid, OrderByComparator<Task> orderByComparator)
		throws com.ncrowd.exception.NoSuchTaskException {

		return getPersistence().findByUuid_PrevAndNext(
			taskId, uuid, orderByComparator);
	}

	/**
	 * Removes all the tasks where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of tasks where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching tasks
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the task where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchTaskException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public static Task findByUUID_G(String uuid, long groupId)
		throws com.ncrowd.exception.NoSuchTaskException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the task where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching task, or <code>null</code> if a matching task could not be found
	 */
	public static Task fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the task where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching task, or <code>null</code> if a matching task could not be found
	 */
	public static Task fetchByUUID_G(
		String uuid, long groupId, boolean retrieveFromCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, retrieveFromCache);
	}

	/**
	 * Removes the task where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the task that was removed
	 */
	public static Task removeByUUID_G(String uuid, long groupId)
		throws com.ncrowd.exception.NoSuchTaskException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of tasks where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching tasks
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the tasks where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching tasks
	 */
	public static List<Task> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the tasks where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @return the range of matching tasks
	 */
	public static List<Task> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the tasks where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching tasks
	 */
	public static List<Task> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Task> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the tasks where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching tasks
	 */
	public static List<Task> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Task> orderByComparator, boolean retrieveFromCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, retrieveFromCache);
	}

	/**
	 * Returns the first task in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public static Task findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Task> orderByComparator)
		throws com.ncrowd.exception.NoSuchTaskException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first task in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task, or <code>null</code> if a matching task could not be found
	 */
	public static Task fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Task> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last task in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public static Task findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Task> orderByComparator)
		throws com.ncrowd.exception.NoSuchTaskException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last task in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task, or <code>null</code> if a matching task could not be found
	 */
	public static Task fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Task> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the tasks before and after the current task in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param taskId the primary key of the current task
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next task
	 * @throws NoSuchTaskException if a task with the primary key could not be found
	 */
	public static Task[] findByUuid_C_PrevAndNext(
			long taskId, String uuid, long companyId,
			OrderByComparator<Task> orderByComparator)
		throws com.ncrowd.exception.NoSuchTaskException {

		return getPersistence().findByUuid_C_PrevAndNext(
			taskId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the tasks where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of tasks where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching tasks
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the tasks where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @return the matching tasks
	 */
	public static List<Task> findByUserGroupId(
		long userGroupId, boolean taskGroup) {

		return getPersistence().findByUserGroupId(userGroupId, taskGroup);
	}

	/**
	 * Returns a range of all the tasks where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @return the range of matching tasks
	 */
	public static List<Task> findByUserGroupId(
		long userGroupId, boolean taskGroup, int start, int end) {

		return getPersistence().findByUserGroupId(
			userGroupId, taskGroup, start, end);
	}

	/**
	 * Returns an ordered range of all the tasks where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching tasks
	 */
	public static List<Task> findByUserGroupId(
		long userGroupId, boolean taskGroup, int start, int end,
		OrderByComparator<Task> orderByComparator) {

		return getPersistence().findByUserGroupId(
			userGroupId, taskGroup, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the tasks where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching tasks
	 */
	public static List<Task> findByUserGroupId(
		long userGroupId, boolean taskGroup, int start, int end,
		OrderByComparator<Task> orderByComparator, boolean retrieveFromCache) {

		return getPersistence().findByUserGroupId(
			userGroupId, taskGroup, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	 * Returns the first task in the ordered set where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public static Task findByUserGroupId_First(
			long userGroupId, boolean taskGroup,
			OrderByComparator<Task> orderByComparator)
		throws com.ncrowd.exception.NoSuchTaskException {

		return getPersistence().findByUserGroupId_First(
			userGroupId, taskGroup, orderByComparator);
	}

	/**
	 * Returns the first task in the ordered set where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task, or <code>null</code> if a matching task could not be found
	 */
	public static Task fetchByUserGroupId_First(
		long userGroupId, boolean taskGroup,
		OrderByComparator<Task> orderByComparator) {

		return getPersistence().fetchByUserGroupId_First(
			userGroupId, taskGroup, orderByComparator);
	}

	/**
	 * Returns the last task in the ordered set where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public static Task findByUserGroupId_Last(
			long userGroupId, boolean taskGroup,
			OrderByComparator<Task> orderByComparator)
		throws com.ncrowd.exception.NoSuchTaskException {

		return getPersistence().findByUserGroupId_Last(
			userGroupId, taskGroup, orderByComparator);
	}

	/**
	 * Returns the last task in the ordered set where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task, or <code>null</code> if a matching task could not be found
	 */
	public static Task fetchByUserGroupId_Last(
		long userGroupId, boolean taskGroup,
		OrderByComparator<Task> orderByComparator) {

		return getPersistence().fetchByUserGroupId_Last(
			userGroupId, taskGroup, orderByComparator);
	}

	/**
	 * Returns the tasks before and after the current task in the ordered set where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param taskId the primary key of the current task
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next task
	 * @throws NoSuchTaskException if a task with the primary key could not be found
	 */
	public static Task[] findByUserGroupId_PrevAndNext(
			long taskId, long userGroupId, boolean taskGroup,
			OrderByComparator<Task> orderByComparator)
		throws com.ncrowd.exception.NoSuchTaskException {

		return getPersistence().findByUserGroupId_PrevAndNext(
			taskId, userGroupId, taskGroup, orderByComparator);
	}

	/**
	 * Removes all the tasks where userGroupId = &#63; and taskGroup = &#63; from the database.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 */
	public static void removeByUserGroupId(
		long userGroupId, boolean taskGroup) {

		getPersistence().removeByUserGroupId(userGroupId, taskGroup);
	}

	/**
	 * Returns the number of tasks where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @return the number of matching tasks
	 */
	public static int countByUserGroupId(long userGroupId, boolean taskGroup) {
		return getPersistence().countByUserGroupId(userGroupId, taskGroup);
	}

	/**
	 * Returns all the tasks where userGroupId = &#63; and sprintId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param taskGroup the task group
	 * @return the matching tasks
	 */
	public static List<Task> findByG_S(
		long userGroupId, long sprintId, boolean taskGroup) {

		return getPersistence().findByG_S(userGroupId, sprintId, taskGroup);
	}

	/**
	 * Returns a range of all the tasks where userGroupId = &#63; and sprintId = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @return the range of matching tasks
	 */
	public static List<Task> findByG_S(
		long userGroupId, long sprintId, boolean taskGroup, int start,
		int end) {

		return getPersistence().findByG_S(
			userGroupId, sprintId, taskGroup, start, end);
	}

	/**
	 * Returns an ordered range of all the tasks where userGroupId = &#63; and sprintId = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching tasks
	 */
	public static List<Task> findByG_S(
		long userGroupId, long sprintId, boolean taskGroup, int start, int end,
		OrderByComparator<Task> orderByComparator) {

		return getPersistence().findByG_S(
			userGroupId, sprintId, taskGroup, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the tasks where userGroupId = &#63; and sprintId = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching tasks
	 */
	public static List<Task> findByG_S(
		long userGroupId, long sprintId, boolean taskGroup, int start, int end,
		OrderByComparator<Task> orderByComparator, boolean retrieveFromCache) {

		return getPersistence().findByG_S(
			userGroupId, sprintId, taskGroup, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	 * Returns the first task in the ordered set where userGroupId = &#63; and sprintId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public static Task findByG_S_First(
			long userGroupId, long sprintId, boolean taskGroup,
			OrderByComparator<Task> orderByComparator)
		throws com.ncrowd.exception.NoSuchTaskException {

		return getPersistence().findByG_S_First(
			userGroupId, sprintId, taskGroup, orderByComparator);
	}

	/**
	 * Returns the first task in the ordered set where userGroupId = &#63; and sprintId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task, or <code>null</code> if a matching task could not be found
	 */
	public static Task fetchByG_S_First(
		long userGroupId, long sprintId, boolean taskGroup,
		OrderByComparator<Task> orderByComparator) {

		return getPersistence().fetchByG_S_First(
			userGroupId, sprintId, taskGroup, orderByComparator);
	}

	/**
	 * Returns the last task in the ordered set where userGroupId = &#63; and sprintId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public static Task findByG_S_Last(
			long userGroupId, long sprintId, boolean taskGroup,
			OrderByComparator<Task> orderByComparator)
		throws com.ncrowd.exception.NoSuchTaskException {

		return getPersistence().findByG_S_Last(
			userGroupId, sprintId, taskGroup, orderByComparator);
	}

	/**
	 * Returns the last task in the ordered set where userGroupId = &#63; and sprintId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task, or <code>null</code> if a matching task could not be found
	 */
	public static Task fetchByG_S_Last(
		long userGroupId, long sprintId, boolean taskGroup,
		OrderByComparator<Task> orderByComparator) {

		return getPersistence().fetchByG_S_Last(
			userGroupId, sprintId, taskGroup, orderByComparator);
	}

	/**
	 * Returns the tasks before and after the current task in the ordered set where userGroupId = &#63; and sprintId = &#63; and taskGroup = &#63;.
	 *
	 * @param taskId the primary key of the current task
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next task
	 * @throws NoSuchTaskException if a task with the primary key could not be found
	 */
	public static Task[] findByG_S_PrevAndNext(
			long taskId, long userGroupId, long sprintId, boolean taskGroup,
			OrderByComparator<Task> orderByComparator)
		throws com.ncrowd.exception.NoSuchTaskException {

		return getPersistence().findByG_S_PrevAndNext(
			taskId, userGroupId, sprintId, taskGroup, orderByComparator);
	}

	/**
	 * Removes all the tasks where userGroupId = &#63; and sprintId = &#63; and taskGroup = &#63; from the database.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param taskGroup the task group
	 */
	public static void removeByG_S(
		long userGroupId, long sprintId, boolean taskGroup) {

		getPersistence().removeByG_S(userGroupId, sprintId, taskGroup);
	}

	/**
	 * Returns the number of tasks where userGroupId = &#63; and sprintId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param taskGroup the task group
	 * @return the number of matching tasks
	 */
	public static int countByG_S(
		long userGroupId, long sprintId, boolean taskGroup) {

		return getPersistence().countByG_S(userGroupId, sprintId, taskGroup);
	}

	/**
	 * Returns all the tasks where userGroupId = &#63; and sprintId = &#63; and statusId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param statusId the status ID
	 * @param taskGroup the task group
	 * @return the matching tasks
	 */
	public static List<Task> findByG_S_S(
		long userGroupId, long sprintId, long statusId, boolean taskGroup) {

		return getPersistence().findByG_S_S(
			userGroupId, sprintId, statusId, taskGroup);
	}

	/**
	 * Returns a range of all the tasks where userGroupId = &#63; and sprintId = &#63; and statusId = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param statusId the status ID
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @return the range of matching tasks
	 */
	public static List<Task> findByG_S_S(
		long userGroupId, long sprintId, long statusId, boolean taskGroup,
		int start, int end) {

		return getPersistence().findByG_S_S(
			userGroupId, sprintId, statusId, taskGroup, start, end);
	}

	/**
	 * Returns an ordered range of all the tasks where userGroupId = &#63; and sprintId = &#63; and statusId = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param statusId the status ID
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching tasks
	 */
	public static List<Task> findByG_S_S(
		long userGroupId, long sprintId, long statusId, boolean taskGroup,
		int start, int end, OrderByComparator<Task> orderByComparator) {

		return getPersistence().findByG_S_S(
			userGroupId, sprintId, statusId, taskGroup, start, end,
			orderByComparator);
	}

	/**
	 * Returns an ordered range of all the tasks where userGroupId = &#63; and sprintId = &#63; and statusId = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param statusId the status ID
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching tasks
	 */
	public static List<Task> findByG_S_S(
		long userGroupId, long sprintId, long statusId, boolean taskGroup,
		int start, int end, OrderByComparator<Task> orderByComparator,
		boolean retrieveFromCache) {

		return getPersistence().findByG_S_S(
			userGroupId, sprintId, statusId, taskGroup, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	 * Returns the first task in the ordered set where userGroupId = &#63; and sprintId = &#63; and statusId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param statusId the status ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public static Task findByG_S_S_First(
			long userGroupId, long sprintId, long statusId, boolean taskGroup,
			OrderByComparator<Task> orderByComparator)
		throws com.ncrowd.exception.NoSuchTaskException {

		return getPersistence().findByG_S_S_First(
			userGroupId, sprintId, statusId, taskGroup, orderByComparator);
	}

	/**
	 * Returns the first task in the ordered set where userGroupId = &#63; and sprintId = &#63; and statusId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param statusId the status ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task, or <code>null</code> if a matching task could not be found
	 */
	public static Task fetchByG_S_S_First(
		long userGroupId, long sprintId, long statusId, boolean taskGroup,
		OrderByComparator<Task> orderByComparator) {

		return getPersistence().fetchByG_S_S_First(
			userGroupId, sprintId, statusId, taskGroup, orderByComparator);
	}

	/**
	 * Returns the last task in the ordered set where userGroupId = &#63; and sprintId = &#63; and statusId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param statusId the status ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public static Task findByG_S_S_Last(
			long userGroupId, long sprintId, long statusId, boolean taskGroup,
			OrderByComparator<Task> orderByComparator)
		throws com.ncrowd.exception.NoSuchTaskException {

		return getPersistence().findByG_S_S_Last(
			userGroupId, sprintId, statusId, taskGroup, orderByComparator);
	}

	/**
	 * Returns the last task in the ordered set where userGroupId = &#63; and sprintId = &#63; and statusId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param statusId the status ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task, or <code>null</code> if a matching task could not be found
	 */
	public static Task fetchByG_S_S_Last(
		long userGroupId, long sprintId, long statusId, boolean taskGroup,
		OrderByComparator<Task> orderByComparator) {

		return getPersistence().fetchByG_S_S_Last(
			userGroupId, sprintId, statusId, taskGroup, orderByComparator);
	}

	/**
	 * Returns the tasks before and after the current task in the ordered set where userGroupId = &#63; and sprintId = &#63; and statusId = &#63; and taskGroup = &#63;.
	 *
	 * @param taskId the primary key of the current task
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param statusId the status ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next task
	 * @throws NoSuchTaskException if a task with the primary key could not be found
	 */
	public static Task[] findByG_S_S_PrevAndNext(
			long taskId, long userGroupId, long sprintId, long statusId,
			boolean taskGroup, OrderByComparator<Task> orderByComparator)
		throws com.ncrowd.exception.NoSuchTaskException {

		return getPersistence().findByG_S_S_PrevAndNext(
			taskId, userGroupId, sprintId, statusId, taskGroup,
			orderByComparator);
	}

	/**
	 * Removes all the tasks where userGroupId = &#63; and sprintId = &#63; and statusId = &#63; and taskGroup = &#63; from the database.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param statusId the status ID
	 * @param taskGroup the task group
	 */
	public static void removeByG_S_S(
		long userGroupId, long sprintId, long statusId, boolean taskGroup) {

		getPersistence().removeByG_S_S(
			userGroupId, sprintId, statusId, taskGroup);
	}

	/**
	 * Returns the number of tasks where userGroupId = &#63; and sprintId = &#63; and statusId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param statusId the status ID
	 * @param taskGroup the task group
	 * @return the number of matching tasks
	 */
	public static int countByG_S_S(
		long userGroupId, long sprintId, long statusId, boolean taskGroup) {

		return getPersistence().countByG_S_S(
			userGroupId, sprintId, statusId, taskGroup);
	}

	/**
	 * Returns all the tasks where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @return the matching tasks
	 */
	public static List<Task> findByTaskGroup(
		long userGroupId, boolean taskGroup) {

		return getPersistence().findByTaskGroup(userGroupId, taskGroup);
	}

	/**
	 * Returns a range of all the tasks where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @return the range of matching tasks
	 */
	public static List<Task> findByTaskGroup(
		long userGroupId, boolean taskGroup, int start, int end) {

		return getPersistence().findByTaskGroup(
			userGroupId, taskGroup, start, end);
	}

	/**
	 * Returns an ordered range of all the tasks where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching tasks
	 */
	public static List<Task> findByTaskGroup(
		long userGroupId, boolean taskGroup, int start, int end,
		OrderByComparator<Task> orderByComparator) {

		return getPersistence().findByTaskGroup(
			userGroupId, taskGroup, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the tasks where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching tasks
	 */
	public static List<Task> findByTaskGroup(
		long userGroupId, boolean taskGroup, int start, int end,
		OrderByComparator<Task> orderByComparator, boolean retrieveFromCache) {

		return getPersistence().findByTaskGroup(
			userGroupId, taskGroup, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	 * Returns the first task in the ordered set where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public static Task findByTaskGroup_First(
			long userGroupId, boolean taskGroup,
			OrderByComparator<Task> orderByComparator)
		throws com.ncrowd.exception.NoSuchTaskException {

		return getPersistence().findByTaskGroup_First(
			userGroupId, taskGroup, orderByComparator);
	}

	/**
	 * Returns the first task in the ordered set where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task, or <code>null</code> if a matching task could not be found
	 */
	public static Task fetchByTaskGroup_First(
		long userGroupId, boolean taskGroup,
		OrderByComparator<Task> orderByComparator) {

		return getPersistence().fetchByTaskGroup_First(
			userGroupId, taskGroup, orderByComparator);
	}

	/**
	 * Returns the last task in the ordered set where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public static Task findByTaskGroup_Last(
			long userGroupId, boolean taskGroup,
			OrderByComparator<Task> orderByComparator)
		throws com.ncrowd.exception.NoSuchTaskException {

		return getPersistence().findByTaskGroup_Last(
			userGroupId, taskGroup, orderByComparator);
	}

	/**
	 * Returns the last task in the ordered set where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task, or <code>null</code> if a matching task could not be found
	 */
	public static Task fetchByTaskGroup_Last(
		long userGroupId, boolean taskGroup,
		OrderByComparator<Task> orderByComparator) {

		return getPersistence().fetchByTaskGroup_Last(
			userGroupId, taskGroup, orderByComparator);
	}

	/**
	 * Returns the tasks before and after the current task in the ordered set where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param taskId the primary key of the current task
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next task
	 * @throws NoSuchTaskException if a task with the primary key could not be found
	 */
	public static Task[] findByTaskGroup_PrevAndNext(
			long taskId, long userGroupId, boolean taskGroup,
			OrderByComparator<Task> orderByComparator)
		throws com.ncrowd.exception.NoSuchTaskException {

		return getPersistence().findByTaskGroup_PrevAndNext(
			taskId, userGroupId, taskGroup, orderByComparator);
	}

	/**
	 * Removes all the tasks where userGroupId = &#63; and taskGroup = &#63; from the database.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 */
	public static void removeByTaskGroup(long userGroupId, boolean taskGroup) {
		getPersistence().removeByTaskGroup(userGroupId, taskGroup);
	}

	/**
	 * Returns the number of tasks where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @return the number of matching tasks
	 */
	public static int countByTaskGroup(long userGroupId, boolean taskGroup) {
		return getPersistence().countByTaskGroup(userGroupId, taskGroup);
	}

	/**
	 * Returns all the tasks where userGroupId = &#63; and description = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param description the description
	 * @param taskGroup the task group
	 * @return the matching tasks
	 */
	public static List<Task> findByTaskGroupName(
		long userGroupId, String description, boolean taskGroup) {

		return getPersistence().findByTaskGroupName(
			userGroupId, description, taskGroup);
	}

	/**
	 * Returns a range of all the tasks where userGroupId = &#63; and description = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param description the description
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @return the range of matching tasks
	 */
	public static List<Task> findByTaskGroupName(
		long userGroupId, String description, boolean taskGroup, int start,
		int end) {

		return getPersistence().findByTaskGroupName(
			userGroupId, description, taskGroup, start, end);
	}

	/**
	 * Returns an ordered range of all the tasks where userGroupId = &#63; and description = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param description the description
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching tasks
	 */
	public static List<Task> findByTaskGroupName(
		long userGroupId, String description, boolean taskGroup, int start,
		int end, OrderByComparator<Task> orderByComparator) {

		return getPersistence().findByTaskGroupName(
			userGroupId, description, taskGroup, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the tasks where userGroupId = &#63; and description = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param description the description
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching tasks
	 */
	public static List<Task> findByTaskGroupName(
		long userGroupId, String description, boolean taskGroup, int start,
		int end, OrderByComparator<Task> orderByComparator,
		boolean retrieveFromCache) {

		return getPersistence().findByTaskGroupName(
			userGroupId, description, taskGroup, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	 * Returns the first task in the ordered set where userGroupId = &#63; and description = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param description the description
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public static Task findByTaskGroupName_First(
			long userGroupId, String description, boolean taskGroup,
			OrderByComparator<Task> orderByComparator)
		throws com.ncrowd.exception.NoSuchTaskException {

		return getPersistence().findByTaskGroupName_First(
			userGroupId, description, taskGroup, orderByComparator);
	}

	/**
	 * Returns the first task in the ordered set where userGroupId = &#63; and description = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param description the description
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task, or <code>null</code> if a matching task could not be found
	 */
	public static Task fetchByTaskGroupName_First(
		long userGroupId, String description, boolean taskGroup,
		OrderByComparator<Task> orderByComparator) {

		return getPersistence().fetchByTaskGroupName_First(
			userGroupId, description, taskGroup, orderByComparator);
	}

	/**
	 * Returns the last task in the ordered set where userGroupId = &#63; and description = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param description the description
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public static Task findByTaskGroupName_Last(
			long userGroupId, String description, boolean taskGroup,
			OrderByComparator<Task> orderByComparator)
		throws com.ncrowd.exception.NoSuchTaskException {

		return getPersistence().findByTaskGroupName_Last(
			userGroupId, description, taskGroup, orderByComparator);
	}

	/**
	 * Returns the last task in the ordered set where userGroupId = &#63; and description = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param description the description
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task, or <code>null</code> if a matching task could not be found
	 */
	public static Task fetchByTaskGroupName_Last(
		long userGroupId, String description, boolean taskGroup,
		OrderByComparator<Task> orderByComparator) {

		return getPersistence().fetchByTaskGroupName_Last(
			userGroupId, description, taskGroup, orderByComparator);
	}

	/**
	 * Returns the tasks before and after the current task in the ordered set where userGroupId = &#63; and description = &#63; and taskGroup = &#63;.
	 *
	 * @param taskId the primary key of the current task
	 * @param userGroupId the user group ID
	 * @param description the description
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next task
	 * @throws NoSuchTaskException if a task with the primary key could not be found
	 */
	public static Task[] findByTaskGroupName_PrevAndNext(
			long taskId, long userGroupId, String description,
			boolean taskGroup, OrderByComparator<Task> orderByComparator)
		throws com.ncrowd.exception.NoSuchTaskException {

		return getPersistence().findByTaskGroupName_PrevAndNext(
			taskId, userGroupId, description, taskGroup, orderByComparator);
	}

	/**
	 * Removes all the tasks where userGroupId = &#63; and description = &#63; and taskGroup = &#63; from the database.
	 *
	 * @param userGroupId the user group ID
	 * @param description the description
	 * @param taskGroup the task group
	 */
	public static void removeByTaskGroupName(
		long userGroupId, String description, boolean taskGroup) {

		getPersistence().removeByTaskGroupName(
			userGroupId, description, taskGroup);
	}

	/**
	 * Returns the number of tasks where userGroupId = &#63; and description = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param description the description
	 * @param taskGroup the task group
	 * @return the number of matching tasks
	 */
	public static int countByTaskGroupName(
		long userGroupId, String description, boolean taskGroup) {

		return getPersistence().countByTaskGroupName(
			userGroupId, description, taskGroup);
	}

	/**
	 * Caches the task in the entity cache if it is enabled.
	 *
	 * @param task the task
	 */
	public static void cacheResult(Task task) {
		getPersistence().cacheResult(task);
	}

	/**
	 * Caches the tasks in the entity cache if it is enabled.
	 *
	 * @param tasks the tasks
	 */
	public static void cacheResult(List<Task> tasks) {
		getPersistence().cacheResult(tasks);
	}

	/**
	 * Creates a new task with the primary key. Does not add the task to the database.
	 *
	 * @param taskId the primary key for the new task
	 * @return the new task
	 */
	public static Task create(long taskId) {
		return getPersistence().create(taskId);
	}

	/**
	 * Removes the task with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param taskId the primary key of the task
	 * @return the task that was removed
	 * @throws NoSuchTaskException if a task with the primary key could not be found
	 */
	public static Task remove(long taskId)
		throws com.ncrowd.exception.NoSuchTaskException {

		return getPersistence().remove(taskId);
	}

	public static Task updateImpl(Task task) {
		return getPersistence().updateImpl(task);
	}

	/**
	 * Returns the task with the primary key or throws a <code>NoSuchTaskException</code> if it could not be found.
	 *
	 * @param taskId the primary key of the task
	 * @return the task
	 * @throws NoSuchTaskException if a task with the primary key could not be found
	 */
	public static Task findByPrimaryKey(long taskId)
		throws com.ncrowd.exception.NoSuchTaskException {

		return getPersistence().findByPrimaryKey(taskId);
	}

	/**
	 * Returns the task with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param taskId the primary key of the task
	 * @return the task, or <code>null</code> if a task with the primary key could not be found
	 */
	public static Task fetchByPrimaryKey(long taskId) {
		return getPersistence().fetchByPrimaryKey(taskId);
	}

	/**
	 * Returns all the tasks.
	 *
	 * @return the tasks
	 */
	public static List<Task> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the tasks.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @return the range of tasks
	 */
	public static List<Task> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the tasks.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of tasks
	 */
	public static List<Task> findAll(
		int start, int end, OrderByComparator<Task> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the tasks.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of tasks
	 */
	public static List<Task> findAll(
		int start, int end, OrderByComparator<Task> orderByComparator,
		boolean retrieveFromCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, retrieveFromCache);
	}

	/**
	 * Removes all the tasks from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of tasks.
	 *
	 * @return the number of tasks
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static TaskPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<TaskPersistence, TaskPersistence>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(TaskPersistence.class);

		ServiceTracker<TaskPersistence, TaskPersistence> serviceTracker =
			new ServiceTracker<TaskPersistence, TaskPersistence>(
				bundle.getBundleContext(), TaskPersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}