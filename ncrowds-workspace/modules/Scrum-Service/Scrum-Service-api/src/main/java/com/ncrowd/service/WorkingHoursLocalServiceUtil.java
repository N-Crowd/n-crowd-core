/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.service;

import org.osgi.annotation.versioning.ProviderType;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for WorkingHours. This utility wraps
 * <code>com.ncrowd.service.impl.WorkingHoursLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Sudhanshu
 * @see WorkingHoursLocalService
 * @generated
 */
@ProviderType
public class WorkingHoursLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.ncrowd.service.impl.WorkingHoursLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the working hours to the database. Also notifies the appropriate model listeners.
	 *
	 * @param workingHours the working hours
	 * @return the working hours that was added
	 */
	public static com.ncrowd.model.WorkingHours addWorkingHours(
		com.ncrowd.model.WorkingHours workingHours) {

		return getService().addWorkingHours(workingHours);
	}

	/**
	 * Creates a new working hours with the primary key. Does not add the working hours to the database.
	 *
	 * @param workingHoursId the primary key for the new working hours
	 * @return the new working hours
	 */
	public static com.ncrowd.model.WorkingHours createWorkingHours(
		long workingHoursId) {

		return getService().createWorkingHours(workingHoursId);
	}

	/**
	 * @throws PortalException
	 */
	public static com.liferay.portal.kernel.model.PersistedModel
			deletePersistedModel(
				com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the working hours with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param workingHoursId the primary key of the working hours
	 * @return the working hours that was removed
	 * @throws PortalException if a working hours with the primary key could not be found
	 */
	public static com.ncrowd.model.WorkingHours deleteWorkingHours(
			long workingHoursId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deleteWorkingHours(workingHoursId);
	}

	/**
	 * Deletes the working hours from the database. Also notifies the appropriate model listeners.
	 *
	 * @param workingHours the working hours
	 * @return the working hours that was removed
	 */
	public static com.ncrowd.model.WorkingHours deleteWorkingHours(
		com.ncrowd.model.WorkingHours workingHours) {

		return getService().deleteWorkingHours(workingHours);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery
		dynamicQuery() {

		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.ncrowd.model.impl.WorkingHoursModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.ncrowd.model.impl.WorkingHoursModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static com.ncrowd.model.WorkingHours fetchWorkingHours(
		long workingHoursId) {

		return getService().fetchWorkingHours(workingHoursId);
	}

	/**
	 * Returns the working hours matching the UUID and group.
	 *
	 * @param uuid the working hours's UUID
	 * @param groupId the primary key of the group
	 * @return the matching working hours, or <code>null</code> if a matching working hours could not be found
	 */
	public static com.ncrowd.model.WorkingHours
		fetchWorkingHoursByUuidAndGroupId(String uuid, long groupId) {

		return getService().fetchWorkingHoursByUuidAndGroupId(uuid, groupId);
	}

	public static java.util.List<com.ncrowd.model.WorkingHours> findByTaskId(
		long taskId) {

		return getService().findByTaskId(taskId);
	}

	public static java.util.List<com.ncrowd.model.WorkingHours>
		findByUserGroupId(long userGroupId) {

		return getService().findByUserGroupId(userGroupId);
	}

	public static java.util.List<com.ncrowd.model.WorkingHours> findByUserId(
		long userId) {

		return getService().findByUserId(userId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static com.liferay.portal.kernel.model.PersistedModel
			getPersistedModel(java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the working hours with the primary key.
	 *
	 * @param workingHoursId the primary key of the working hours
	 * @return the working hours
	 * @throws PortalException if a working hours with the primary key could not be found
	 */
	public static com.ncrowd.model.WorkingHours getWorkingHours(
			long workingHoursId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getWorkingHours(workingHoursId);
	}

	/**
	 * Returns the working hours matching the UUID and group.
	 *
	 * @param uuid the working hours's UUID
	 * @param groupId the primary key of the group
	 * @return the matching working hours
	 * @throws PortalException if a matching working hours could not be found
	 */
	public static com.ncrowd.model.WorkingHours getWorkingHoursByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getWorkingHoursByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the working hourses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.ncrowd.model.impl.WorkingHoursModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of working hourses
	 * @param end the upper bound of the range of working hourses (not inclusive)
	 * @return the range of working hourses
	 */
	public static java.util.List<com.ncrowd.model.WorkingHours>
		getWorkingHourses(int start, int end) {

		return getService().getWorkingHourses(start, end);
	}

	/**
	 * Returns all the working hourses matching the UUID and company.
	 *
	 * @param uuid the UUID of the working hourses
	 * @param companyId the primary key of the company
	 * @return the matching working hourses, or an empty list if no matches were found
	 */
	public static java.util.List<com.ncrowd.model.WorkingHours>
		getWorkingHoursesByUuidAndCompanyId(String uuid, long companyId) {

		return getService().getWorkingHoursesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of working hourses matching the UUID and company.
	 *
	 * @param uuid the UUID of the working hourses
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of working hourses
	 * @param end the upper bound of the range of working hourses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching working hourses, or an empty list if no matches were found
	 */
	public static java.util.List<com.ncrowd.model.WorkingHours>
		getWorkingHoursesByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.ncrowd.model.WorkingHours> orderByComparator) {

		return getService().getWorkingHoursesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of working hourses.
	 *
	 * @return the number of working hourses
	 */
	public static int getWorkingHoursesCount() {
		return getService().getWorkingHoursesCount();
	}

	public static boolean saveWorkingHours(
		com.ncrowd.model.WorkingHours hours) {

		return getService().saveWorkingHours(hours);
	}

	/**
	 * Updates the working hours in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param workingHours the working hours
	 * @return the working hours that was updated
	 */
	public static com.ncrowd.model.WorkingHours updateWorkingHours(
		com.ncrowd.model.WorkingHours workingHours) {

		return getService().updateWorkingHours(workingHours);
	}

	public static WorkingHoursLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker
		<WorkingHoursLocalService, WorkingHoursLocalService> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(WorkingHoursLocalService.class);

		ServiceTracker<WorkingHoursLocalService, WorkingHoursLocalService>
			serviceTracker =
				new ServiceTracker
					<WorkingHoursLocalService, WorkingHoursLocalService>(
						bundle.getBundleContext(),
						WorkingHoursLocalService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}