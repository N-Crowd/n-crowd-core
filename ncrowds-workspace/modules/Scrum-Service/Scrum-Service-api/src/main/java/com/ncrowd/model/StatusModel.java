/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.model.ShardedModel;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The base model interface for the Status service. Represents a row in the &quot;Scrum_Status&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation <code>com.ncrowd.model.impl.StatusModelImpl</code> exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in <code>com.ncrowd.model.impl.StatusImpl</code>.
 * </p>
 *
 * @author Sudhanshu
 * @see Status
 * @generated
 */
@ProviderType
public interface StatusModel extends BaseModel<Status>, ShardedModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a status model instance should use the {@link Status} interface instead.
	 */

	/**
	 * Returns the primary key of this status.
	 *
	 * @return the primary key of this status
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this status.
	 *
	 * @param primaryKey the primary key of this status
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the uuid of this status.
	 *
	 * @return the uuid of this status
	 */
	@AutoEscape
	public String getUuid();

	/**
	 * Sets the uuid of this status.
	 *
	 * @param uuid the uuid of this status
	 */
	public void setUuid(String uuid);

	/**
	 * Returns the status ID of this status.
	 *
	 * @return the status ID of this status
	 */
	public long getStatusId();

	/**
	 * Sets the status ID of this status.
	 *
	 * @param statusId the status ID of this status
	 */
	public void setStatusId(long statusId);

	/**
	 * Returns the company ID of this status.
	 *
	 * @return the company ID of this status
	 */
	@Override
	public long getCompanyId();

	/**
	 * Sets the company ID of this status.
	 *
	 * @param companyId the company ID of this status
	 */
	@Override
	public void setCompanyId(long companyId);

	/**
	 * Returns the group ID of this status.
	 *
	 * @return the group ID of this status
	 */
	public long getGroupId();

	/**
	 * Sets the group ID of this status.
	 *
	 * @param groupId the group ID of this status
	 */
	public void setGroupId(long groupId);

	/**
	 * Returns the user group ID of this status.
	 *
	 * @return the user group ID of this status
	 */
	public long getUserGroupId();

	/**
	 * Sets the user group ID of this status.
	 *
	 * @param userGroupId the user group ID of this status
	 */
	public void setUserGroupId(long userGroupId);

	/**
	 * Returns the name of this status.
	 *
	 * @return the name of this status
	 */
	@AutoEscape
	public String getName();

	/**
	 * Sets the name of this status.
	 *
	 * @param name the name of this status
	 */
	public void setName(String name);

	/**
	 * Returns the sort index of this status.
	 *
	 * @return the sort index of this status
	 */
	public int getSortIndex();

	/**
	 * Sets the sort index of this status.
	 *
	 * @param sortIndex the sort index of this status
	 */
	public void setSortIndex(int sortIndex);

	/**
	 * Returns the fixed status of this status.
	 *
	 * @return the fixed status of this status
	 */
	public boolean getFixedStatus();

	/**
	 * Returns <code>true</code> if this status is fixed status.
	 *
	 * @return <code>true</code> if this status is fixed status; <code>false</code> otherwise
	 */
	public boolean isFixedStatus();

	/**
	 * Sets whether this status is fixed status.
	 *
	 * @param fixedStatus the fixed status of this status
	 */
	public void setFixedStatus(boolean fixedStatus);

}