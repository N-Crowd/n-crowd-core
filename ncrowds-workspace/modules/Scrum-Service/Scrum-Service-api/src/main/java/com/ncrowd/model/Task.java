/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.model;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The extended model interface for the Task service. Represents a row in the &quot;Scrum_Task&quot; database table, with each column mapped to a property of this class.
 *
 * @author Sudhanshu
 * @see TaskModel
 * @generated
 */
@ImplementationClassName("com.ncrowd.model.impl.TaskImpl")
@ProviderType
public interface Task extends PersistedModel, TaskModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to <code>com.ncrowd.model.impl.TaskImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<Task, Long> TASK_ID_ACCESSOR =
		new Accessor<Task, Long>() {

			@Override
			public Long get(Task task) {
				return task.getTaskId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<Task> getTypeClass() {
				return Task.class;
			}

		};

	public double getTimeForCopy();

	public void setTimeForCopy(double timeForCopy);

	public int compareTo(Task task);

	public WorkingHours getTimeToSave();

	public void setWorkingHours(java.util.List<WorkingHours> hours);

	public void setTimeToSave(WorkingHours timeToSave);

	public void setGroupTask(Task groupTask);

	public Task getGroupTask();

	public Status getStatus();

	public void setStatus(Status status);

	public String getAssignedToName();

	public com.liferay.portal.kernel.model.User getAssignedToUser();

	public void setAssignedToUser(com.liferay.portal.kernel.model.User user);

	public java.util.List<WorkingHours> getWorkingHours();

	public float getTotalWorkingHours();

	public String getStatusTableClass();

}