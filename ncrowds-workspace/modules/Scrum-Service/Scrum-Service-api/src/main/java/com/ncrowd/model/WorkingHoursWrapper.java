/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.osgi.annotation.versioning.ProviderType;

/**
 * <p>
 * This class is a wrapper for {@link WorkingHours}.
 * </p>
 *
 * @author Sudhanshu
 * @see WorkingHours
 * @generated
 */
@ProviderType
public class WorkingHoursWrapper
	extends BaseModelWrapper<WorkingHours>
	implements WorkingHours, ModelWrapper<WorkingHours> {

	public WorkingHoursWrapper(WorkingHours workingHours) {
		super(workingHours);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("workingHoursId", getWorkingHoursId());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("userGroupId", getUserGroupId());
		attributes.put("userId", getUserId());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("taskId", getTaskId());
		attributes.put("hours", getHours());
		attributes.put("startDate", getStartDate());
		attributes.put("comment", getComment());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long workingHoursId = (Long)attributes.get("workingHoursId");

		if (workingHoursId != null) {
			setWorkingHoursId(workingHoursId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long userGroupId = (Long)attributes.get("userGroupId");

		if (userGroupId != null) {
			setUserGroupId(userGroupId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long taskId = (Long)attributes.get("taskId");

		if (taskId != null) {
			setTaskId(taskId);
		}

		Double hours = (Double)attributes.get("hours");

		if (hours != null) {
			setHours(hours);
		}

		Date startDate = (Date)attributes.get("startDate");

		if (startDate != null) {
			setStartDate(startDate);
		}

		String comment = (String)attributes.get("comment");

		if (comment != null) {
			setComment(comment);
		}
	}

	/**
	 * Returns the comment of this working hours.
	 *
	 * @return the comment of this working hours
	 */
	@Override
	public String getComment() {
		return model.getComment();
	}

	/**
	 * Returns the company ID of this working hours.
	 *
	 * @return the company ID of this working hours
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this working hours.
	 *
	 * @return the create date of this working hours
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	@Override
	public String getEmployeeName() {
		return model.getEmployeeName();
	}

	/**
	 * Returns the group ID of this working hours.
	 *
	 * @return the group ID of this working hours
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the hours of this working hours.
	 *
	 * @return the hours of this working hours
	 */
	@Override
	public double getHours() {
		return model.getHours();
	}

	/**
	 * Returns the modified date of this working hours.
	 *
	 * @return the modified date of this working hours
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the primary key of this working hours.
	 *
	 * @return the primary key of this working hours
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the start date of this working hours.
	 *
	 * @return the start date of this working hours
	 */
	@Override
	public Date getStartDate() {
		return model.getStartDate();
	}

	/**
	 * Returns the task ID of this working hours.
	 *
	 * @return the task ID of this working hours
	 */
	@Override
	public long getTaskId() {
		return model.getTaskId();
	}

	/**
	 * Returns the user group ID of this working hours.
	 *
	 * @return the user group ID of this working hours
	 */
	@Override
	public long getUserGroupId() {
		return model.getUserGroupId();
	}

	/**
	 * Returns the user ID of this working hours.
	 *
	 * @return the user ID of this working hours
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user uuid of this working hours.
	 *
	 * @return the user uuid of this working hours
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this working hours.
	 *
	 * @return the uuid of this working hours
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	/**
	 * Returns the working hours ID of this working hours.
	 *
	 * @return the working hours ID of this working hours
	 */
	@Override
	public long getWorkingHoursId() {
		return model.getWorkingHoursId();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the comment of this working hours.
	 *
	 * @param comment the comment of this working hours
	 */
	@Override
	public void setComment(String comment) {
		model.setComment(comment);
	}

	/**
	 * Sets the company ID of this working hours.
	 *
	 * @param companyId the company ID of this working hours
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this working hours.
	 *
	 * @param createDate the create date of this working hours
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the group ID of this working hours.
	 *
	 * @param groupId the group ID of this working hours
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the hours of this working hours.
	 *
	 * @param hours the hours of this working hours
	 */
	@Override
	public void setHours(double hours) {
		model.setHours(hours);
	}

	/**
	 * Sets the modified date of this working hours.
	 *
	 * @param modifiedDate the modified date of this working hours
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the primary key of this working hours.
	 *
	 * @param primaryKey the primary key of this working hours
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the start date of this working hours.
	 *
	 * @param startDate the start date of this working hours
	 */
	@Override
	public void setStartDate(Date startDate) {
		model.setStartDate(startDate);
	}

	/**
	 * Sets the task ID of this working hours.
	 *
	 * @param taskId the task ID of this working hours
	 */
	@Override
	public void setTaskId(long taskId) {
		model.setTaskId(taskId);
	}

	/**
	 * Sets the user group ID of this working hours.
	 *
	 * @param userGroupId the user group ID of this working hours
	 */
	@Override
	public void setUserGroupId(long userGroupId) {
		model.setUserGroupId(userGroupId);
	}

	/**
	 * Sets the user ID of this working hours.
	 *
	 * @param userId the user ID of this working hours
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user uuid of this working hours.
	 *
	 * @param userUuid the user uuid of this working hours
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this working hours.
	 *
	 * @param uuid the uuid of this working hours
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	/**
	 * Sets the working hours ID of this working hours.
	 *
	 * @param workingHoursId the working hours ID of this working hours
	 */
	@Override
	public void setWorkingHoursId(long workingHoursId) {
		model.setWorkingHoursId(workingHoursId);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected WorkingHoursWrapper wrap(WorkingHours workingHours) {
		return new WorkingHoursWrapper(workingHours);
	}

}