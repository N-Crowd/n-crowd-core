/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.model;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.HashMap;
import java.util.Map;

import org.osgi.annotation.versioning.ProviderType;

/**
 * <p>
 * This class is a wrapper for {@link DocumentMapping}.
 * </p>
 *
 * @author Sudhanshu
 * @see DocumentMapping
 * @generated
 */
@ProviderType
public class DocumentMappingWrapper
	extends BaseModelWrapper<DocumentMapping>
	implements DocumentMapping, ModelWrapper<DocumentMapping> {

	public DocumentMappingWrapper(DocumentMapping documentMapping) {
		super(documentMapping);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("mappingId", getMappingId());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("userGroupId", getUserGroupId());
		attributes.put("taskId", getTaskId());
		attributes.put("dlFileEntryId", getDlFileEntryId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long mappingId = (Long)attributes.get("mappingId");

		if (mappingId != null) {
			setMappingId(mappingId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long userGroupId = (Long)attributes.get("userGroupId");

		if (userGroupId != null) {
			setUserGroupId(userGroupId);
		}

		Long taskId = (Long)attributes.get("taskId");

		if (taskId != null) {
			setTaskId(taskId);
		}

		Long dlFileEntryId = (Long)attributes.get("dlFileEntryId");

		if (dlFileEntryId != null) {
			setDlFileEntryId(dlFileEntryId);
		}
	}

	/**
	 * Returns the company ID of this document mapping.
	 *
	 * @return the company ID of this document mapping
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the dl file entry ID of this document mapping.
	 *
	 * @return the dl file entry ID of this document mapping
	 */
	@Override
	public long getDlFileEntryId() {
		return model.getDlFileEntryId();
	}

	/**
	 * Returns the group ID of this document mapping.
	 *
	 * @return the group ID of this document mapping
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the mapping ID of this document mapping.
	 *
	 * @return the mapping ID of this document mapping
	 */
	@Override
	public long getMappingId() {
		return model.getMappingId();
	}

	/**
	 * Returns the primary key of this document mapping.
	 *
	 * @return the primary key of this document mapping
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the task ID of this document mapping.
	 *
	 * @return the task ID of this document mapping
	 */
	@Override
	public long getTaskId() {
		return model.getTaskId();
	}

	/**
	 * Returns the user group ID of this document mapping.
	 *
	 * @return the user group ID of this document mapping
	 */
	@Override
	public long getUserGroupId() {
		return model.getUserGroupId();
	}

	/**
	 * Returns the uuid of this document mapping.
	 *
	 * @return the uuid of this document mapping
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the company ID of this document mapping.
	 *
	 * @param companyId the company ID of this document mapping
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the dl file entry ID of this document mapping.
	 *
	 * @param dlFileEntryId the dl file entry ID of this document mapping
	 */
	@Override
	public void setDlFileEntryId(long dlFileEntryId) {
		model.setDlFileEntryId(dlFileEntryId);
	}

	/**
	 * Sets the group ID of this document mapping.
	 *
	 * @param groupId the group ID of this document mapping
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the mapping ID of this document mapping.
	 *
	 * @param mappingId the mapping ID of this document mapping
	 */
	@Override
	public void setMappingId(long mappingId) {
		model.setMappingId(mappingId);
	}

	/**
	 * Sets the primary key of this document mapping.
	 *
	 * @param primaryKey the primary key of this document mapping
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the task ID of this document mapping.
	 *
	 * @param taskId the task ID of this document mapping
	 */
	@Override
	public void setTaskId(long taskId) {
		model.setTaskId(taskId);
	}

	/**
	 * Sets the user group ID of this document mapping.
	 *
	 * @param userGroupId the user group ID of this document mapping
	 */
	@Override
	public void setUserGroupId(long userGroupId) {
		model.setUserGroupId(userGroupId);
	}

	/**
	 * Sets the uuid of this document mapping.
	 *
	 * @param uuid the uuid of this document mapping
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	protected DocumentMappingWrapper wrap(DocumentMapping documentMapping) {
		return new DocumentMappingWrapper(documentMapping);
	}

}