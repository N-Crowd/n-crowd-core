/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.osgi.annotation.versioning.ProviderType;

/**
 * <p>
 * This class is a wrapper for {@link Task}.
 * </p>
 *
 * @author Sudhanshu
 * @see Task
 * @generated
 */
@ProviderType
public class TaskWrapper
	extends BaseModelWrapper<Task> implements Task, ModelWrapper<Task> {

	public TaskWrapper(Task task) {
		super(task);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("taskId", getTaskId());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("userGroupId", getUserGroupId());
		attributes.put("userId", getUserId());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("sprintId", getSprintId());
		attributes.put("statusId", getStatusId());
		attributes.put("description", getDescription());
		attributes.put("text", getText());
		attributes.put("assignedTo", getAssignedTo());
		attributes.put("evaluation", getEvaluation());
		attributes.put("sortIndex", getSortIndex());
		attributes.put("individualColor", getIndividualColor());
		attributes.put("fixedSince", getFixedSince());
		attributes.put("taskGroupId", getTaskGroupId());
		attributes.put("taskGroup", isTaskGroup());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long taskId = (Long)attributes.get("taskId");

		if (taskId != null) {
			setTaskId(taskId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long userGroupId = (Long)attributes.get("userGroupId");

		if (userGroupId != null) {
			setUserGroupId(userGroupId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long sprintId = (Long)attributes.get("sprintId");

		if (sprintId != null) {
			setSprintId(sprintId);
		}

		Long statusId = (Long)attributes.get("statusId");

		if (statusId != null) {
			setStatusId(statusId);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String text = (String)attributes.get("text");

		if (text != null) {
			setText(text);
		}

		Long assignedTo = (Long)attributes.get("assignedTo");

		if (assignedTo != null) {
			setAssignedTo(assignedTo);
		}

		Double evaluation = (Double)attributes.get("evaluation");

		if (evaluation != null) {
			setEvaluation(evaluation);
		}

		Integer sortIndex = (Integer)attributes.get("sortIndex");

		if (sortIndex != null) {
			setSortIndex(sortIndex);
		}

		String individualColor = (String)attributes.get("individualColor");

		if (individualColor != null) {
			setIndividualColor(individualColor);
		}

		Date fixedSince = (Date)attributes.get("fixedSince");

		if (fixedSince != null) {
			setFixedSince(fixedSince);
		}

		Long taskGroupId = (Long)attributes.get("taskGroupId");

		if (taskGroupId != null) {
			setTaskGroupId(taskGroupId);
		}

		Boolean taskGroup = (Boolean)attributes.get("taskGroup");

		if (taskGroup != null) {
			setTaskGroup(taskGroup);
		}
	}

	@Override
	public int compareTo(Task task) {
		return model.compareTo(task);
	}

	/**
	 * Returns the assigned to of this task.
	 *
	 * @return the assigned to of this task
	 */
	@Override
	public long getAssignedTo() {
		return model.getAssignedTo();
	}

	@Override
	public String getAssignedToName() {
		return model.getAssignedToName();
	}

	@Override
	public com.liferay.portal.kernel.model.User getAssignedToUser() {
		return model.getAssignedToUser();
	}

	/**
	 * Returns the company ID of this task.
	 *
	 * @return the company ID of this task
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this task.
	 *
	 * @return the create date of this task
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	/**
	 * Returns the description of this task.
	 *
	 * @return the description of this task
	 */
	@Override
	public String getDescription() {
		return model.getDescription();
	}

	/**
	 * Returns the evaluation of this task.
	 *
	 * @return the evaluation of this task
	 */
	@Override
	public double getEvaluation() {
		return model.getEvaluation();
	}

	/**
	 * Returns the fixed since of this task.
	 *
	 * @return the fixed since of this task
	 */
	@Override
	public Date getFixedSince() {
		return model.getFixedSince();
	}

	/**
	 * Returns the group ID of this task.
	 *
	 * @return the group ID of this task
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	@Override
	public Task getGroupTask() {
		return model.getGroupTask();
	}

	/**
	 * Returns the individual color of this task.
	 *
	 * @return the individual color of this task
	 */
	@Override
	public String getIndividualColor() {
		return model.getIndividualColor();
	}

	/**
	 * Returns the modified date of this task.
	 *
	 * @return the modified date of this task
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the primary key of this task.
	 *
	 * @return the primary key of this task
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the sort index of this task.
	 *
	 * @return the sort index of this task
	 */
	@Override
	public int getSortIndex() {
		return model.getSortIndex();
	}

	/**
	 * Returns the sprint ID of this task.
	 *
	 * @return the sprint ID of this task
	 */
	@Override
	public long getSprintId() {
		return model.getSprintId();
	}

	@Override
	public Status getStatus() {
		return model.getStatus();
	}

	/**
	 * Returns the status ID of this task.
	 *
	 * @return the status ID of this task
	 */
	@Override
	public long getStatusId() {
		return model.getStatusId();
	}

	@Override
	public String getStatusTableClass() {
		return model.getStatusTableClass();
	}

	/**
	 * Returns the task group of this task.
	 *
	 * @return the task group of this task
	 */
	@Override
	public boolean getTaskGroup() {
		return model.getTaskGroup();
	}

	/**
	 * Returns the task group ID of this task.
	 *
	 * @return the task group ID of this task
	 */
	@Override
	public long getTaskGroupId() {
		return model.getTaskGroupId();
	}

	/**
	 * Returns the task ID of this task.
	 *
	 * @return the task ID of this task
	 */
	@Override
	public long getTaskId() {
		return model.getTaskId();
	}

	/**
	 * Returns the text of this task.
	 *
	 * @return the text of this task
	 */
	@Override
	public String getText() {
		return model.getText();
	}

	@Override
	public double getTimeForCopy() {
		return model.getTimeForCopy();
	}

	@Override
	public WorkingHours getTimeToSave() {
		return model.getTimeToSave();
	}

	@Override
	public float getTotalWorkingHours() {
		return model.getTotalWorkingHours();
	}

	/**
	 * Returns the user group ID of this task.
	 *
	 * @return the user group ID of this task
	 */
	@Override
	public long getUserGroupId() {
		return model.getUserGroupId();
	}

	/**
	 * Returns the user ID of this task.
	 *
	 * @return the user ID of this task
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user uuid of this task.
	 *
	 * @return the user uuid of this task
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this task.
	 *
	 * @return the uuid of this task
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public java.util.List<WorkingHours> getWorkingHours() {
		return model.getWorkingHours();
	}

	/**
	 * Returns <code>true</code> if this task is task group.
	 *
	 * @return <code>true</code> if this task is task group; <code>false</code> otherwise
	 */
	@Override
	public boolean isTaskGroup() {
		return model.isTaskGroup();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the assigned to of this task.
	 *
	 * @param assignedTo the assigned to of this task
	 */
	@Override
	public void setAssignedTo(long assignedTo) {
		model.setAssignedTo(assignedTo);
	}

	@Override
	public void setAssignedToUser(com.liferay.portal.kernel.model.User user) {
		model.setAssignedToUser(user);
	}

	/**
	 * Sets the company ID of this task.
	 *
	 * @param companyId the company ID of this task
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this task.
	 *
	 * @param createDate the create date of this task
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the description of this task.
	 *
	 * @param description the description of this task
	 */
	@Override
	public void setDescription(String description) {
		model.setDescription(description);
	}

	/**
	 * Sets the evaluation of this task.
	 *
	 * @param evaluation the evaluation of this task
	 */
	@Override
	public void setEvaluation(double evaluation) {
		model.setEvaluation(evaluation);
	}

	/**
	 * Sets the fixed since of this task.
	 *
	 * @param fixedSince the fixed since of this task
	 */
	@Override
	public void setFixedSince(Date fixedSince) {
		model.setFixedSince(fixedSince);
	}

	/**
	 * Sets the group ID of this task.
	 *
	 * @param groupId the group ID of this task
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	@Override
	public void setGroupTask(Task groupTask) {
		model.setGroupTask(groupTask);
	}

	/**
	 * Sets the individual color of this task.
	 *
	 * @param individualColor the individual color of this task
	 */
	@Override
	public void setIndividualColor(String individualColor) {
		model.setIndividualColor(individualColor);
	}

	/**
	 * Sets the modified date of this task.
	 *
	 * @param modifiedDate the modified date of this task
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the primary key of this task.
	 *
	 * @param primaryKey the primary key of this task
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the sort index of this task.
	 *
	 * @param sortIndex the sort index of this task
	 */
	@Override
	public void setSortIndex(int sortIndex) {
		model.setSortIndex(sortIndex);
	}

	/**
	 * Sets the sprint ID of this task.
	 *
	 * @param sprintId the sprint ID of this task
	 */
	@Override
	public void setSprintId(long sprintId) {
		model.setSprintId(sprintId);
	}

	@Override
	public void setStatus(Status status) {
		model.setStatus(status);
	}

	/**
	 * Sets the status ID of this task.
	 *
	 * @param statusId the status ID of this task
	 */
	@Override
	public void setStatusId(long statusId) {
		model.setStatusId(statusId);
	}

	/**
	 * Sets whether this task is task group.
	 *
	 * @param taskGroup the task group of this task
	 */
	@Override
	public void setTaskGroup(boolean taskGroup) {
		model.setTaskGroup(taskGroup);
	}

	/**
	 * Sets the task group ID of this task.
	 *
	 * @param taskGroupId the task group ID of this task
	 */
	@Override
	public void setTaskGroupId(long taskGroupId) {
		model.setTaskGroupId(taskGroupId);
	}

	/**
	 * Sets the task ID of this task.
	 *
	 * @param taskId the task ID of this task
	 */
	@Override
	public void setTaskId(long taskId) {
		model.setTaskId(taskId);
	}

	/**
	 * Sets the text of this task.
	 *
	 * @param text the text of this task
	 */
	@Override
	public void setText(String text) {
		model.setText(text);
	}

	@Override
	public void setTimeForCopy(double timeForCopy) {
		model.setTimeForCopy(timeForCopy);
	}

	@Override
	public void setTimeToSave(WorkingHours timeToSave) {
		model.setTimeToSave(timeToSave);
	}

	/**
	 * Sets the user group ID of this task.
	 *
	 * @param userGroupId the user group ID of this task
	 */
	@Override
	public void setUserGroupId(long userGroupId) {
		model.setUserGroupId(userGroupId);
	}

	/**
	 * Sets the user ID of this task.
	 *
	 * @param userId the user ID of this task
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user uuid of this task.
	 *
	 * @param userUuid the user uuid of this task
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this task.
	 *
	 * @param uuid the uuid of this task
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public void setWorkingHours(java.util.List<WorkingHours> hours) {
		model.setWorkingHours(hours);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected TaskWrapper wrap(Task task) {
		return new TaskWrapper(task);
	}

}