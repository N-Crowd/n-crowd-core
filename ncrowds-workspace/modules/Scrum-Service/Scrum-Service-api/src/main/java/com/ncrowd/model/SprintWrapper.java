/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.model;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.osgi.annotation.versioning.ProviderType;

/**
 * <p>
 * This class is a wrapper for {@link Sprint}.
 * </p>
 *
 * @author Sudhanshu
 * @see Sprint
 * @generated
 */
@ProviderType
public class SprintWrapper
	extends BaseModelWrapper<Sprint> implements Sprint, ModelWrapper<Sprint> {

	public SprintWrapper(Sprint sprint) {
		super(sprint);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("sprintId", getSprintId());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("userGroupId", getUserGroupId());
		attributes.put("startDate", getStartDate());
		attributes.put("endDate", getEndDate());
		attributes.put("sprintNumber", getSprintNumber());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long sprintId = (Long)attributes.get("sprintId");

		if (sprintId != null) {
			setSprintId(sprintId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long userGroupId = (Long)attributes.get("userGroupId");

		if (userGroupId != null) {
			setUserGroupId(userGroupId);
		}

		Date startDate = (Date)attributes.get("startDate");

		if (startDate != null) {
			setStartDate(startDate);
		}

		Date endDate = (Date)attributes.get("endDate");

		if (endDate != null) {
			setEndDate(endDate);
		}

		Integer sprintNumber = (Integer)attributes.get("sprintNumber");

		if (sprintNumber != null) {
			setSprintNumber(sprintNumber);
		}
	}

	/**
	 * Returns the company ID of this sprint.
	 *
	 * @return the company ID of this sprint
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the end date of this sprint.
	 *
	 * @return the end date of this sprint
	 */
	@Override
	public Date getEndDate() {
		return model.getEndDate();
	}

	/**
	 * Returns the group ID of this sprint.
	 *
	 * @return the group ID of this sprint
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the primary key of this sprint.
	 *
	 * @return the primary key of this sprint
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the sprint ID of this sprint.
	 *
	 * @return the sprint ID of this sprint
	 */
	@Override
	public long getSprintId() {
		return model.getSprintId();
	}

	/**
	 * Returns the sprint number of this sprint.
	 *
	 * @return the sprint number of this sprint
	 */
	@Override
	public int getSprintNumber() {
		return model.getSprintNumber();
	}

	/**
	 * Returns the start date of this sprint.
	 *
	 * @return the start date of this sprint
	 */
	@Override
	public Date getStartDate() {
		return model.getStartDate();
	}

	/**
	 * Returns the user group ID of this sprint.
	 *
	 * @return the user group ID of this sprint
	 */
	@Override
	public long getUserGroupId() {
		return model.getUserGroupId();
	}

	/**
	 * Returns the uuid of this sprint.
	 *
	 * @return the uuid of this sprint
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the company ID of this sprint.
	 *
	 * @param companyId the company ID of this sprint
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the end date of this sprint.
	 *
	 * @param endDate the end date of this sprint
	 */
	@Override
	public void setEndDate(Date endDate) {
		model.setEndDate(endDate);
	}

	/**
	 * Sets the group ID of this sprint.
	 *
	 * @param groupId the group ID of this sprint
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the primary key of this sprint.
	 *
	 * @param primaryKey the primary key of this sprint
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the sprint ID of this sprint.
	 *
	 * @param sprintId the sprint ID of this sprint
	 */
	@Override
	public void setSprintId(long sprintId) {
		model.setSprintId(sprintId);
	}

	/**
	 * Sets the sprint number of this sprint.
	 *
	 * @param sprintNumber the sprint number of this sprint
	 */
	@Override
	public void setSprintNumber(int sprintNumber) {
		model.setSprintNumber(sprintNumber);
	}

	/**
	 * Sets the start date of this sprint.
	 *
	 * @param startDate the start date of this sprint
	 */
	@Override
	public void setStartDate(Date startDate) {
		model.setStartDate(startDate);
	}

	/**
	 * Sets the user group ID of this sprint.
	 *
	 * @param userGroupId the user group ID of this sprint
	 */
	@Override
	public void setUserGroupId(long userGroupId) {
		model.setUserGroupId(userGroupId);
	}

	/**
	 * Sets the uuid of this sprint.
	 *
	 * @param uuid the uuid of this sprint
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	protected SprintWrapper wrap(Sprint sprint) {
		return new SprintWrapper(sprint);
	}

}