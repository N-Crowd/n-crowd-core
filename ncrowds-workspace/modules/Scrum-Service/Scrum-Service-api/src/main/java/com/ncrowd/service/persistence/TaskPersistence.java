/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.ncrowd.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.ncrowd.exception.NoSuchTaskException;
import com.ncrowd.model.Task;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the task service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Sudhanshu
 * @see TaskUtil
 * @generated
 */
@ProviderType
public interface TaskPersistence extends BasePersistence<Task> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link TaskUtil} to access the task persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the tasks where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching tasks
	 */
	public java.util.List<Task> findByUuid(String uuid);

	/**
	 * Returns a range of all the tasks where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @return the range of matching tasks
	 */
	public java.util.List<Task> findByUuid(String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the tasks where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching tasks
	 */
	public java.util.List<Task> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator);

	/**
	 * Returns an ordered range of all the tasks where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching tasks
	 */
	public java.util.List<Task> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Returns the first task in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public Task findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Task>
				orderByComparator)
		throws NoSuchTaskException;

	/**
	 * Returns the first task in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task, or <code>null</code> if a matching task could not be found
	 */
	public Task fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator);

	/**
	 * Returns the last task in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public Task findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Task>
				orderByComparator)
		throws NoSuchTaskException;

	/**
	 * Returns the last task in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task, or <code>null</code> if a matching task could not be found
	 */
	public Task fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator);

	/**
	 * Returns the tasks before and after the current task in the ordered set where uuid = &#63;.
	 *
	 * @param taskId the primary key of the current task
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next task
	 * @throws NoSuchTaskException if a task with the primary key could not be found
	 */
	public Task[] findByUuid_PrevAndNext(
			long taskId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Task>
				orderByComparator)
		throws NoSuchTaskException;

	/**
	 * Removes all the tasks where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of tasks where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching tasks
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the task where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchTaskException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public Task findByUUID_G(String uuid, long groupId)
		throws NoSuchTaskException;

	/**
	 * Returns the task where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching task, or <code>null</code> if a matching task could not be found
	 */
	public Task fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the task where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching task, or <code>null</code> if a matching task could not be found
	 */
	public Task fetchByUUID_G(
		String uuid, long groupId, boolean retrieveFromCache);

	/**
	 * Removes the task where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the task that was removed
	 */
	public Task removeByUUID_G(String uuid, long groupId)
		throws NoSuchTaskException;

	/**
	 * Returns the number of tasks where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching tasks
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the tasks where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching tasks
	 */
	public java.util.List<Task> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the tasks where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @return the range of matching tasks
	 */
	public java.util.List<Task> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the tasks where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching tasks
	 */
	public java.util.List<Task> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator);

	/**
	 * Returns an ordered range of all the tasks where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching tasks
	 */
	public java.util.List<Task> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Returns the first task in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public Task findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Task>
				orderByComparator)
		throws NoSuchTaskException;

	/**
	 * Returns the first task in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task, or <code>null</code> if a matching task could not be found
	 */
	public Task fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator);

	/**
	 * Returns the last task in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public Task findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Task>
				orderByComparator)
		throws NoSuchTaskException;

	/**
	 * Returns the last task in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task, or <code>null</code> if a matching task could not be found
	 */
	public Task fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator);

	/**
	 * Returns the tasks before and after the current task in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param taskId the primary key of the current task
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next task
	 * @throws NoSuchTaskException if a task with the primary key could not be found
	 */
	public Task[] findByUuid_C_PrevAndNext(
			long taskId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Task>
				orderByComparator)
		throws NoSuchTaskException;

	/**
	 * Removes all the tasks where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of tasks where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching tasks
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the tasks where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @return the matching tasks
	 */
	public java.util.List<Task> findByUserGroupId(
		long userGroupId, boolean taskGroup);

	/**
	 * Returns a range of all the tasks where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @return the range of matching tasks
	 */
	public java.util.List<Task> findByUserGroupId(
		long userGroupId, boolean taskGroup, int start, int end);

	/**
	 * Returns an ordered range of all the tasks where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching tasks
	 */
	public java.util.List<Task> findByUserGroupId(
		long userGroupId, boolean taskGroup, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator);

	/**
	 * Returns an ordered range of all the tasks where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching tasks
	 */
	public java.util.List<Task> findByUserGroupId(
		long userGroupId, boolean taskGroup, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Returns the first task in the ordered set where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public Task findByUserGroupId_First(
			long userGroupId, boolean taskGroup,
			com.liferay.portal.kernel.util.OrderByComparator<Task>
				orderByComparator)
		throws NoSuchTaskException;

	/**
	 * Returns the first task in the ordered set where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task, or <code>null</code> if a matching task could not be found
	 */
	public Task fetchByUserGroupId_First(
		long userGroupId, boolean taskGroup,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator);

	/**
	 * Returns the last task in the ordered set where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public Task findByUserGroupId_Last(
			long userGroupId, boolean taskGroup,
			com.liferay.portal.kernel.util.OrderByComparator<Task>
				orderByComparator)
		throws NoSuchTaskException;

	/**
	 * Returns the last task in the ordered set where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task, or <code>null</code> if a matching task could not be found
	 */
	public Task fetchByUserGroupId_Last(
		long userGroupId, boolean taskGroup,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator);

	/**
	 * Returns the tasks before and after the current task in the ordered set where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param taskId the primary key of the current task
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next task
	 * @throws NoSuchTaskException if a task with the primary key could not be found
	 */
	public Task[] findByUserGroupId_PrevAndNext(
			long taskId, long userGroupId, boolean taskGroup,
			com.liferay.portal.kernel.util.OrderByComparator<Task>
				orderByComparator)
		throws NoSuchTaskException;

	/**
	 * Removes all the tasks where userGroupId = &#63; and taskGroup = &#63; from the database.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 */
	public void removeByUserGroupId(long userGroupId, boolean taskGroup);

	/**
	 * Returns the number of tasks where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @return the number of matching tasks
	 */
	public int countByUserGroupId(long userGroupId, boolean taskGroup);

	/**
	 * Returns all the tasks where userGroupId = &#63; and sprintId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param taskGroup the task group
	 * @return the matching tasks
	 */
	public java.util.List<Task> findByG_S(
		long userGroupId, long sprintId, boolean taskGroup);

	/**
	 * Returns a range of all the tasks where userGroupId = &#63; and sprintId = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @return the range of matching tasks
	 */
	public java.util.List<Task> findByG_S(
		long userGroupId, long sprintId, boolean taskGroup, int start, int end);

	/**
	 * Returns an ordered range of all the tasks where userGroupId = &#63; and sprintId = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching tasks
	 */
	public java.util.List<Task> findByG_S(
		long userGroupId, long sprintId, boolean taskGroup, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator);

	/**
	 * Returns an ordered range of all the tasks where userGroupId = &#63; and sprintId = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching tasks
	 */
	public java.util.List<Task> findByG_S(
		long userGroupId, long sprintId, boolean taskGroup, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Returns the first task in the ordered set where userGroupId = &#63; and sprintId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public Task findByG_S_First(
			long userGroupId, long sprintId, boolean taskGroup,
			com.liferay.portal.kernel.util.OrderByComparator<Task>
				orderByComparator)
		throws NoSuchTaskException;

	/**
	 * Returns the first task in the ordered set where userGroupId = &#63; and sprintId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task, or <code>null</code> if a matching task could not be found
	 */
	public Task fetchByG_S_First(
		long userGroupId, long sprintId, boolean taskGroup,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator);

	/**
	 * Returns the last task in the ordered set where userGroupId = &#63; and sprintId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public Task findByG_S_Last(
			long userGroupId, long sprintId, boolean taskGroup,
			com.liferay.portal.kernel.util.OrderByComparator<Task>
				orderByComparator)
		throws NoSuchTaskException;

	/**
	 * Returns the last task in the ordered set where userGroupId = &#63; and sprintId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task, or <code>null</code> if a matching task could not be found
	 */
	public Task fetchByG_S_Last(
		long userGroupId, long sprintId, boolean taskGroup,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator);

	/**
	 * Returns the tasks before and after the current task in the ordered set where userGroupId = &#63; and sprintId = &#63; and taskGroup = &#63;.
	 *
	 * @param taskId the primary key of the current task
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next task
	 * @throws NoSuchTaskException if a task with the primary key could not be found
	 */
	public Task[] findByG_S_PrevAndNext(
			long taskId, long userGroupId, long sprintId, boolean taskGroup,
			com.liferay.portal.kernel.util.OrderByComparator<Task>
				orderByComparator)
		throws NoSuchTaskException;

	/**
	 * Removes all the tasks where userGroupId = &#63; and sprintId = &#63; and taskGroup = &#63; from the database.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param taskGroup the task group
	 */
	public void removeByG_S(long userGroupId, long sprintId, boolean taskGroup);

	/**
	 * Returns the number of tasks where userGroupId = &#63; and sprintId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param taskGroup the task group
	 * @return the number of matching tasks
	 */
	public int countByG_S(long userGroupId, long sprintId, boolean taskGroup);

	/**
	 * Returns all the tasks where userGroupId = &#63; and sprintId = &#63; and statusId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param statusId the status ID
	 * @param taskGroup the task group
	 * @return the matching tasks
	 */
	public java.util.List<Task> findByG_S_S(
		long userGroupId, long sprintId, long statusId, boolean taskGroup);

	/**
	 * Returns a range of all the tasks where userGroupId = &#63; and sprintId = &#63; and statusId = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param statusId the status ID
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @return the range of matching tasks
	 */
	public java.util.List<Task> findByG_S_S(
		long userGroupId, long sprintId, long statusId, boolean taskGroup,
		int start, int end);

	/**
	 * Returns an ordered range of all the tasks where userGroupId = &#63; and sprintId = &#63; and statusId = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param statusId the status ID
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching tasks
	 */
	public java.util.List<Task> findByG_S_S(
		long userGroupId, long sprintId, long statusId, boolean taskGroup,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator);

	/**
	 * Returns an ordered range of all the tasks where userGroupId = &#63; and sprintId = &#63; and statusId = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param statusId the status ID
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching tasks
	 */
	public java.util.List<Task> findByG_S_S(
		long userGroupId, long sprintId, long statusId, boolean taskGroup,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Returns the first task in the ordered set where userGroupId = &#63; and sprintId = &#63; and statusId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param statusId the status ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public Task findByG_S_S_First(
			long userGroupId, long sprintId, long statusId, boolean taskGroup,
			com.liferay.portal.kernel.util.OrderByComparator<Task>
				orderByComparator)
		throws NoSuchTaskException;

	/**
	 * Returns the first task in the ordered set where userGroupId = &#63; and sprintId = &#63; and statusId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param statusId the status ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task, or <code>null</code> if a matching task could not be found
	 */
	public Task fetchByG_S_S_First(
		long userGroupId, long sprintId, long statusId, boolean taskGroup,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator);

	/**
	 * Returns the last task in the ordered set where userGroupId = &#63; and sprintId = &#63; and statusId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param statusId the status ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public Task findByG_S_S_Last(
			long userGroupId, long sprintId, long statusId, boolean taskGroup,
			com.liferay.portal.kernel.util.OrderByComparator<Task>
				orderByComparator)
		throws NoSuchTaskException;

	/**
	 * Returns the last task in the ordered set where userGroupId = &#63; and sprintId = &#63; and statusId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param statusId the status ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task, or <code>null</code> if a matching task could not be found
	 */
	public Task fetchByG_S_S_Last(
		long userGroupId, long sprintId, long statusId, boolean taskGroup,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator);

	/**
	 * Returns the tasks before and after the current task in the ordered set where userGroupId = &#63; and sprintId = &#63; and statusId = &#63; and taskGroup = &#63;.
	 *
	 * @param taskId the primary key of the current task
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param statusId the status ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next task
	 * @throws NoSuchTaskException if a task with the primary key could not be found
	 */
	public Task[] findByG_S_S_PrevAndNext(
			long taskId, long userGroupId, long sprintId, long statusId,
			boolean taskGroup,
			com.liferay.portal.kernel.util.OrderByComparator<Task>
				orderByComparator)
		throws NoSuchTaskException;

	/**
	 * Removes all the tasks where userGroupId = &#63; and sprintId = &#63; and statusId = &#63; and taskGroup = &#63; from the database.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param statusId the status ID
	 * @param taskGroup the task group
	 */
	public void removeByG_S_S(
		long userGroupId, long sprintId, long statusId, boolean taskGroup);

	/**
	 * Returns the number of tasks where userGroupId = &#63; and sprintId = &#63; and statusId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param sprintId the sprint ID
	 * @param statusId the status ID
	 * @param taskGroup the task group
	 * @return the number of matching tasks
	 */
	public int countByG_S_S(
		long userGroupId, long sprintId, long statusId, boolean taskGroup);

	/**
	 * Returns all the tasks where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @return the matching tasks
	 */
	public java.util.List<Task> findByTaskGroup(
		long userGroupId, boolean taskGroup);

	/**
	 * Returns a range of all the tasks where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @return the range of matching tasks
	 */
	public java.util.List<Task> findByTaskGroup(
		long userGroupId, boolean taskGroup, int start, int end);

	/**
	 * Returns an ordered range of all the tasks where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching tasks
	 */
	public java.util.List<Task> findByTaskGroup(
		long userGroupId, boolean taskGroup, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator);

	/**
	 * Returns an ordered range of all the tasks where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching tasks
	 */
	public java.util.List<Task> findByTaskGroup(
		long userGroupId, boolean taskGroup, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Returns the first task in the ordered set where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public Task findByTaskGroup_First(
			long userGroupId, boolean taskGroup,
			com.liferay.portal.kernel.util.OrderByComparator<Task>
				orderByComparator)
		throws NoSuchTaskException;

	/**
	 * Returns the first task in the ordered set where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task, or <code>null</code> if a matching task could not be found
	 */
	public Task fetchByTaskGroup_First(
		long userGroupId, boolean taskGroup,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator);

	/**
	 * Returns the last task in the ordered set where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public Task findByTaskGroup_Last(
			long userGroupId, boolean taskGroup,
			com.liferay.portal.kernel.util.OrderByComparator<Task>
				orderByComparator)
		throws NoSuchTaskException;

	/**
	 * Returns the last task in the ordered set where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task, or <code>null</code> if a matching task could not be found
	 */
	public Task fetchByTaskGroup_Last(
		long userGroupId, boolean taskGroup,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator);

	/**
	 * Returns the tasks before and after the current task in the ordered set where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param taskId the primary key of the current task
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next task
	 * @throws NoSuchTaskException if a task with the primary key could not be found
	 */
	public Task[] findByTaskGroup_PrevAndNext(
			long taskId, long userGroupId, boolean taskGroup,
			com.liferay.portal.kernel.util.OrderByComparator<Task>
				orderByComparator)
		throws NoSuchTaskException;

	/**
	 * Removes all the tasks where userGroupId = &#63; and taskGroup = &#63; from the database.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 */
	public void removeByTaskGroup(long userGroupId, boolean taskGroup);

	/**
	 * Returns the number of tasks where userGroupId = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param taskGroup the task group
	 * @return the number of matching tasks
	 */
	public int countByTaskGroup(long userGroupId, boolean taskGroup);

	/**
	 * Returns all the tasks where userGroupId = &#63; and description = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param description the description
	 * @param taskGroup the task group
	 * @return the matching tasks
	 */
	public java.util.List<Task> findByTaskGroupName(
		long userGroupId, String description, boolean taskGroup);

	/**
	 * Returns a range of all the tasks where userGroupId = &#63; and description = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param description the description
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @return the range of matching tasks
	 */
	public java.util.List<Task> findByTaskGroupName(
		long userGroupId, String description, boolean taskGroup, int start,
		int end);

	/**
	 * Returns an ordered range of all the tasks where userGroupId = &#63; and description = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param description the description
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching tasks
	 */
	public java.util.List<Task> findByTaskGroupName(
		long userGroupId, String description, boolean taskGroup, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator);

	/**
	 * Returns an ordered range of all the tasks where userGroupId = &#63; and description = &#63; and taskGroup = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param description the description
	 * @param taskGroup the task group
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching tasks
	 */
	public java.util.List<Task> findByTaskGroupName(
		long userGroupId, String description, boolean taskGroup, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Returns the first task in the ordered set where userGroupId = &#63; and description = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param description the description
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public Task findByTaskGroupName_First(
			long userGroupId, String description, boolean taskGroup,
			com.liferay.portal.kernel.util.OrderByComparator<Task>
				orderByComparator)
		throws NoSuchTaskException;

	/**
	 * Returns the first task in the ordered set where userGroupId = &#63; and description = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param description the description
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching task, or <code>null</code> if a matching task could not be found
	 */
	public Task fetchByTaskGroupName_First(
		long userGroupId, String description, boolean taskGroup,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator);

	/**
	 * Returns the last task in the ordered set where userGroupId = &#63; and description = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param description the description
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task
	 * @throws NoSuchTaskException if a matching task could not be found
	 */
	public Task findByTaskGroupName_Last(
			long userGroupId, String description, boolean taskGroup,
			com.liferay.portal.kernel.util.OrderByComparator<Task>
				orderByComparator)
		throws NoSuchTaskException;

	/**
	 * Returns the last task in the ordered set where userGroupId = &#63; and description = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param description the description
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching task, or <code>null</code> if a matching task could not be found
	 */
	public Task fetchByTaskGroupName_Last(
		long userGroupId, String description, boolean taskGroup,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator);

	/**
	 * Returns the tasks before and after the current task in the ordered set where userGroupId = &#63; and description = &#63; and taskGroup = &#63;.
	 *
	 * @param taskId the primary key of the current task
	 * @param userGroupId the user group ID
	 * @param description the description
	 * @param taskGroup the task group
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next task
	 * @throws NoSuchTaskException if a task with the primary key could not be found
	 */
	public Task[] findByTaskGroupName_PrevAndNext(
			long taskId, long userGroupId, String description,
			boolean taskGroup,
			com.liferay.portal.kernel.util.OrderByComparator<Task>
				orderByComparator)
		throws NoSuchTaskException;

	/**
	 * Removes all the tasks where userGroupId = &#63; and description = &#63; and taskGroup = &#63; from the database.
	 *
	 * @param userGroupId the user group ID
	 * @param description the description
	 * @param taskGroup the task group
	 */
	public void removeByTaskGroupName(
		long userGroupId, String description, boolean taskGroup);

	/**
	 * Returns the number of tasks where userGroupId = &#63; and description = &#63; and taskGroup = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param description the description
	 * @param taskGroup the task group
	 * @return the number of matching tasks
	 */
	public int countByTaskGroupName(
		long userGroupId, String description, boolean taskGroup);

	/**
	 * Caches the task in the entity cache if it is enabled.
	 *
	 * @param task the task
	 */
	public void cacheResult(Task task);

	/**
	 * Caches the tasks in the entity cache if it is enabled.
	 *
	 * @param tasks the tasks
	 */
	public void cacheResult(java.util.List<Task> tasks);

	/**
	 * Creates a new task with the primary key. Does not add the task to the database.
	 *
	 * @param taskId the primary key for the new task
	 * @return the new task
	 */
	public Task create(long taskId);

	/**
	 * Removes the task with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param taskId the primary key of the task
	 * @return the task that was removed
	 * @throws NoSuchTaskException if a task with the primary key could not be found
	 */
	public Task remove(long taskId) throws NoSuchTaskException;

	public Task updateImpl(Task task);

	/**
	 * Returns the task with the primary key or throws a <code>NoSuchTaskException</code> if it could not be found.
	 *
	 * @param taskId the primary key of the task
	 * @return the task
	 * @throws NoSuchTaskException if a task with the primary key could not be found
	 */
	public Task findByPrimaryKey(long taskId) throws NoSuchTaskException;

	/**
	 * Returns the task with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param taskId the primary key of the task
	 * @return the task, or <code>null</code> if a task with the primary key could not be found
	 */
	public Task fetchByPrimaryKey(long taskId);

	/**
	 * Returns all the tasks.
	 *
	 * @return the tasks
	 */
	public java.util.List<Task> findAll();

	/**
	 * Returns a range of all the tasks.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @return the range of tasks
	 */
	public java.util.List<Task> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the tasks.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of tasks
	 */
	public java.util.List<Task> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator);

	/**
	 * Returns an ordered range of all the tasks.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TaskModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of tasks
	 * @param end the upper bound of the range of tasks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of tasks
	 */
	public java.util.List<Task> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Task>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Removes all the tasks from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of tasks.
	 *
	 * @return the number of tasks
	 */
	public int countAll();

}